"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import pandas as pd
import math
import pyomo.environ as pyo
from Model_Library.OptimizationModel import EntityResult, OptimizationModel
from Model_Library.Component.model.AbstractComponent import ComponentKind, ComponentCommodity
from Tooling.dynamics.Dynamic import resample

def flexibility_activation(community, key, strategy):
    # prepare data structures
    graphs = dict()
    for ps_name, ps in community.prosumers.items():
        graphs[ps_name] = ps.build_graph()
    for ca_name, ca in community.community_assets.items():
        graphs[ca_name] = ca.build_graph()
    rescheduling_models = dict()
    flex_dict = dict()
    c_f_dict = dict()
    flex_dynamic_dict = dict()
    validation_models = dict()
    validated_results = dict()
    for ps_name, ps in community.prosumers.items():
        validated_results[ps_name] = ps.get_empty_entity_result()
    for ca_name, ca in community.community_assets.items():
        validated_results[ca_name] = ca.get_empty_entity_result()
    community.result[key] = EntityResult(community.dynamic, [('agg_balance', 'T'), ('agg_export', 'T'), ('agg_import', 'T'), ('internal_exchange', 'T')])

    i_start = community.dynamic.index_of(0)
    i_end = community.dynamic.index_of(community.dynamic.number_of_steps())
    n_overlap_default = 0  # deafult: 0; nr of timesteps the starting point of each interval is shifted into the prior interval
    n_interval_default = 4*24  # default: 4*24; number of time steps in one interval of the RH

    n_interval = n_interval_default

    n_overlap = n_overlap_default

    i_step_start = i_start

    valid = True

    changes = 0

    while i_step_start < i_end:
        print('-------------------' + str(i_step_start) + '-----------------')

        # PREPARE INTERVAL
        if i_step_start + n_interval - n_overlap < i_end and i_step_start + n_interval <= i_end:
            # interval does not reach overall end
            n_horizon_rh = n_interval  # number of time steps in this RH-interval
            n_fix = max(n_horizon_rh - n_overlap, 1)  # number of fixed time steps of this RH-interval
        elif i_step_start + n_interval - n_overlap < i_end and i_step_start + n_interval > i_end:
            # interval reaches end of horizon, but just without overlap
            n_horizon_rh = i_end - i_step_start  # interval length is adapted
            n_fix = max(n_interval - n_overlap, 1)  # but not the number of fixed time steps
        else:
            # fixed time steps hit overall time horizon --> adapt also the fixed time steps
            n_horizon_rh = i_end - i_step_start
            n_fix = n_horizon_rh

        # extract relevant timesteps values from overall time series
        i_step_end = i_step_start + n_horizon_rh
        i_fix_end = i_step_start + n_fix

        # reschedule
        for ps_name, ps in community.prosumers.items():
            component_sizes = {component.name: ps._result[ps._last_result_key][(component.name, 'capacity')] for component in ps.get_components()}
            grid_slack_values = {component.name: (ps._result[ps._last_result_key][(component.name, 'input_1')], ps._result[ps._last_result_key][(component.name, 'output_1')]) for component in ps.get_components(kind=ComponentKind.GRID, commodity=ComponentCommodity.ELECTRICITY)}
            storage_slack_values = {component.name: ps._result[ps._last_result_key][(component.name, 'energy')] for component in ps.get_components(kind=ComponentKind.STORAGE)}
            configuration = {
                'fix_sizing': {
                    'values': component_sizes
                },
                'predict': {},
                'consumption_slack': {
                    'commodity': [ComponentCommodity.COLD, ComponentCommodity.HEAT],
                    'strategy_factor': 100
                },
                'grid_slack': {
                    'commodity': ComponentCommodity.ELECTRICITY,
                    'strategy_factor': 100,
                    'values': grid_slack_values
                },
                'storage_slack': {
                    'commodity': ComponentCommodity.ALL,
                    'values': storage_slack_values,
                    'strategy_factor': 1
                },
                'storage_boundaries': {
                    'commodity': ComponentCommodity.ALL,
                    'min': 0,
                    'max': 1
                }
            }
            if i_step_start != i_start:
                initial_time_step = ps._dynamic.index_of(ps._dynamic.position_of(i_step_start) - 1)
                storage_connect_values = {component.name: validated_results[ps_name][(component.name, 'energy')][initial_time_step] for component in ps.get_components(kind=ComponentKind.STORAGE)}
                configuration['storage_connect'] = {
                    'commodity': ComponentCommodity.ALL,
                    'values': storage_connect_values
                }
            rescheduling_model = ps.optimize(configuration, i_step_start, i_step_end)
            if rescheduling_model is None:
                valid = False
                break
            else:
                rescheduling_models[ps_name] = rescheduling_model
                valid = True

        if not valid:
            changes += 1
            result, n_interval = reduce_invervall(community, n_interval, i_step_start)
            if not result:
                print('Unteres Limit erreicht')
                break
            continue

        for ca_name, ca in community.community_assets.items():
            component_sizes = {component.name: ca._result[ca._last_result_key][(component.name, 'capacity')] for component in ca.get_components()}
            grid_slack_values = {component.name: (ca._result[ca._last_result_key][(component.name, 'input_1')], ca._result[ca._last_result_key][(component.name, 'output_1')]) for component in ca.get_components(kind=ComponentKind.GRID, commodity=ComponentCommodity.ELECTRICITY)}
            storage_slack_values = {component.name: ca._result[ca._last_result_key][(component.name, 'energy')] for component in ca.get_components(kind=ComponentKind.STORAGE)}
            configuration = {
                'fix_sizing': {
                    'values': component_sizes
                },
                'predict': {},
                'consumption_slack': {
                    'commodity': [ComponentCommodity.COLD, ComponentCommodity.HEAT],
                    'strategy_factor': 100
                },
                'grid_slack': {
                    'commodity': ComponentCommodity.ELECTRICITY,
                    'strategy_factor': 100,
                    'values': grid_slack_values
                },
                'storage_slack': {
                    'commodity': ComponentCommodity.ALL,
                    'values': storage_slack_values,
                    'strategy_factor': 1
                },
                'storage_boundaries': {
                    'commodity': ComponentCommodity.ALL,
                    'min': 0,
                    'max': 1
                }
            }
            if i_step_start != i_start:
                initial_time_step = ca._dynamic.index_of(ca._dynamic.position_of(i_step_start) - 1)
                storage_connect_values = {component.name: validated_results[ca_name][(component.name, 'energy')][initial_time_step] for component in ca.get_components(kind=ComponentKind.STORAGE)}
                configuration['storage_connect'] = {
                    'commodity': ComponentCommodity.ALL,
                    'values': storage_connect_values
                }
            rescheduling_model = ca.optimize(configuration, i_step_start, i_step_end)
            if rescheduling_model is None:
                valid = False
                break
            else:
                rescheduling_models[ca_name] = rescheduling_model
                valid = True

        if not valid:
            changes += 1
            result, n_interval = reduce_invervall(community, n_interval, i_step_start)
            if not result:
                print('Unteres Limit erreicht')
                break
            continue

        # calculate flexibility
        for ps_name, ps in community.prosumers.items():
            flex, c_f, flex_dynamic = calculate_flex(ps, graphs[ps_name], rescheduling_models[ps_name], i_step_start, i_step_end)
            flex_dict[ps_name] = flex
            c_f_dict[ps_name] = c_f
            flex_dynamic_dict[ps_name] = flex_dynamic

        for ca_name, ca in community.community_assets.items():
            flex, c_f, flex_dynamic = calculate_flex(ca, graphs[ca_name], rescheduling_models[ca_name], i_step_start, i_step_end)
            flex_dict[ca_name] = flex
            c_f_dict[ca_name] = c_f
            flex_dynamic_dict[ca_name] = flex_dynamic

        # aggregate rescheduled balance
        rescheduled_balance = aggregate_rescheduled_balance(community, rescheduling_models, i_step_start, i_step_end)

        # activate flexibility
        activation_model = activate_flex(community, i_step_start, i_step_end, rescheduled_balance, strategy, flex_dict, c_f_dict, flex_dynamic_dict, rescheduling_models)

        if activation_model is None:
            changes += 1
            result, n_interval = reduce_invervall(community, n_interval, i_step_start)
            if not result:
                print('Unteres Limit erreicht')
                break
            continue

        # validate
        valid = validate(community, i_start, i_step_start, i_fix_end, activation_model, validation_models, validated_results)
        if not valid:
            changes += 1
            result, n_interval = reduce_invervall(community, n_interval, i_step_start)
            if not result:
                print('Unteres Limit erreicht')
                break
            continue
                
        # fix results
        for ps_name in community.prosumers:
            validated_results[ps_name].extract_partial_results_from_model(validation_models[ps_name], i_step_start, i_fix_end)
        for ca_name in community.community_assets:
            validated_results[ca_name].extract_partial_results_from_model(validation_models[ca_name], i_step_start, i_fix_end)

        # aggregate exports and imports
        aggregate_fixed_exports_imports(community, key, validated_results, i_step_start, i_fix_end)

        # prepare next interval
        i_step_start = int(i_step_start + max(n_interval - n_overlap, 1))
        n_interval = n_interval_default
        n_overlap = n_overlap_default
    
    for ps_name, ps in community.prosumers.items():
        ps._result[key] = validated_results[ps_name]
        ps._last_result_key = key

    for ca_name, ca in community.community_assets.items():
        ca._result[key] = validated_results[ca_name]
        ca._last_result_key = key

def reduce_invervall(community, n_interval, i_step_start):
    #TODO assumes that n_overlap and n_overlap_default is 0
    n_interval_new = int(math.floor(n_interval / 2))
    if n_interval_new <= 0:
        return False, None
    while any(not prosumer._dynamic.has_index(i_step_start + n_interval_new) for prosumer in community.prosumers.values()) or any(not community_asset._dynamic.has_index(i_step_start + n_interval_new) for community_asset in community.community_assets.values()):
        if n_interval_new <= 0:
            return False, None
        n_interval_new -= 1
    return True, n_interval_new
    
def aggregate_rescheduled_balance(community, rescheduling_models, i_start, i_end):
    dynamic = community.dynamic.partial_dynamic(i_start, i_end)
    rescheduled_balance = pd.Series(data=0.0, index=dynamic.time_steps())

    for ps_name, ps in community.prosumers.items():
        rescheduled_data = ps.get_export_import(rescheduling_models[ps_name], dynamic)
        rescheduled_balance += sum(rescheduled_export - rescheduled_import for rescheduled_export, rescheduled_import in rescheduled_data.values())
    for ca_name, ca in community.community_assets.items():
        rescheduled_data = ca.get_export_import(rescheduling_models[ca_name], dynamic)
        rescheduled_balance += sum(rescheduled_export - rescheduled_import for rescheduled_export, rescheduled_import in rescheduled_data.values())
    
    return rescheduled_balance

def calculate_flex(actor, graph, rescheduling_model, i_start, i_end):
    """
    The calc flex method does the following things:
        1) building a graph of the prosumer to search it. The graph is a dictionary that has a structure which
        connects every component (represented by its name as the key in a dictionary) with its neighbours
        2) apply depth first search to the graph and find the endings of it. from the ending it backpropagates to
        the grid. On the way it samples flexibilities while checking efficiency impacts and constraints of all
        components on its way
        3) measures the time in hours until every flexibility offer can be offered. This could be used as an
        incentive of how convenient it is in this moment for the prosumer to actually offer this flexibility

    Parameters
    -------
    time_steps: time steps of current RH-interval
    """

    # building the graph of the prosumer in a way that every component knows to which neighbours
    # it is connected with input/output flows
    dynamic = actor._dynamic.partial_dynamic(i_start, i_end)

    # get the grid name
    try:
        grid_name = [comp for comp in actor._components if actor._components[comp].type == 'ElectricalGrid'][0]
    except:
        print('The grid for starting the dfs search was not found')
        return

    # do the dfs search first with the incoming flows and then with the outgoing flows
    visited = set()

    # make the DFS first for the electricity side, afterwards for heat/demand side
    try:
        elec_flex_from_inv, c_f_inv = dfs(actor, graph, rescheduling_model, visited, grid_name, dynamic, [], 'electricity')
    except TypeError:
        print('There is no flexibility from Storage or PV')

    visited.remove(grid_name)

    columns_list = ['e_dch_dmd', 'e_cha_dmd', 'e_dch_inv', 'e_cha_inv',
                    'flex_pos_dmd', 'flex_pos_inc_dmd', 'flex_pos_dec_dmd',
                    'flex_neg_dmd', 'flex_neg_inc_dmd', 'flex_neg_dec_dmd',
                    'flex_pos_inv', 'flex_pos_inc_inv', 'flex_pos_dec_inv',
                    'flex_neg_inv', 'flex_neg_inc_inv', 'flex_neg_dec_inv']

    flex = pd.DataFrame(data=0.0, columns=columns_list, index=dynamic.time_steps())

    # add flexibility from the inverter side (PV and battery)
    flex['e_dch_inv'] = elec_flex_from_inv['e_dch']
    flex['e_cha_inv'] = elec_flex_from_inv['e_cha']
    flex['flex_pos_inv'] = elec_flex_from_inv['flex_pos']
    flex['flex_pos_inc_inv'] = elec_flex_from_inv['flex_pos_inc']
    flex['flex_pos_dec_inv'] = elec_flex_from_inv['flex_pos_dec']
    flex['flex_neg_inv'] = elec_flex_from_inv['flex_neg']
    flex['flex_neg_inc_inv'] = elec_flex_from_inv['flex_neg_inc']
    flex['flex_neg_dec_inv'] = elec_flex_from_inv['flex_neg_dec']

    # reset the grid values so that the additional flexibility is seen seperately
    grid_comp = actor._components[grid_name]
    for col in grid_comp.flex.columns:
        grid_comp.flex[col].values[:] = 0.0

    try:
        elec_flex_from_dmd, c_f_dmd = dfs(actor, graph, rescheduling_model, visited, grid_name, dynamic, [], 'heat')
        flex_dmd = 1
    except TypeError:
        flex_dmd = 0
        print('There is no flexibility from DMD or Heat')

    # combine correction factors in one dict
    if not flex_dmd:
        c_f_dmd = dict()
        c_f_dmd['c_f_dch'] = pd.Series(data=0, index=dynamic.time_steps())
        c_f_dmd['c_static_dch'] = pd.Series(data=0, index=dynamic.time_steps())
        c_f_dmd['c_f_cha'] = pd.Series(data=0, index=dynamic.time_steps())
        c_f_dmd['c_static_cha'] = pd.Series(data=0, index=dynamic.time_steps())
    c_f_dict = {'elec': c_f_inv, 'heat': c_f_dmd}

    # add (if exists) the flexibility from demand side (heat comm_batt + heat pump)
    if flex_dmd:
        flex['e_dch_dmd'] = elec_flex_from_dmd['e_dch']
        flex['e_cha_dmd'] = elec_flex_from_dmd['e_cha']
        flex['flex_pos_dmd'] = elec_flex_from_dmd['flex_pos']
        flex['flex_pos_inc_dmd'] = elec_flex_from_dmd['flex_pos_inc']
        flex['flex_pos_dec_dmd'] = elec_flex_from_dmd['flex_pos_dec']
        flex['flex_neg_dmd'] = elec_flex_from_dmd['flex_neg']
        flex['flex_neg_inc_dmd'] = elec_flex_from_dmd['flex_neg_inc']
        flex['flex_neg_dec_dmd'] = elec_flex_from_dmd['flex_neg_dec']
    
    return flex, c_f_dict, dynamic

def dfs(actor, graph, results, visited, node, dynamic, act_path, type):
    """
    This function starts a DFS search beginning with the grid node and searches for flexible components.
    Every time it finds a flexible component following steps are done:
        1) Calculate the maximal possible flexibilities from this component
        2) Backpropagate through the graph back to the grid
            - to check if constraints of the components in between are regarded
            - to calculate the efficiency losses of the component to the grid
        3) Finally, the usable flexibility of this component is added to the total amount of flexibility of the prosumer
    Parameters
    ----------
    results: rescheduled results of this prosumer
    visited: list of visited nodes
    node: currently visited node
    time_steps: relevant time steps of this RH-interval
    init_results: initial results. Needed for DF calculation.
    act_path: the path that is currently supervised. Used for backpropagation
    type: can either be heat or electricity. This changes the vertexes beeing locked at.
            Reason: The demand available flexibility on demand side should not interfere with the electricity side.
            We differnciate between comm_batt from heat comm_batt and comm_batt from electric storages.
    """
    if node not in visited:

        # get current node component
        node_key = [comp for comp in actor._components if comp == node][0]
        node_comp = actor._components[node_key]

        # initialize flexibility df in first iteration
        columns_list = ['e_dch', 'flex_pos', 'flex_pos_inc', 'flex_pos_dec',
                        'e_cha', 'flex_neg', 'flex_neg_inc', 'flex_neg_dec']

        node_comp.temp_flex = pd.DataFrame(data=0.0, columns=columns_list, index=dynamic.time_steps())

        # flexible components are not added to the visited comps because they can be accessed through various paths
        if not node_comp.flexible:
            visited.add(node)

        act_path.append(node)
        c_static_df = pd.DataFrame(data=0.0, columns=['c_static_dch', 'c_static_cha'], index=dynamic.time_steps())
        c_f_df = pd.DataFrame(data=1.0, columns=['c_f_dch', 'c_f_cha'], index=dynamic.time_steps())
        c_f_df = pd.concat([c_f_df, c_static_df], axis=1)

        #just go further down the tree if the component is not flexible
        if not node_comp.flexible:
            if type == 'electricity':
                for neighbour in graph[node]['neigh_in']:
                    if neighbour not in visited and neighbour in graph.keys():
                        flex_pre_comp, c_f_pre_comp = dfs(actor, graph, results, visited, neighbour, dynamic, act_path, type)
                        node_comp.temp_flex += flex_pre_comp  # adding the flexibility from the previous object to this component
                        # choose the worst (DCH --> highest CHA--> lowest) correction factor of all neighbours so
                        new_c_f = c_f_pre_comp['c_f_dch']
                        c_f_df['c_f_dch'] = new_c_f.combine(c_f_df['c_f_dch'], max)

                        new_c_f = c_f_pre_comp['c_static_dch']
                        c_f_df['c_static_dch'] = new_c_f.combine(c_f_df['c_static_dch'], max)  # maybe change so that just one factor is used

                        new_c_f = c_f_pre_comp['c_f_cha']
                        c_f_df['c_f_cha'] = new_c_f.combine(c_f_df['c_f_cha'], min)

                        new_c_f = c_f_pre_comp['c_static_cha']
                        c_f_df['c_static_cha'] = new_c_f.combine(c_f_df['c_static_cha'], max)

            elif type == 'heat':
                for neighbour in graph[node]['neigh_out']:
                    if neighbour not in visited and neighbour in graph.keys():
                        flex_pre_comp, c_f_pre_comp = dfs(actor, graph, results, visited, neighbour, dynamic, act_path, type)
                        node_comp.temp_flex += flex_pre_comp  # adding the flexibility from the previous object to this component
                        # choose the worst (DCH --> highest CHA--> lowest) correction factor of all neighbours so
                        new_c_f = c_f_pre_comp['c_f_dch']
                        c_f_df['c_f_dch'] = new_c_f.combine(c_f_df['c_f_dch'], max)

                        new_c_f = c_f_pre_comp['c_static_dch']
                        c_f_df['c_static_dch'] = new_c_f.combine(c_f_df['c_static_dch'],max)  # maybe change so that just one factor is used

                        new_c_f = c_f_pre_comp['c_f_cha']
                        c_f_df['c_f_cha'] = new_c_f.combine(c_f_df['c_f_cha'], min) # max here because COP of HP is larger than 1

                        new_c_f = c_f_pre_comp['c_static_cha']
                        c_f_df['c_static_cha'] = new_c_f.combine(c_f_df['c_static_cha'], max)

        # adjust the correction factors according to components efficiencies
        c_f_df = node_comp.calc_correction_factors(actor._dynamic.time_steps(), c_f_df, results)

        # when there are no neighbours anymore to check, calculate the flexibility of this comp itself
        if node_comp.flexible:
            # calculate the MAXIMAL THEORETICAL FLEXIBILITY in the component class
            node_comp.calc_flex_comp(results, dynamic, actor._result)

        # check limits of non flexible comp because flexible comps intrinsically respect their
        # limits from flexibility calculation
        else:
            input_flows, output_flows = get_planned_flows(results, node_comp, dynamic)
            # Transform the flexibilities to the input side of the inflexible component
            node_comp.adjust_flex_with_efficiency(results, dynamic)
            # Check, if the maximal available flexibility could hit the power limits of this component
            node_comp.check_limits(input_flows, output_flows, results, dynamic)

        # remove node from list if it has no more neighbours
        act_path.remove(node)

        return node_comp.temp_flex, c_f_df

def get_planned_flows(rsl, component, dynamic):
    """
    get initially planned power flows in and out of a prosumers component

    Parameters
    ----------
    rsl: prosumers initial result
    component: component
    time_steps: time steps of this RH-interval

    Returns
    -------
    in_flow: scheduled power flow into component
    out_flow: scheduled power flow out of component
    """
    input_commodity_1, input_commodity_2, output_commoditey_1, output_commoditey_2 = component.get_input_output_commodities()

    in_flow = pd.Series(data=0, index=dynamic.time_steps())

    if input_commodity_1 == ComponentCommodity.ELECTRICITY:
        in_flow += rsl[(component.name, 'input_1')]

    if input_commodity_2 == ComponentCommodity.ELECTRICITY:
        in_flow += rsl[(component.name, 'input_2')]

    out_flow = pd.Series(data=0, index=dynamic.time_steps())

    if output_commoditey_1 == ComponentCommodity.ELECTRICITY:
        out_flow += rsl[(component.name, 'output_1')]

    if output_commoditey_2 == ComponentCommodity.ELECTRICITY:
        out_flow += rsl[(component.name, 'output_2')]

    return in_flow, out_flow

def activate_flex(community, i_start, i_end, rescheduled_balance, strategy, flex_dict, c_f_dict, flex_dynamic_dict, rescheduling_models):
    dynamic = community.dynamic.partial_dynamic(i_start, i_end) # TODO alles ab hier überarbeiten, wenn ich variableninterpolation implementiert habe
    model = build_activation_model(community, dynamic, rescheduled_balance, strategy, flex_dict, c_f_dict, flex_dynamic_dict, rescheduling_models)

    options = dict()
    options['MIPGap'] = 0.02
    options['Presolve'] = 2
    options['TimeLimit'] = 200

    model.solve(options, True)

    if model.is_ok():
        return model
    else:
        return None
    
def build_activation_model(community, dynamic, rescheduled_balance, strategy, flex_dict, c_f_dict, flex_dynamic_dict, rescheduling_models):
    model = OptimizationModel(community.name, dynamic)

    for ps_name, ps in community.prosumers.items():
        add_base_variables(ps_name, ps, model, flex_dict[ps_name], flex_dynamic_dict[ps_name])
        add_base_cons(ps_name, ps, model, flex_dict[ps_name], c_f_dict[ps_name], flex_dynamic_dict[ps_name])

    for ca_name, ca in community.community_assets.items():
        add_base_variables(ca_name, ca, model, flex_dict[ca_name], flex_dynamic_dict[ps_name])
        add_base_cons(ca_name, ca, model, flex_dict[ca_name], c_f_dict[ca_name], flex_dynamic_dict[ps_name])

    add_strategy(community, model, rescheduled_balance, strategy, rescheduling_models)

    return model

def add_base_variables(name, actor, model, flex, flex_dynamic):
    flex_min_inv = resample(flex['flex_neg_inv'], flex_dynamic, model.dynamic)
    flex_max_inv = resample(flex['flex_pos_inv'], flex_dynamic, model.dynamic)

    flex_min_dmd = resample(flex['flex_neg_dmd'], flex_dynamic, model.dynamic)
    flex_max_dmd = resample(flex['flex_pos_dmd'], flex_dynamic, model.dynamic)

    model.add(('flex_' + name,), pyo.Var(model.T, bounds=(None, None)))

    model.add(('flex_pos_inv_' + name,), pyo.Var(model.T, bounds=(0, None)))
    model.add(('flex_neg_inv_' + name,), pyo.Var(model.T, bounds=(0, None)))

    model.add(('flex_pos_dmd_' + name,), pyo.Var(model.T, bounds=(0, None)))
    model.add(('flex_neg_dmd_' + name,), pyo.Var(model.T, bounds=(0, None)))

    def rule(m, t):
        return model.component_dict[('flex_pos_inv_' + name,)][t] <= 0.8 * flex_max_inv[t]
    model.add(('flex_pos_inv_' + name + '_ub',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('flex_neg_inv_' + name,)][t] <= 0.8 * flex_min_inv[t]
    model.add(('flex_neg_inv_' + name + '_ub',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('flex_pos_dmd_' + name,)][t] <= 0.8 * flex_max_dmd[t]
    model.add(('flex_pos_dmd_' + name + '_ub',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('flex_neg_dmd_' + name,)][t] <= 0.8 * flex_min_dmd[t]
    model.add(('flex_neg_dmd_' + name + '_ub',), pyo.Constraint(model.T, rule = rule))

    # there are just these two binary variables for the whole prosumer
    # because we can not use negative flex from dmd and positive flex from elec. So they will be the same
    model.add(('z_flex_pos_inv_' + name,), pyo.Var(model.T, domain=pyo.Binary))
    model.add(('z_flex_neg_inv_' + name,), pyo.Var(model.T, domain=pyo.Binary))
    model.add(('z_flex_pos_dmd_' + name,), pyo.Var(model.T, domain=pyo.Binary))
    model.add(('z_flex_neg_dmd_' + name,), pyo.Var(model.T, domain=pyo.Binary))

def add_base_cons(name, actor, model, flex, c_f_unsampled, flex_dynamic):
    e_dch_inv = resample(flex['e_dch_inv'], flex_dynamic, model.dynamic)
    e_cha_inv = resample(flex['e_cha_inv'], flex_dynamic, model.dynamic)

    e_dch_dmd = resample(flex['e_dch_dmd'], flex_dynamic, model.dynamic)
    e_cha_dmd = resample(flex['e_cha_dmd'], flex_dynamic, model.dynamic)

    c_f = dict()
    c_f['elec'] = dict()
    c_f['elec']['c_f_dch'] = resample(c_f_unsampled['elec']['c_f_dch'], flex_dynamic, model.dynamic)
    c_f['elec']['c_static_dch'] = resample(c_f_unsampled['elec']['c_static_dch'], flex_dynamic, model.dynamic)
    c_f['elec']['c_f_cha'] = resample(c_f_unsampled['elec']['c_f_cha'], flex_dynamic, model.dynamic)
    c_f['elec']['c_static_cha'] = resample(c_f_unsampled['elec']['c_static_cha'], flex_dynamic, model.dynamic)
    c_f['heat'] = dict()
    c_f['heat']['c_f_dch'] = resample(c_f_unsampled['heat']['c_f_dch'], flex_dynamic, model.dynamic)
    c_f['heat']['c_static_dch'] = resample(c_f_unsampled['heat']['c_static_dch'], flex_dynamic, model.dynamic)
    c_f['heat']['c_f_cha'] = resample(c_f_unsampled['heat']['c_f_cha'], flex_dynamic, model.dynamic)
    c_f['heat']['c_static_cha'] = resample(c_f_unsampled['heat']['c_static_cha'], flex_dynamic, model.dynamic)

    bigM = 50

    def rule(m, t):
        return model.component_dict[('flex_' + name,)][t] == model.component_dict[('flex_pos_inv_' + name,)][t] - model.component_dict[('flex_neg_inv_' + name,)][t] + model.component_dict[('flex_pos_dmd_' + name,)][t] - model.component_dict[('flex_neg_dmd_' + name,)][t]
    model.add(('flex_' + name + '_sum',), pyo.Constraint(model.T, rule = rule))

    # if z_flex_sign == 1 --> flex_pos < bigM; flex_neg = 0
    def rule(m, t):
        return model.component_dict[('flex_pos_inv_' + name,)][t] <= model.component_dict[('z_flex_pos_inv_' + name,)][t] * bigM
    model.add(('z_flex_pos_inv_' + name + '_1',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('flex_pos_inv_' + name,)][t] >= model.component_dict[('z_flex_pos_inv_' + name,)][t] * 0.001
    model.add(('z_flex_pos_inv_' + name + '_2',), pyo.Constraint(model.T, rule = rule))


    def rule(m, t):
        return model.component_dict[('flex_neg_inv_' + name,)][t] <= model.component_dict[('z_flex_neg_inv_' + name,)][t] * bigM
    model.add(('z_flex_neg_inv_' + name + '_1',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('flex_neg_inv_' + name,)][t] >= model.component_dict[('z_flex_neg_inv_' + name,)][t] * 0.001
    model.add(('z_flex_neg_inv_' + name + '_2',), pyo.Constraint(model.T, rule = rule))


    def rule(m, t):
        return model.component_dict[('flex_pos_dmd_' + name,)][t] <= model.component_dict[('z_flex_pos_dmd_' + name,)][t] * bigM
    model.add(('z_flex_pos_dmd_' + name + '_1',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('flex_pos_dmd_' + name,)][t] >= model.component_dict[('z_flex_pos_dmd_' + name,)][t] * 0.001
    model.add(('z_flex_pos_dmd_' + name + '_2',), pyo.Constraint(model.T, rule = rule))


    def rule(m, t):
        return model.component_dict[('flex_neg_dmd_' + name,)][t] <= model.component_dict[('z_flex_neg_dmd_' + name,)][t] * bigM
    model.add(('z_flex_neg_dmd_' + name + '_1',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('flex_neg_dmd_' + name,)][t] >= model.component_dict[('z_flex_neg_dmd_' + name,)][t] * 0.001
    model.add(('z_flex_neg_dmd_' + name + '_2',), pyo.Constraint(model.T, rule = rule))

    # make sure that just one flexibility is used at a time
    def rule(m, t):
        return model.component_dict[('z_flex_pos_inv_' + name,)][t] + model.component_dict[('z_flex_neg_inv_' + name,)][t] <= 1
    model.add(('z_flex_pos_inv_' + name + '_3',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('z_flex_pos_dmd_' + name,)][t] + model.component_dict[('z_flex_neg_dmd_' + name,)][t] <= 1
    model.add(('z_flex_pos_dmd_' + name + '_3',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('z_flex_pos_inv_' + name,)][t] + model.component_dict[('z_flex_neg_dmd_' + name,)][t] <= 1
    model.add(('z_flex_pos_inv_' + name + '_4',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('z_flex_pos_dmd_' + name,)][t] + model.component_dict[('z_flex_neg_inv_' + name,)][t] <= 1
    model.add(('z_flex_pos_dmd_' + name + '_4',), pyo.Constraint(model.T, rule = rule))

    # upper energy boundary for energy flexibility
    def rule(m, t):
        return pyo.quicksum(
            model.component_dict[('flex_pos_inv_' + name,)][t_old] * c_f['elec']['c_f_dch'][t_old] +
            model.component_dict[('z_flex_pos_inv_' + name,)][t_old] * c_f['elec']['c_static_dch'][t_old] * model.step_size(t_old)
            for t_old in range(model.T.first(), model.T.first() + model.T.ord(t))) - pyo.quicksum(
            model.component_dict[('flex_neg_inv_' + name,)][t_old] * c_f['elec']['c_f_cha'][t_old] -
            model.component_dict[('z_flex_neg_inv_' + name,)][t_old] * c_f['elec']['c_static_cha'][t_old] * model.step_size(t_old)
            for t_old in range(model.T.first(), model.T.first() + model.T.ord(t))) <= 1 * e_dch_inv[t]
    model.add(('flex_inv_' + name + '_ub',), pyo.Constraint(model.T, rule = rule))

    # lower energy boundary for energy flexibility
    def rule(m, t):
        return pyo.quicksum(
            model.component_dict[('flex_pos_inv_' + name,)][t_old] * c_f['elec']['c_f_dch'][t_old] +
            model.component_dict[('z_flex_pos_inv_' + name,)][t_old] * c_f['elec']['c_static_dch'][t_old] * model.step_size(t_old)
            for t_old in range(model.T.first(), model.T.first() + model.T.ord(t))) - pyo.quicksum(
            model.component_dict[('flex_neg_inv_' + name,)][t_old] * c_f['elec']['c_f_cha'][t_old] -
            model.component_dict[('z_flex_neg_inv_' + name,)][t_old] * c_f['elec']['c_static_cha'][t_old] * model.step_size(t_old)
            for t_old in range(model.T.first(), model.T.first() + model.T.ord(t))) >= -1 * e_cha_inv[t]
    model.add(('flex_inv_' + name + '_lb',), pyo.Constraint(model.T, rule = rule))

    if e_cha_dmd.any():
        # upper energy boundary for energy flexibility
        def rule(m, t):
            return pyo.quicksum(
                model.component_dict[('flex_pos_dmd_' + name,)][t_old] * c_f['heat']['c_f_dch'][t_old] +
                model.component_dict[('z_flex_pos_dmd_' + name,)][t_old] * c_f['heat']['c_static_dch'][t_old] * model.step_size(t_old)
                for t_old in range(model.T.first(), model.T.first() + model.T.ord(t))) - pyo.quicksum(
                model.component_dict[('flex_neg_dmd_' + name,)][t_old] * c_f['heat']['c_f_cha'][t_old] -
                model.component_dict[('z_flex_neg_dmd_' + name,)][t_old] * c_f['heat']['c_static_cha'][t_old] * model.step_size(t_old)
                for t_old in range(model.T.first(), model.T.first() + model.T.ord(t))) <= 1 * e_dch_dmd[t]
        model.add(('flex_dmd_' + name + '_ub',), pyo.Constraint(model.T, rule = rule))

        # lower energy boundary for energy flexibility
        def rule(m, t):
            return pyo.quicksum(
                model.component_dict[('flex_pos_dmd_' + name,)][t_old] * c_f['heat']['c_f_dch'][t_old] +
                model.component_dict[('z_flex_pos_dmd_' + name,)][t_old] * c_f['heat']['c_static_dch'][t_old] * model.step_size(t_old)
                for t_old in range(model.T.first(), model.T.first() + model.T.ord(t))) - pyo.quicksum(
                model.component_dict[('flex_neg_dmd_' + name,)][t_old] * c_f['heat']['c_f_cha'][t_old] -
                model.component_dict[('z_flex_neg_dmd_' + name,)][t_old] * c_f['heat']['c_static_cha'][t_old] * model.step_size(t_old)
                for t_old in range(model.T.first(), model.T.first() + model.T.ord(t))) >= -1 * e_cha_dmd[t]
        model.add(('flex_dmd_' + name + '_lb',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return pyo.quicksum(
                model.component_dict[('flex_pos_dmd_' + name,)][t_old] * c_f['heat']['c_f_dch'][t_old] +
                model.component_dict[('z_flex_pos_dmd_' + name,)][t_old] * c_f['heat']['c_static_dch'][t_old] * model.step_size(t_old)
                for t_old in range(model.T.first(), model.T.first() + model.T.ord(t))) <= pyo.quicksum(
                model.component_dict[('flex_neg_dmd_' + name,)][t_old] * c_f['heat']['c_f_cha'][t_old] -
                model.component_dict[('z_flex_neg_dmd_' + name,)][t_old] * c_f['heat']['c_static_cha'][t_old] * model.step_size(t_old)
                for t_old in range(model.T.first(), model.T.first() + model.T.ord(t)))
        model.add(('flex_dmd_' + name + '_1',), pyo.Constraint(model.T, rule = rule))
        
    else:
        def rule(m, t):
            return model.component_dict[('flex_pos_dmd_' + name,)][t] == 0
        model.add(('flex_dmd_' + name + '_1',), pyo.Constraint(model.T, rule = rule))
        
        def rule(m, t):
            return model.component_dict[('flex_neg_dmd_' + name,)][t] == 0
        model.add(('flex_dmd_' + name + '_2',), pyo.Constraint(model.T, rule = rule))

def add_strategy(community, model, rescheduled_balance, strategy, rescheduling_models):
    if 'max_operational_profit' == strategy:
        # Energy exported by all actors of the community
        model.add(('agg_export',), pyo.Var(model.T, bounds=(0, None)))
        # Energy imported by all actors of the community
        model.add(('agg_import',), pyo.Var(model.T, bounds=(0, None)))
        # Part of the energy imported by all actors that is fullfilled by part of the energy exported by all actors in the community
        model.add(('internal_exchange',), pyo.Var(model.T, bounds=(0, None)))
        # Energy exported by the community to the external grid
        model.add(('community_export',), pyo.Var(model.T, bounds=(0, None)))
        # Energy imported by the community from the extrnal grid
        model.add(('community_import',), pyo.Var(model.T, bounds=(0, None)))

        bigM = 1000

        for ps_name, ps in community.prosumers.items():
            model.add(('z_' + ps_name,), pyo.Var(model.T, domain=pyo.Binary))
            model.add(('export_' + ps_name,), pyo.Var(model.T, bounds=(0, None)))
            model.add(('import_' + ps_name,), pyo.Var(model.T, bounds=(0, None)))

            grid_name = [comp for comp in ps._components if ps._components[comp].type == 'ElectricalGrid'][0]
            ps_export, ps_import = ps.get_export_import(rescheduling_models[ps_name], model.dynamic)[grid_name]

            # z_ps == 0, if outputs > inputs
            # z_ps == 1, if inputs > outputs
            def rule(m, t):
                return model.component_dict[('z_' + ps_name,)][t] <= 1 - (ps_export[t] - ps_import[t] + model.component_dict[('flex_' + ps_name,)][t]) / bigM
            model.add(('z_' + ps_name + '_1',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('z_' + ps_name,)][t] >= -1 * (ps_export[t] - ps_import[t] + model.component_dict[('flex_' + ps_name,)][t]) / bigM
            model.add(('z_' + ps_name + '_2',), pyo.Constraint(model.T, rule = rule))

            # output_ps = output_ps - input_ps + flex if outputs + flex > inputs (z_ps= 0)
            # output_ps = 0 if z_ps = 1
            # input_ps = output_ps - input_ps + flex if inputs - flex > outputs (z_ps = 1)
            # input_ps = 0 if z_ps = 0
            def rule(m, t):
                return model.component_dict[('export_' + ps_name,)][t] <= (ps_export[t] - ps_import[t] + model.component_dict[('flex_' + ps_name,)][t]) + (model.component_dict[('z_' + ps_name,)][t]) * bigM
            model.add(('z_' + ps_name + '_3',), pyo.Constraint(model.T, rule = rule))
            
            def rule(m, t):
                return model.component_dict[('export_' + ps_name,)][t] >= (ps_export[t] - ps_import[t] + model.component_dict[('flex_' + ps_name,)][t]) - (model.component_dict[('z_' + ps_name,)][t]) * bigM
            model.add(('z_' + ps_name + '_4',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('import_' + ps_name,)][t] <= -1 * (ps_export[t] - ps_import[t] + model.component_dict[('flex_' + ps_name,)][t]) + (1 - model.component_dict[('z_' + ps_name,)][t]) * bigM
            model.add(('z_' + ps_name + '_5',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('import_' + ps_name,)][t] >= -1 * (ps_export[t] - ps_import[t] + model.component_dict[('flex_' + ps_name,)][t]) - (1 - model.component_dict[('z_' + ps_name,)][t]) * bigM
            model.add(('z_' + ps_name + '_6',), pyo.Constraint(model.T, rule = rule))

            # input_ps == 0 if z_ps == 0
            def rule(m, t):
                return model.component_dict[('import_' + ps_name,)][t] >= -1 * bigM * model.component_dict[('z_' + ps_name,)][t]
            model.add(('z_' + ps_name + '_7',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('import_' + ps_name,)][t] <= bigM * model.component_dict[('z_' + ps_name,)][t]
            model.add(('z_' + ps_name + '_8',), pyo.Constraint(model.T, rule = rule))

            # output_ps == 0 if z_ps == 1
            def rule(m, t):
                return model.component_dict[('export_' + ps_name,)][t] >= -1 * bigM * (1 - model.component_dict[('z_' + ps_name,)][t])
            model.add(('z_' + ps_name + '_9',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('export_' + ps_name,)][t] <= bigM * (1 - model.component_dict[('z_' + ps_name,)][t])
            model.add(('z_' + ps_name + '_10',), pyo.Constraint(model.T, rule = rule))

        for ca_name, ca in community.community_assets.items():
            model.add(('z_' + ca_name,), pyo.Var(model.T, domain=pyo.Binary))
            model.add(('export_' + ca_name,), pyo.Var(model.T, bounds=(0, None)))
            model.add(('import_' + ca_name,), pyo.Var(model.T, bounds=(0, None)))

            grid_name = [comp for comp in ca._components if ca._components[comp].type == 'ElectricalGrid'][0]
            ca_export, ca_import = ca.get_export_import(rescheduling_models[ca_name], model.dynamic)[grid_name]

            # z_ca == 0, if outputs > inputs
            # z_ca == 1, if inputs > outputs
            def rule(m, t):
                return model.component_dict[('z_' + ca_name,)][t] <= 1 - (ca_export[t] - ca_import[t] + model.component_dict[('flex_' + ca_name,)][t]) / bigM
            model.add(('z_' + ca_name + '_1',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('z_' + ca_name,)][t] >= -1 * (ca_export[t] - ca_import[t] + model.component_dict[('flex_' + ca_name,)][t]) / bigM
            model.add(('z_' + ca_name + '_2',), pyo.Constraint(model.T, rule = rule))

            # output_ca = output_ca - input_ca + flex if outputs + flex > inputs (z_ca= 0)
            # output_ca = 0 if z_ca = 1
            # input_ca = output_ca - input_ca + flex if inputs - flex > outputs (z_ca = 1)
            # input_ca = 0 if z_ca = 0
            def rule(m, t):
                return model.component_dict[('export_' + ca_name,)][t] <= (ca_export[t] - ca_import[t] + model.component_dict[('flex_' + ca_name,)][t]) + (model.component_dict[('z_' + ca_name,)][t]) * bigM
            model.add(('z_' + ca_name + '_3',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('export_' + ca_name,)][t] >= (ca_export[t] - ca_import[t] + model.component_dict[('flex_' + ca_name,)][t]) - (model.component_dict[('z_' + ca_name,)][t]) * bigM
            model.add(('z_' + ca_name + '_4',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('import_' + ca_name,)][t] <= -1 * (ca_export[t] - ca_import[t] + model.component_dict[('flex_' + ca_name,)][t]) + (1 - model.component_dict[('z_' + ca_name,)][t]) * bigM
            model.add(('z_' + ca_name + '_5',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('import_' + ca_name,)][t] >= -1 * (ca_export[t] - ca_import[t] + model.component_dict[('flex_' + ca_name,)][t]) - (1 - model.component_dict[('z_' + ca_name,)][t]) * bigM
            model.add(('z_' + ca_name + '_6',), pyo.Constraint(model.T, rule = rule))

            # input_ca == 0 if z_ca == 0
            def rule(m, t):
                return model.component_dict[('import_' + ca_name,)][t] >= -1 * bigM * model.component_dict[('z_' + ca_name,)][t]
            model.add(('z_' + ca_name + '_7',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('import_' + ca_name,)][t] <= bigM * model.component_dict[('z_' + ca_name,)][t]
            model.add(('z_' + ca_name + '_8',), pyo.Constraint(model.T, rule = rule))

            # output_ca == 0 if z_ca == 1
            def rule(m, t):
                return model.component_dict[('export_' + ca_name,)][t] >= -1 * bigM * (1 - model.component_dict[('z_' + ca_name,)][t])
            model.add(('z_' + ca_name + '_9',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[('export_' + ca_name,)][t] <= bigM * (1 - model.component_dict[('z_' + ca_name,)][t])
            model.add(('z_' + ca_name + '_10',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[('agg_export',)][t] == pyo.quicksum(model.component_dict[('export_' + ps_name,)][t] for ps_name in community.prosumers) + pyo.quicksum(model.component_dict[('export_' + ca_name,)][t] for ca_name in community.community_assets)
        model.add(('outputs_cons',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[('agg_import',)][t] == pyo.quicksum(model.component_dict[('import_' + ps_name,)][t] for ps_name in community.prosumers) + pyo.quicksum(model.component_dict[('import_' + ca_name,)][t] for ca_name in community.community_assets)
        model.add(('intputs_cons',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[('internal_exchange',)][t] <= model.component_dict[('agg_export',)][t]
        model.add(('internal_exchange_ub_output',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[('internal_exchange',)][t] <= model.component_dict[('agg_import',)][t]
        model.add(('internal_exchange_ub_input',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[('community_export',)][t] == model.component_dict[('agg_export',)][t] - model.component_dict[('internal_exchange',)][t]
        model.add(('community_export_cons',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[('community_import',)][t] == model.component_dict[('agg_import',)][t] - model.component_dict[('internal_exchange',)][t]
        model.add(('community_import_cons',), pyo.Constraint(model.T, rule = rule))

        # Peak Shaving
        model.add(('peak_community_import',), pyo.Var())

        def rule(m, t):
            return model.component_dict[('community_import',)][t] <= model.component_dict[('peak_community_import',)]
        model.add(('peak_community_import_cons',), pyo.Constraint(model.T, rule = rule))

        # The sum of the external demands after DF activations are not allowed to be higher than before.
        # This aims to prohibit arbitrage trading.
        community_import = rescheduled_balance[model.time_steps] * -1
        community_import.loc[community_import < 0] = 0
        total_community_import = community_import.sum()

        model.add(('total_community_import_cons',), pyo.Constraint(expr = pyo.quicksum(model.component_dict[('community_import',)][t] for t in model.time_steps) <= total_community_import))

        # factor that converts the simulation to ONE year
        annual_factor = 8760.0 / sum(model.dynamic.step_size_p(position) for position in range(model.dynamic.number_of_steps()))

        model.block.f1 = pyo.Var()
        elec_price_int = resample(community.configuration['elec_price_int'], community.dynamic, model.dynamic)
        elec_price_ext = resample(community.configuration['elec_price_ext'], community.dynamic, model.dynamic)
        injection_price = resample(community.configuration['injection_price'], community.dynamic, model.dynamic)
        model.block.C_f1 = pyo.Constraint(expr=model.block.f1 == (- pyo.quicksum(model.component_dict[('internal_exchange',)][t] * elec_price_int[t] * model.step_size(t) for t in model.time_steps)
                                                                  - pyo.quicksum(model.component_dict[('community_import',)][t] * elec_price_ext[t] * model.step_size(t) for t in model.time_steps)
                                                                  + pyo.quicksum(model.component_dict[('internal_exchange',)][t] * injection_price[t] * model.step_size(t) for t in model.time_steps)
                                                                  + pyo.quicksum(model.component_dict[('community_export',)][t] * injection_price[t] * model.step_size(t) for t in model.time_steps)
                                                                 ) * annual_factor
                                                               - model.component_dict[('peak_community_import',)] * community.configuration['network_usage_capacity_fee'])

        model.block.O_f1 = pyo.Objective(expr=model.block.f1, sense=pyo.maximize)
    if 'max_wholesale_profit' == strategy:
        model.add(('agg_balance',), pyo.Var(model.T, bounds=(None, None)))

        def rule(m, t):
            return model.component_dict[('agg_balance',)][t] == rescheduled_balance[t] + pyo.quicksum(model.component_dict[('flex_' + ps_name,)][t] for ps_name in community.prosumers) + pyo.quicksum(model.component_dict[('flex_' + cc_name,)][t] for cc_name in community.community_assets)
        model.add(('agg_balance_cons',), pyo.Constraint(model.T, rule = rule))
       
        model.block.f1 = pyo.Var()
        model.block.C_f1 = pyo.Constraint(expr=model.block.f1 == pyo.quicksum(model.component_dict[('agg_balance',)][t] * community.configuration['spot_price'][t] for t in model.time_steps))

        model.block.O_f1 = pyo.Objective(expr=model.block.f1, sense=pyo.maximize)
    
def validate(community, i_start, i_start_validation, i_end_validation, activation_model, validation_models, validated_results):
    dynamic_validation = community.dynamic.partial_dynamic(i_start_validation, i_end_validation)
    time_steps_validation = community.dynamic.indices_within(i_start_validation, i_end_validation)

    for ps_name, ps in community.prosumers.items():
        dynamic_ps = ps._dynamic.partial_dynamic(i_start_validation, i_end_validation)
        grid_name = [comp for comp in ps._components if ps._components[comp].type == 'ElectricalGrid'][0]
        grid_fix_values = {component.name: (ps._result[ps._last_result_key][(component.name, 'input_1')], ps._result[ps._last_result_key][(component.name, 'output_1')]) for component in ps.get_components(kind=ComponentKind.GRID, commodity=ComponentCommodity.ELECTRICITY) if component.name != grid_name}
        grid_fix_values[grid_name] = (resample(activation_model[('export_' + ps_name,)][time_steps_validation], dynamic_validation, dynamic_ps), resample(activation_model[('import_' + ps_name,)][time_steps_validation], dynamic_validation, dynamic_ps))
        component_sizes = {component.name: ps._result[ps._last_result_key][(component.name, 'capacity')] for component in ps.get_components()}
        storage_slack_values = {component.name: ps._result[ps._last_result_key][(component.name, 'energy')] for component in ps.get_components(kind=ComponentKind.STORAGE)}
        configuration = {
            'fix_sizing': {
                'values': component_sizes
            },
            'consumption_slack': {
                'commodity': [ComponentCommodity.COLD, ComponentCommodity.HEAT],
                'strategy_factor': 10000
            },
            'grid_fix': {
                'commodity': ComponentCommodity.ELECTRICITY,
                'values': grid_fix_values
            },
            'storage_slack': {
                'commodity': ComponentCommodity.ALL,
                'values': storage_slack_values,
                'strategy_factor': 10
            },
            'storage_boundaries': {
                'commodity': ComponentCommodity.ALL,
                'min': 0,
                'max': 1
            }
        }
        if i_start != i_start_validation:
            initial_time_step = ps._dynamic.index_of(ps._dynamic.position_of(i_start_validation) - 1)
            storage_connect_values = {component.name: validated_results[ps_name][(component.name, 'energy')][initial_time_step] for component in ps.get_components(kind=ComponentKind.STORAGE)}
            configuration['storage_connect'] = {
                'commodity': ComponentCommodity.ALL,
                'values': storage_connect_values
            }
        validation_model = ps.optimize(configuration, i_start_validation, i_end_validation)
        if validation_model is None:
            return False
        else:
            validation_models[ps_name] = validation_model
        
    for ca_name, ca in community.community_assets.items():
        dynamic_ca = ca._dynamic.partial_dynamic(i_start_validation, i_end_validation)
        grid_name = [comp for comp in ca._components if ca._components[comp].type == 'ElectricalGrid'][0]
        grid_fix_values = {component.name: (ca._result[ca._last_result_key][(component.name, 'input_1')], ca._result[ca._last_result_key][(component.name, 'output_1')]) for component in ca.get_components(kind=ComponentKind.GRID, commodity=ComponentCommodity.ELECTRICITY) if component.name != grid_name}
        grid_fix_values[grid_name] = (resample(activation_model[('export_' + ca_name,)][time_steps_validation], dynamic_validation, dynamic_ca), resample(activation_model[('import_' + ca_name,)][time_steps_validation], dynamic_validation, dynamic_ca))
        component_sizes = {component.name: ca._result[ca._last_result_key][(component.name, 'capacity')] for component in ca.get_components()}
        storage_slack_values = {component.name: ca._result[ca._last_result_key][(component.name, 'energy')] for component in ca.get_components(kind=ComponentKind.STORAGE)}
        configuration = {
            'fix_sizing': {
                'values': component_sizes
            },
            'consumption_slack': {
                'commodity': [ComponentCommodity.COLD, ComponentCommodity.HEAT],
                'strategy_factor': 10000
            },
            'grid_fix': {
                'commodity': ComponentCommodity.ELECTRICITY,
                'values': grid_fix_values
            },
            'storage_slack': {
                'commodity': ComponentCommodity.ALL,
                'values': storage_slack_values,
                'strategy_factor': 10
            },
            'storage_boundaries': {
                'commodity': ComponentCommodity.ALL,
                'min': 0,
                'max': 1
            }
        }
        if i_start != i_start_validation:
            initial_time_step = ca._dynamic.index_of(ca._dynamic.position_of(i_start_validation) - 1)
            storage_connect_values = {component.name: validated_results[ca_name][(component.name, 'energy')][initial_time_step] for component in ca.get_components(kind=ComponentKind.STORAGE)}
            configuration['storage_connect'] = {
                'commodity': ComponentCommodity.ALL,
                'values': storage_connect_values
            }
        validation_model = ca.optimize(configuration, i_start_validation, i_end_validation)
        if validation_model is None:
            return False
        else:
            validation_models[ca_name] = validation_model
        
    return True
        
def aggregate_fixed_exports_imports(community, key, validated_results, i_start, i_end_fix):
    dynamic_fix = community.dynamic.partial_dynamic(i_start, i_end_fix)
    
    agg_validated_export = pd.Series(0.0, index=dynamic_fix.time_steps())
    agg_validated_import = pd.Series(0.0, index=dynamic_fix.time_steps())
    
    for ps_name, ps in community.prosumers.items():
        validated_data = ps.get_export_import(validated_results[ps_name], dynamic_fix)
        for validated_export, validated_import in validated_data.values():
            agg_validated_export += validated_export
            agg_validated_import += validated_import
    
    for ca_name, ca in community.community_assets.items():
        validated_data = ca.get_export_import(validated_results[ca_name], dynamic_fix)
        for validated_export, validated_import in validated_data.values():
            agg_validated_export += validated_export
            agg_validated_import += validated_import
    
    community.result[key]['agg_balance'][dynamic_fix.time_steps()] = agg_validated_export - agg_validated_import
    community.result[key]['agg_export'][dynamic_fix.time_steps()]  = agg_validated_export
    community.result[key]['agg_import'][dynamic_fix.time_steps()]  = agg_validated_import
    community.result[key]['internal_exchange'][dynamic_fix.time_steps()]  = pd.concat([agg_validated_export, agg_validated_import], axis=1).min(axis=1)
