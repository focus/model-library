"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import pandas as pd
import os
from datetime import timedelta
from Model_Library.Component.model.EMS_components.Coordinator import implement_sizing_strategy
from Model_Library.OptimizationModel import OptimizationModel, EntityResult
from Tooling.dynamics.Dynamic import resample
from Model_Library.flexibility import flexibility_activation

class Community:

    def __init__(self, name, configuration, prosumers, community_assets, profiles, dynamic):

        self.name = name
        self.configuration = configuration
        self.prosumers = prosumers
        self.community_assets = community_assets

        wholesale_price = resample(profiles[self.configuration['wholesale_price']][0], profiles[self.configuration['wholesale_price']][1], dynamic)
        self.configuration['elec_price_int'] = (wholesale_price + self.configuration['network_usage_energy_fee'] + self.configuration['levies_int'] + self.configuration['concession'] + self.configuration['electricity_tax_int']) * (1 + self.configuration['VAT'])
        self.configuration['elec_price_ext'] = (wholesale_price + self.configuration['network_usage_energy_fee'] + self.configuration['levies_ext'] + self.configuration['concession'] + self.configuration['electricity_tax_ext']) * (1 + self.configuration['VAT'])
        self.configuration['injection_price'] = resample(profiles[self.configuration['injection_price']][0], profiles[self.configuration['injection_price']][1], dynamic)
        
        self.dynamic = dynamic

        self.result = {'initial': EntityResult(dynamic, [('agg_balance', 'T'), ('agg_export', 'T'), ('agg_import', 'T'), ('internal_exchange', 'T')])}

        agg_export = pd.Series(0.0, index=dynamic.time_steps())
        agg_import = pd.Series(0.0, index=dynamic.time_steps())
        for ps in self.prosumers.values():
            ps_data = ps.get_export_import(ps._result[ps._last_result_key], self.dynamic)
            for ps_export, ps_import in ps_data.values():
                agg_export += ps_export
                agg_import += ps_import
        self.result['initial']['agg_balance'] = agg_export - agg_import
        self.result['initial']['agg_export'] = agg_export
        self.result['initial']['agg_import'] = agg_import
        self.result['initial']['internal_exchange'] = pd.concat([agg_export, agg_import], axis=1).min(axis=1)
        self.last_result_key = 'initial'

    def optimize_sizing(self, key, strategy_name):
        if not self.community_assets:
            return
        """
        Sizing the community assets.
        1) All Prosumer objects containing CAs are iteratively called and
        an aggregated mathematical model is build. This aggregated model is necessary because the CAs do not act
        independently as a prosumer but collectively.
        2) pass the CA strategy to the coordinator so it can implment the objective function
        3) start optimization
        4) add information to community data
        5) update data for coordinator

        Parameters
        ----------
        time_steps: time steps of the whole time horizon
        """

        self.build_sizing_model(strategy_name)

        options = dict()
        options['MIPGap'] = 0.02
        options['Presolve'] = 2
        options['TimeLimit'] = 200

        self._model_sizing.solve(options, False)

        if self._model_sizing.is_ok():
            # add the result to the community assets
            for ca_name, ca in self.community_assets.items():
                # no resampling necessary, because the model has the same dynamic as the prosumer
                ca._result[key] = ca.get_empty_entity_result()
                ca._result[key].extract_results_from_model(self._model_sizing.blocks[(ca_name,)])
                ca._last_result_key = key
        else:
            print('ERROR: The model is infeasible or unbounded: no optimal solution found')

        self.result[key] = EntityResult(self.dynamic, [('agg_balance', 'T'), ('agg_export', 'T'), ('agg_import', 'T'), ('internal_exchange', 'T')])

        agg_export = self.result[self.last_result_key]['agg_export'].copy()
        agg_import = self.result[self.last_result_key]['agg_import'].copy()
        for ca in self.community_assets.values():
            ca_data = ca.get_export_import(ca._result[key], self.dynamic)
            for ca_export, ca_import in ca_data.values():
                agg_export += ca_export
                agg_import += ca_import
        self.result[key]['agg_balance'] = agg_export - agg_import
        self.result[key]['agg_export'] = agg_export
        self.result[key]['agg_import'] = agg_import
        self.result[key]['internal_exchange'] = pd.concat([agg_export, agg_import], axis=1).min(axis=1)
        self.last_result_key = key

    def build_sizing_model(self, strategy_name):
        self._model_sizing = OptimizationModel(self.name, self.dynamic)

        for ca in self.community_assets.values():

            ca.build_sizing_model(self._model_sizing)

        implement_sizing_strategy(self, self._model_sizing, strategy_name)

    def optimize_operation(self, key, community_operation_strategy):
        flexibility_activation(self, key, community_operation_strategy)
        return

    def analyze_results(self):
        """
        Analyze community results with economic and technical indices.
        """

        # The factor that converts the simulation to ONE year
        annual_factor = timedelta(days=365) / timedelta(hours=sum(self.dynamic.step_size_p(position) for position in range(self.dynamic.number_of_steps())))

        self.analysis = dict()
        self.analysis['economical'] = dict()
        self.analysis['technical'] = dict()
        self.annuity_dict = dict()

        for key in self.result:
            print('---------------------WELFARE ' + key + '---------------------')

            community_import = self.result[key]['agg_import'] - self.result[key]['internal_exchange']
            community_export = self.result[key]['agg_export'] - self.result[key]['internal_exchange']
            total_community_import = sum(community_import)
            peak_community_import = community_import.max()
            peak_community_import_costs = peak_community_import * self.configuration['network_usage_capacity_fee']

            try:
                t_fullload = total_community_import / peak_community_import
                print('Fullload hours ' + key + ': ' + str(t_fullload))
            except ZeroDivisionError:
                t_fullload = float('inf') # to get the higher price

            internal_costs = self.result[key]['internal_exchange'] * self.dynamic.step_sizes() * self.configuration['elec_price_int'].values
            external_costs = community_import * self.dynamic.step_sizes() * self.configuration['elec_price_ext'].values

            internal_revenue = self.result[key]['internal_exchange'] * self.dynamic.step_sizes() * self.configuration['injection_price'].values
            external_revenue = community_export * self.dynamic.step_sizes() * self.configuration['injection_price'].values

            inv_costs = 0
            if len(self.community_assets) > 0 and key != 'initial':
                for var in self._model_sizing.component_dict:
                    if len(var) == 3 and var[0] in self.community_assets.keys() and var[2] == 'capital_cost':
                        inv_costs += self._model_sizing.component_dict[var].value

            annuity = -inv_costs + (internal_revenue.sum() + external_revenue.sum() - internal_costs.sum() - external_costs.sum()) * annual_factor - peak_community_import_costs

            self.analysis['economical'][key] = {'inv costs': inv_costs,
                                                'peak power costs': peak_community_import_costs,
                                                'internal costs': internal_costs.sum(),
                                                'external costs': external_costs.sum(),
                                                'internal revenue': internal_revenue.sum(),
                                                'external revenue': external_revenue.sum(),
                                                'annuity': annuity}

            print('--------------------TECHNICAL ' + key + '------------------------------')

            total_community_export = community_export.sum()
            peak_community_export = community_export.max()
            try:
                energy_balance_index = 1 - (total_community_import + total_community_export) / \
                                       (self.result[key]['agg_import'].sum() + self.result[key]['agg_export'].sum())

                self_sufficiency_index = 1 - (total_community_import / self.result[key]['agg_import'].sum())  # same as dividing internal exchange with aggregated demand
            except ZeroDivisionError:
                energy_balance_index = float('inf')
                self_sufficiency_index = float('inf')
            try:
                self_cons_index = (self.result[key]['internal_exchange'].sum() / self.result[key]['agg_export'].sum())
            except ZeroDivisionError:
                self_cons_index = float('inf')
            try:
                Peak2AVG_ext_demand = peak_community_import / community_import.mean()
            except ZeroDivisionError:
                Peak2AVG_ext_demand = 0
            try:
                Peak2AVG_ext_supply = peak_community_export / community_export.mean()
            except ZeroDivisionError:
                Peak2AVG_ext_supply = 0

            self.analysis['technical'][key] = {'full load hours': t_fullload,
                                               'external_demand': community_import.sum(),
                                               'external_supply': community_export.sum(),
                                               'internal_exchange': sum(self.result[key]['internal_exchange']),
                                               #'CO2 emissions': external_demand.sum() * self.configuration['elec_emission'],
                                                'peak_ext_demand': peak_community_import,
                                                'peak_ext_supply': peak_community_export,
                                                'energy_balance_index': energy_balance_index,
                                                'self_suff_index': self_sufficiency_index,
                                                'self_cons_index': self_cons_index,
                                               'Peak2AVG_ext_demand': Peak2AVG_ext_demand,
                                               'Peak2AVG_ext_supply': Peak2AVG_ext_supply}

        """--------------------ANALYZE PROSUMER ANNUITIES--------------------------------------"""
        for key in self.result.keys():
            if key == 'validated':
                df = True
            else:
                df = False

            cint_buy, cint_sell = self.calc_internal_prices(
                self.result[key]['agg_export'],
                self.result[key]['agg_import'],
                self.configuration['injection_price'].values,
                self.configuration['elec_price_int'].values,
                self.configuration['elec_price_ext'].values)

            injection_price = self.configuration['injection_price'].values
            ca_profit_df = 0
            ca_profit_agg = 0
            # CA
            for ps_name, ps in self.community_assets.items():
                if ps_name not in self.annuity_dict.keys():
                    self.annuity_dict[ps_name] = dict()

                a1,a2 = ps.calc_annuity(cint_buy, cint_sell,df)

                if df:
                    ca_profit_df = a2
                    self.annuity_dict[ps_name]['validated'] = a2
                else:
                    ca_profit_agg = a2
                    self.annuity_dict[ps_name]['initial'] = 0
                    self.annuity_dict[ps_name]['CA'] = a2

            for ps_name, ps in self.prosumers.items():
                if ps_name not in self.annuity_dict.keys():
                    self.annuity_dict[ps_name] = dict()
                a1,a2 = ps.calc_annuity(cint_buy, cint_sell,df)
                if df:
                    self.annuity_dict[ps_name]['validated'] = a2 - (inv_costs - ca_profit_df)/len(self.prosumers)
                else:
                    self.annuity_dict[ps_name]['initial'] = a1
                    self.annuity_dict[ps_name]['CA'] = a2 - (inv_costs - ca_profit_agg)/len(self.prosumers)

        return

    def calc_internal_prices(self, supply, demand, sell, p_int, p_ext, price_mech ='split'):
        """
        Calculating the internal prices for covering the communities internal demand with overproduction.
        SDR price:  from "Evaluation of peer-to-peer energy sharing mechanisms based on a
                   multiagent simulation framework" Zhou 2018
                   --> depends on the ratio between demand and supply
        Split:  Bill sharing from "Evaluation of peer-to-peer energy sharing mechanisms based on a
                   multiagent simulation framework" Zhou 2018
        Parameters
        ----------
        supply: amount of energy supplied to the grid in every time step
        demand: amount of energy demanded by the generation in every time step
        sell: remuneration for selling electricity
        p_int: price for buying electricity internally
        p_ext: price for buying electricity externally
        price_mech: adapted price_mech

        Returns:
        ----------
        price_internal_buy: new price for buying electricity
        price_internal_sell: new remuneration for selling electricity
        """

        if price_mech == 'SDR':

            price_internal_sell = pd.Series(data=0.0, index=demand.index)
            price_internal_buy = pd.Series(data=0.0, index=demand.index)

            sd_ratio = pd.Series(data=2.0, index=demand.index)
            sd_ratio.loc[demand > 0] = supply.loc[demand > 0] / demand.loc[demand > 0]


            for t in demand.index:

                if sd_ratio[t] < 1:
                    price_internal_sell[t] = (self.configuration['elec_price_ext_low'][t] * self.configuration['injection_price'].values[t]) \
                                             / ((self.configuration['elec_price_ext_low'][t] - self.configuration['injection_price'].values[t]) * sd_ratio[t] + self.configuration['injection_price'].values[t])

                    price_internal_buy[t] = price_internal_sell[t] * sd_ratio[t] \
                                            + self.configuration['elec_price_ext_low'][t] * (1 - sd_ratio[t])
                else:
                    price_internal_sell[t] = self.configuration['injection_price'].values[t]
                    price_internal_buy[t] = self.configuration['injection_price'.values][t]
        elif price_mech == 'split':
            price_internal_sell = pd.Series(data=0.0, index=demand.index)
            price_internal_buy = pd.Series(data=0.0, index=demand.index)
            for t in demand.index:
                p_avg = (p_ext[t] - p_int[t])/2

                if demand[t] <= supply[t] and supply[t] > 0:
                    price_internal_sell[t] = (demand[t] * (sell[t] + p_avg) + (supply[t] - demand[t]) * sell[t]) / supply[t]
                    price_internal_buy[t] = p_int[t] + p_avg

                elif demand[t] >= supply[t] and demand[t] > 0:
                    price_internal_buy[t] = (supply[t] * (p_int[t] + p_avg) + (demand[t] - supply[t]) * p_ext[t]) / demand[t]
                    price_internal_sell[t] = sell[t] + p_avg

                else:
                    price_internal_sell[t] = 5000
                    price_internal_buy[t] = 5000

        return price_internal_buy, price_internal_sell

    def save_results(self):
        if not os.path.exists('output_files/'):
            os.makedirs('output_files/')
        if not os.path.exists('output_files/' + self.name + '/'):
            os.makedirs('output_files/' + self.name + '/')
        
        for ps in self.prosumers.values():
            with pd.ExcelWriter('output_files/' + self.name + '/results_' + str(ps._name) + '.xlsx') as writer:
                for key, result in ps._result.items():
                    result.to_excel(writer, sheet_name=key)
        
        for ca in self.community_assets.values():
            with pd.ExcelWriter('output_files/' + self.name + '/results_' + str(ca._name) + '.xlsx') as writer:
                for key, result in ca._result.items():
                    result.to_excel(writer, sheet_name=key)

        p_int = self.configuration['elec_price_int'].values
        p_inj = self.configuration['injection_price'].values
        p_ext = self.configuration['elec_price_ext'].values

        prices = pd.DataFrame({'internal buy': p_int,  'sell': p_inj, 'external buy': p_ext})

        annuities_df = pd.DataFrame(self.annuity_dict)
        analysis_eco = pd.DataFrame(self.analysis['economical'])
        analysis_tech = pd.DataFrame(self.analysis['technical'])

        with pd.ExcelWriter('output_files/' + self.name + '/results_' + self.name + '.xlsx') as writer:
            prices.to_excel(writer, sheet_name='Prices')
            annuities_df.to_excel(writer, sheet_name='PS-Annuities')
            for key, value in self.result.items():
                value.to_excel(writer, sheet_name=str(key))

            analysis_eco.to_excel(writer, sheet_name='Analysis economical')
            analysis_tech.to_excel(writer, sheet_name='Analysis technical')
