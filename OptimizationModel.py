"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import pyomo.environ as pyo
from pyomo.opt import SolverStatus, TerminationCondition
import numpy as np
import pandas as pd
from Tooling.dynamics.Dynamic import resample_variable
    
class OptimizationBlock:
    def __init__(self, name, dynamic, type = pyo.Block):
        self.name = name
        self.dynamic = dynamic
        self.time_steps = list(dynamic.time_steps())

        if type == pyo.Block:
            self.block = type()
        else:
            self.block = type(name)

        self.block.T = pyo.Set(initialize = self.time_steps, ordered = True)
        self.T = self.block.T

        self.block.T_prime = pyo.Set(initialize = [-1] + self.time_steps, ordered = True)
        self.T_prime = self.block.T_prime

        self.component_dict = dict()
        self.u_var = []
        self.i_var = []
        self.i_var_prime = []
        self.objective_terms = []
        self.blocks = dict()

    def step_size(self, index):
        return self.dynamic.step_size(index)

    def add(self, name, component):
        if not isinstance(component, OptimizationBlock):
            self.block.add_component('_'.join(name), component)
        else:
            self.blocks[name] = component
            self.block.add_component('_'.join(name), component.block)
        if isinstance(component, pyo.Var):
            self.component_dict[name] = component
            if component.is_indexed() and component.index_set().local_name == 'T':
                self.i_var.append(name)
            elif not component.is_indexed():
                self.u_var.append(name)
            elif component.is_indexed() and component.index_set().local_name == 'T_prime':
                self.i_var_prime.append(name)
            else:
                raise ValueError('A variable to be added can only be indexed over T, T_prime or be unindexed!')
        if isinstance(component, pyo.Expression):
            self.component_dict[name] = component
        elif isinstance(component, pyo.Param):
            self.component_dict[name] = component

    def add_objective_term(self, term):
        self.block.add_component('objective_' + str(len(self.objective_terms)), term)
        self.objective_terms.append(term)

    def lift_expression(self, name, other, other_expression):
        self.add(name, resample_variable(other_expression, other.dynamic, self.dynamic, self.T))

    # assumes that the given values have the same dynamic as the block
    def set_value(self, name, values):
        component = self.component_dict[name]
        if isinstance(component, pyo.Param):
            for t in component.index_set():
                self.component_dict[name][t] = values[t]

    def __getitem__(self, var_name):
        if var_name in self.u_var:
            return self.component_dict[var_name].value
        elif var_name in self.i_var:
            values = self.component_dict[var_name].get_values()
            return pd.Series(data=(values[t] for t in self.T), index=self.T.ordered_data())
        elif var_name in self.i_var_prime:
            values = self.component_dict[var_name].get_values()
            return pd.Series(data=(values[t] for t in self.T), index=self.T.ordered_data())
        else:
            None

class OptimizationModel(OptimizationBlock):
    def __init__(self, name, dynamic):
        super().__init__(name, dynamic, pyo.ConcreteModel)

    def solve(self, options, tee):
        # glpk(bad for milp), cbc(good for milp), gurobi: linear, ipopt: nonlinear
        # in order to install a new solver paste the .exe file in env. path 'C:\Users\User\anaconda3\envs\envINEED'
        solver = pyo.SolverFactory('gurobi')
        solver.options.update(options)
        self.solver_result = solver.solve(self.block, tee = tee)

    def is_ok(self):
        return self.solver_result.solver.status == SolverStatus.ok and self.solver_result.solver.termination_condition == TerminationCondition.optimal

class EntityResult:
    def __init__(self, dynamic, variables):
        self.dynamic = dynamic

        u_var = []
        i_var =  []
        i_var_prime = []
        self.var_to_res = dict()
        for var, set in variables:
            if set == None:
                u_var.append(var)
                self.var_to_res[var] = 'u_result'
            elif set == 'T':
                i_var.append(var)
                self.var_to_res[var] = 'i_result'
            elif set == 'T_prime':
                i_var_prime.append(var)
                self.var_to_res[var] = 'i_result_prime'
                
        self.u_result = pd.Series(index=u_var)

        self.i_result = pd.DataFrame(index=dynamic.time_steps(), columns=i_var)

        self.i_result_prime = pd.DataFrame(index=[-1] + list(dynamic.time_steps()), columns=i_var_prime)

    def __getitem__(self, var_name):
        if var_name in self.var_to_res:
            res_name = self.var_to_res[var_name]
            return getattr(self, res_name).get(var_name)
        else:
            return None

    def __setitem__(self, var_name, value):
        if var_name in self.var_to_res:
            res_name = self.var_to_res[var_name]
            getattr(self, res_name)[var_name] = value

    def extract_results_from_model(self, model):
        if self.dynamic != model.dynamic:
            raise ValueError('Cannot extract results from a model into a EntitResult with a different dynamic!')
        for var_name in self.u_result.index:
            self.u_result[var_name] = pyo.value(model.component_dict[var_name])
        for var_name in self.i_result.columns:
            values = model.component_dict[var_name].get_values()
            self.i_result[var_name] = np.array(list(values[t] for t in model.T))
        for var_name in self.i_result_prime.columns:
            values = model.component_dict[var_name].get_values()
            self.i_result_prime[var_name] = np.array(list(values[t] for t in model.T_prime))

    def extract_partial_results_from_model(self, model, i_start, i_end):
        if self.dynamic.partial_dynamic(i_start, i_end) != model.dynamic:
            raise ValueError('Cannot extract partial results from a model into a EntitResult with a different dynamic!')
        for var_name in self.i_result.columns:
            values = model.component_dict[var_name].get_values()
            self.i_result[var_name][i_start:i_end] = np.array(list(values[t] for t in model.T))
        for var_name in self.i_result_prime.columns:
            values = model.component_dict[var_name].get_values()
            self.i_result_prime[var_name][i_start+1:i_end+1] = np.array(list(values[t] for t in model.T))

    def to_excel(self, excel_writer, **kwargs):
        sheet_name = kwargs.get('sheet_name', '')
        kwargs['sheet_name'] = 'unindexed' if sheet_name == '' else sheet_name + '_unindexed'
        self.u_result.to_excel(excel_writer, **kwargs)
        kwargs['sheet_name'] = 'T' if sheet_name == '' else sheet_name + '_T'
        self.i_result.to_excel(excel_writer, **kwargs)
        kwargs['sheet_name'] = 'T_prime' if sheet_name == '' else sheet_name + '_T_prime'
        self.i_result_prime.to_excel(excel_writer, **kwargs)
