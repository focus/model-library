"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import importlib
import inspect
import os
import sys

def load_component_library(root):
    package = 'Model_Library.Component.model'
    sys.path.append(root)
    module_paths = []
    classes = {}
    for directory, _, files in os.walk(root):
        for file in files:
            if file.endswith('.py') and file != '__init__.py':
                module_path = directory
                module_path = module_path.replace(root, '')
                module_path = module_path.replace(os.sep, '.')
                if module_path != '' and module_path not in module_paths:
                    module_paths.append(module_path)
                    module = importlib.import_module(module_path, package=package)
                    for name, klass in inspect.getmembers(module, inspect.isclass):
                        classes[name] = klass
    return classes
