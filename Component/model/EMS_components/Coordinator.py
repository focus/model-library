"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import pyomo.environ as pyo
from datetime import timedelta
from Tooling.dynamics.Dynamic import resample

def add_power_variables_sizing(community, model):
    """
    ToDo JBR: Can be substituted by the normal add_power_variables method?
    Introduces all variables necessary for the max_operational_profit and max_wholesale_profit
    objective functions for the sizing of the community assets.
    P_internal_demand is the internally matched demand. P_external_demand is the externally procurred electricity.
    Vice versa, the internal and external supply variables are introduced.

    Parameters
    ----------
    time_steps: time steps of whole problem size
    var_dict: variable dict
    model: pyomo model
    """
    model.add(('internal_exchange',), pyo.Var(model.T, bounds=(0, None)))
    model.add(('community_export',), pyo.Var(model.T, bounds=(0, None)))
    model.add(('community_import',), pyo.Var(model.T, bounds=(0, None)))

def add_peak_shaving_variables(community, model):
    """
    Introduction of peak power that will be used for taking on the peak demanded power value.

    Parameters
    ----------
    time_steps: time steps (not necessary)
    var_dict: dict with all variables
    model: pyomo model
    """
    model.add(('peak_community_import',), pyo.Var())

def add_residual_supply_variables(community, model):
    """
    Introduces the variables for the residual supply of the community. The residual supply takes on
    positive values when the aggregated load profiles of all agents result in surpluses and negative values when
    it results in overall demand.
    These variables are used for objective functions that only focus on the exchange with the environment of the EC.
    This is computational less expensive than distinguish also between external and internal exchange.

    Parameters
    ----------
    time_steps: time steps of current RH-interval
    var_dict: dict with all decision variables
    model: pyomo model
    """
    model.add(('agg_balance',), pyo.Var(model.T, bounds=(None, None)))

def add_power_constraints_sizing(community, model):
    """
    Similar to the "add_power_const"-method. However, here the new community's grid exchange depends on
    the CAs grid exchanges and not on DF activation.

    Parameters
    ----------
    time_steps: time steps of the whole problem size
    var_dict: dict constains all decision variables
    model: pyomo model
    """
    export_vars = []
    import_vars = []
    for ca in community.community_assets.values():
        ca_export_expression, ca_import_expression = ca.get_export_import_expressions(model.blocks[(ca._name,)])
        model.lift_expression((ca._name, 'export'), model.blocks[(ca._name,)], ca_export_expression)
        model.lift_expression((ca._name, 'import'), model.blocks[(ca._name,)], ca_import_expression)
        export_vars.append((ca._name, 'export'))
        import_vars.append((ca._name, 'import'))

    # distinguish between internal and external demand
    def rule(m, t):
        return model.component_dict[('internal_exchange',)][t] <= community.result['initial']['agg_export'][t] + pyo.quicksum(model.component_dict[export_var][t] for export_var in export_vars)
    model.add(('internal_exchange_1',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('internal_exchange',)][t] <= community.result['initial']['agg_import'][t] + pyo.quicksum(model.component_dict[import_var][t] for import_var in import_vars)
    model.add(('internal_exchange_2',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('community_export',)][t] == community.result['initial']['agg_export'][t] + pyo.quicksum(model.component_dict[export_var][t] for export_var in export_vars) - model.component_dict[('internal_exchange',)][t]
    model.add(('community_export_cons',), pyo.Constraint(model.T, rule = rule))

    def rule(m, t):
        return model.component_dict[('community_import',)][t] == community.result['initial']['agg_import'][t] + pyo.quicksum(model.component_dict[import_var][t] for import_var in import_vars) - model.component_dict[('internal_exchange',)][t]
    model.add(('community_import_cons',), pyo.Constraint(model.T, rule = rule))

def add_peak_shaving_constraints(community, model):
    """
    The peak power variable is set as the maximal value of all external demand variables to set it as
    the peak power demand.

    Parameters
    ----------
    time_steps: time steps of current RH-interval or whole problem size (if used for CA sizing)
    var_dict: dict cotains all decision variables
    model: pyomo model
    """
    def rule(m, t):
        return model.component_dict[('community_import',)][t] <= model.component_dict[('peak_community_import',)]
    model.add(('peak_community_import_cons',), pyo.Constraint(model.T, rule = rule))

def add_balance_constraints_sizing(community, model):
    """
    Similar to the "add_res_supply_constr" method but not depending on DF activations
    but on supply and demand variables of the CAs.
    """
    export_vars = []
    import_vars = []
    for ca in community.community_assets.values():
        ca_export_expression, ca_import_expression = ca.get_export_import_expressions(model.blocks[(ca._name,)])
        model.lift_expression((ca._name, 'export'), model.blocks[(ca._name,)], ca_export_expression)
        model.lift_expression((ca._name, 'import'), model.blocks[(ca._name,)], ca_import_expression)
        export_vars.append((ca._name, 'export'))
        import_vars.append((ca._name, 'import'))

    def rule(m, t):
        return model.component_dict[('agg_balance',)][t] == community.result['initial']['agg_balance'][t] + pyo.quicksum(model.component_dict[export_var][t] for export_var in export_vars) - pyo.quicksum(model.component_dict[import_var][t] for import_var in import_vars)
    model.add(('agg_balance_cons',), pyo.Constraint(model.T, rule = rule))

def add_total_community_import_constraints_sizing(community, model):
    """
    The sum of the external demands after DF activations are not allowed to be higher than before.
    This aims to prohibit arbitrage trading.
    """
    community_import = community.result['initial']['agg_balance'][model.time_steps] * -1
    community_import.loc[community_import < 0] = 0
    total_community_import = community_import.sum()

    model.add(('total_community_import_cons',), pyo.Constraint(expr = pyo.quicksum(model.component_dict[('community_import',)][t] for t in model.time_steps) <= total_community_import))

def add_sizing_objective(community, model, strategy_name):
    if 'max_operational_profit' == strategy_name:
        # factor that converts the simulation to ONE year
        annual_factor = timedelta(days=365) / timedelta(hours=sum(model.dynamic.step_size_p(position) for position in range(model.dynamic.number_of_steps())))

        # capital related costs and operating related costs
        capital_costs = []
        for ca in community.community_assets.values():
            for component in ca._components.values():
                capital_cost = component.add_capital_costs(model.blocks[(ca._name,)], ca._configuration)
                if capital_cost is not None:
                    capital_costs.append(((ca._name,), capital_cost))

        model.block.f1 = pyo.Var()
        elec_price_int = resample(community.configuration['elec_price_int'], community.dynamic, model.dynamic)
        elec_price_ext = resample(community.configuration['elec_price_ext'], community.dynamic, model.dynamic)
        injection_price = resample(community.configuration['injection_price'], community.dynamic, model.dynamic)
        model.block.C_f1 = pyo.Constraint(expr=model.block.f1 == - pyo.quicksum(model.blocks[block].component_dict[var] for block, var in capital_costs)
                                                                - (pyo.quicksum(model.component_dict[('internal_exchange',)][t] * elec_price_int[t] * model.step_size(t) for t in model.time_steps)
                                                                 + pyo.quicksum(model.component_dict[('community_import',)][t] * elec_price_ext[t] * model.step_size(t) for t in model.time_steps)
                                                                 - pyo.quicksum(model.component_dict[('internal_exchange',)][t] * injection_price[t] * model.step_size(t) for t in model.time_steps)
                                                                 - pyo.quicksum(model.component_dict[('community_export',)][t] * injection_price[t] * model.step_size(t) for t in model.time_steps)
                                                                  ) * annual_factor
                                                                - model.component_dict[('peak_community_import',)] * community.configuration['network_usage_capacity_fee'])

        model.block.O_f1 = pyo.Objective(expr=model.block.f1, sense=pyo.maximize)

    if 'max_wholesale_profit' == strategy_name:
        # factor that converts the simulation to ONE year
        annual_factor = timedelta(days=365) / timedelta(hours=sum(model.dynamic.step_size_p(position) for position in range(model.dynamic.number_of_steps())))

        # capital related costs and operating related costs
        capital_costs = []
        for ca in community.community_assets.values():
            for component in ca._components.values():
                capital_cost = component.add_capital_costs(model, ca._configuration)
                if capital_cost is not None:
                    capital_costs.append(((ca._name,), capital_cost))

        model.block.f1 = pyo.Var()
        model.block.C_f1 = pyo.Constraint(expr=model.block.f1 == - pyo.quicksum(model.blocks[block].component_dict[var] for block, var in capital_costs)
                                                                 + pyo.quicksum(model.component_dict[('agg_balance',)][t] * community.configuration['spot_price'][t] * model.step_size(t) for t in model.time_steps) * annual_factor)

        model.block.O_f1 = pyo.Objective(expr=model.block.f1, sense=pyo.maximize)

def implement_sizing_strategy(community, model, strategy_name):
    if 'max_operational_profit' == strategy_name:
        add_power_variables_sizing(community, model)
        add_peak_shaving_variables(community, model)

        add_power_constraints_sizing(community, model)
        add_peak_shaving_constraints(community, model)

        add_total_community_import_constraints_sizing(community, model)

    if 'max_wholesale_profit' == strategy_name:
        add_residual_supply_variables(community, model)

        add_balance_constraints_sizing(community, model)

    add_sizing_objective(community, model, strategy_name)
