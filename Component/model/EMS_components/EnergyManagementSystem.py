"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import pyomo.environ as pyo
from Model_Library.Prosumer.scripts import calc_annuity_vdi2067
from datetime import timedelta

def implement_strategy(prosumer, model, configuration):
    if 'strategy' not in configuration:
        implement_single_strategy(prosumer, model, configuration, [])
    else:
        if isinstance(configuration['strategy'], list):
            implement_single_strategy(prosumer, model, configuration, configuration['strategy'])
        elif isinstance(configuration['strategy'], dict):
            for strategy_name, single_strategy in configuration['strategy'].items():
                implement_single_strategy(prosumer, model, configuration, single_strategy, strategy_name)

def implement_single_strategy(prosumer, model, configuration, strategy, name='objective'):
    objective_expression = []
    if 'annuity' in strategy:
        capital_costs = []
        for component in prosumer._components.values():
            capital_cost = component.add_capital_costs(model, prosumer._configuration)
            if capital_cost is not None:
                capital_costs.append(capital_cost)

        operating_costs = []
        for component in prosumer._components.values():
            operating_cost = component.add_operating_costs(model, configuration)
            if operating_cost is not None:
                operating_costs.append(operating_cost)

        # The factor that converts the simulation to ONE year
        annual_factor = timedelta(days=365) / timedelta(hours=sum(model.dynamic.step_size_p(position) for position in range(model.dynamic.number_of_steps())))
        
        objective_expression.append(pyo.quicksum(model.component_dict[capital_cost] for capital_cost in capital_costs) + (pyo.quicksum(model.component_dict[operation_cost][t] for operation_cost in operating_costs for t in model.time_steps) * annual_factor))

    if 'peak_power_costs' in strategy:
        peak_power_costs = []
        for component in prosumer._components.values():
            peak_power_cost = component.add_peak_power_costs(model)
            if peak_power_cost is not None:
                peak_power_costs.append(peak_power_cost)

        # net present value factor (PREISDYNAMISCHER Barwertfaktor) and annuity factor (Annuitätsfaktor)
        dynamic_cash_value = calc_annuity_vdi2067.dynamic_cash_value(prosumer._configuration['planning_horizon'],
                                                                     q=1 + prosumer._configuration['yearly_interest'], r=1)
        annuity_factor = calc_annuity_vdi2067.annuity_factor(prosumer._configuration['planning_horizon'],
                                                             q=1 + prosumer._configuration['yearly_interest'])

        objective_expression.append(pyo.quicksum(model.component_dict[peak_power_cost] for peak_power_cost in peak_power_costs) * dynamic_cash_value * annuity_factor)
    
    if 'c02' in strategy:
        co2_emmisions = []
        for component in prosumer._components.values():
            co2_emmision = component.add_co2_emissions(model, configuration)
            if co2_emmision is not None:
                co2_emmisions.append(co2_emmision)
                
        objective_expression.append(pyo.quicksum(model.component_dict[co2_emmision][t] for co2_emmision in co2_emmisions for t in model.time_steps))
    
    objective_expression.append(pyo.quicksum(term[t] for term in model.objective_terms for t in model.time_steps))

    setattr(model.block, 'f_' + name, pyo.Var())
    setattr(model.block, 'c_' + name, pyo.Constraint(expr=getattr(model.block, 'f_' + name) == pyo.quicksum(term for term in objective_expression)))
    setattr(model.block, 'O_' + name, pyo.Objective(expr=getattr(model.block, 'f_' + name), sense=pyo.minimize))
    