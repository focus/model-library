"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import AbstractComponent, ComponentKind, ComponentCommodity

import pyomo.environ as pyo

class BaseBusBar(AbstractComponent):

    def __init__(self, name, type, commodity, dynamic):
        super().__init__(name=name,
                         type=type,
                         commodity_1=commodity,
                         commodity_2=None,
                         commodity_3=commodity,
                         commodity_4=None,
                         min_size=None,
                         max_size=None,
                         flexible=False,
                         dynamic=dynamic)
        
        self.dynamic = dynamic

    def match(self, kind=ComponentKind.ALL, commodity=ComponentCommodity.ALL):
        match_kind = kind == ComponentKind.ALL or kind == ComponentKind.BUSBAR
        match_commodity = commodity == ComponentCommodity.ALL or \
            commodity == self.commodity or \
            (isinstance(commodity, list) and self.commodity in commodity)
        return match_kind and match_commodity
    
    def get_base_variable_names(self):
        return [((self.name, 'input_1'), 'T'), ((self.name, 'output_1'), 'T')]

    def _add_variables(self, model, prefix):
        model.add(prefix + ('input_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))

    def _add_constraints(self, model, prefix, configuration):
        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] == model.component_dict[prefix + ('input_1',)][t]
        model.add(prefix + ('conser',), pyo.Constraint(model.T, rule = rule))
