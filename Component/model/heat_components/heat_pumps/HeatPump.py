"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseComponent import BaseComponent
from Tooling.predictor.Predictor import Predictor
from Tooling.dynamics.Dynamic import resample

import pyomo.environ as pyo
import pandas as pd

class HeatPump(BaseComponent):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type="HeatPump",
                         commodity_1=ComponentCommodity.ELECTRICITY,
                         commodity_2=None,
                         commodity_3=ComponentCommodity.HEAT,
                         commodity_4=None,
                         configuration=configuration,
                         model_directory=model_directory,
                         dynamic=dynamic)

        if isinstance(configuration['temperature'], str):
            self.temperature = resample(profiles[configuration['temperature']][0], profiles[configuration['temperature']][1], dynamic)
        elif isinstance(configuration['temperature'], dict):
            self.temperature = Predictor(resample(profiles[configuration['temperature']['profile']][0], profiles[configuration['temperature']['profile']][1], dynamic), configuration['temperature']['type'], configuration['temperature']['method'], dynamic)

    def calc_cop(self, amb_t, set_t=40):
        return (set_t + 273.15) / (set_t - amb_t) * self.efficiency

    def _constraint_conser(self, model, prefix, configuration):
        model.add(prefix + ('cop',), pyo.Param(model.T, mutable = True))

        if isinstance(self.temperature, Predictor):
            if 'predict' in configuration:
                temperature = self.temperature.predict(list(model.T))
            else:
                temperature = resample(self.temperature.profile, self.dynamic, model.dynamic)
        else:
            temperature = resample(self.temperature, self.dynamic, model.dynamic)
        cop = self.calc_cop(temperature)

        model.set_value(prefix + ('cop',), cop)
    
        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] == model.component_dict[prefix + ('input_1',)][t] * model.component_dict[prefix + ('cop',)][t]
        model.add(prefix + ('conser',), pyo.Constraint(model.T, rule = rule))

    # Flexibility stuff

    def check_limits(self, input_flows, output_flows, results, dynamic):

        pos_flex = self.temp_flex['flex_pos'][dynamic.time_steps()]
        pos_flex_inc = self.temp_flex['flex_pos_inc'][dynamic.time_steps()]  # increasing of comp output
        pos_flex_dec = self.temp_flex['flex_pos_dec'][dynamic.time_steps()]  # curtailment of comp input

        neg_flex = self.temp_flex['flex_neg'][dynamic.time_steps()]
        neg_flex_inc = self.temp_flex['flex_neg_inc'][dynamic.time_steps()]  # increasing of comp input
        neg_flex_dec = self.temp_flex['flex_neg_dec'][dynamic.time_steps()]  # curtailment of comp output

        power_limit = 1 * results[(self.name, 'capacity')]  # excluding the excess power for safety

        # upper limit for neg-inc df
        upper_limit_neg_inc = power_limit - input_flows
        upper_limit_neg_inc.loc[upper_limit_neg_inc < 0] = 0  # account for the case input_flows > power_limit (excess)

        # max neg-inc df on input side of converter
        max_neg_inc = neg_flex_inc.combine(upper_limit_neg_inc, min)

        # negative flex doesn't need conversion as it already has been
        max_neg_inc_out = max_neg_inc

        self.temp_flex['flex_neg_inc'] = max_neg_inc_out
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] < 0, 'flex_neg_inc'] = 0

        self.temp_flex['flex_neg'] = self.temp_flex['flex_neg_inc'] + self.temp_flex['flex_neg_dec']

    def adjust_flex_with_efficiency(self, results, dynamic):
        # ToDo: Is this correct? Efficiency effects energy as well because in the end the community has to know how much
        # energy is still available after each flex activation. e.g.: If the efficiency would not affect the energy,
        # the community would always underestimate the effect of a positive flexibility activation on the energy amount
        # left

        self.flex_pos_inc_old = self.temp_flex['flex_pos_inc'].copy()
        self.flex_neg_dec_old = self.temp_flex['flex_neg_dec'].copy()

        power_limit = 1 * results[(self.name, 'capacity')]  # excluding the excess power for safety

        total_in = pd.Series(data=0.0, index=dynamic.time_steps())
        total_in += results[(self.name, 'input_1')]  # * self.input_efficiency
        # total out stays zero
        # ------------EFFICIENCY--------------
        # the idea here is that the flexibility that comes out of the inverter is
        # related to the efficiency of the inverter.
        # So considering the power flow of the flexibility type,
        # the net flexibility gets higher or lower compared to the flexibility at the origin component

        # ------POSITIVE--------------
        self.temp_flex['flex_pos_inc'] = 0  # increasing positive flex means lower flex at grid
        # decreasing positive flex means higher flex at grid
        self.temp_flex['flex_pos_dec'] = total_in
        self.temp_flex['flex_pos'] = total_in

        self.temp_flex['e_dch'] = self.temp_flex['e_dch'] / self.cop[dynamic.time_steps()]
        # ------NEGATIVE--------------
        # increasing negative flex means higher flex at grid
        self.temp_flex['flex_neg_inc'] = power_limit - total_in
        self.temp_flex['flex_neg_dec'] = 0  # decreasing neg flex means lower flex at grid
        self.temp_flex['flex_neg'] = power_limit - total_in

        self.temp_flex['e_cha'] = self.temp_flex['e_cha'] / self.cop[dynamic.time_steps()]

    def calc_correction_factors(self, time_steps, c_f_df, results):
        c_f_cha = pd.Series(data=1, index=time_steps)
        #c_f_cha = pd.Series(data=cop, index=T)
        c_static_cha = pd.Series(data=0, index=time_steps)

        c_f_df['c_f_cha'] = c_f_cha * c_f_df['c_f_cha']
        c_f_df['c_static_cha'] = c_static_cha + c_f_df['c_static_cha']

        return c_f_df
