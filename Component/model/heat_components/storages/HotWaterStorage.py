"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseStorage import BaseStorage

import pyomo.environ as pyo
import pandas as pd

class HotWaterStorage(BaseStorage):
    
    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type="HotWaterStorage",
                         commodity=ComponentCommodity.HEAT,
                         configuration=configuration,
                         model_directory=model_directory,
                         dynamic=dynamic)

    def _constraint_conser(self, model, prefix, configuration):
        if 'storage_connect' in configuration and self.match(commodity=configuration['storage_connect']['commodity']):
            last_energy = configuration['storage_connect']['values'][self.name]
            model.add(prefix + ('fix_first_energy',), pyo.Constraint(expr = model.component_dict[prefix + ('energy',)][model.T_prime.first()] == last_energy))
        else:
            model.add(prefix + ('fix_first_energy',), pyo.Constraint(expr = model.component_dict[prefix + ('energy',)][model.T_prime.first()] == self.init_soe * model.component_dict[prefix + ('capacity',)]))
        
        def rule(m, t):
            return model.component_dict[prefix + ('energy',)][t] == model.component_dict[prefix + ('energy',)][model.T_prime[model.T_prime.ord(t) - 1]] * 0.995 + model.component_dict[prefix + ('input_1',)][t] * self.input_efficiency * model.step_size(t) - model.component_dict[prefix + ('output_1',)][t] / self.output_efficiency * model.step_size(t)
        model.add(prefix + ('conser',), pyo.Constraint(model.T, rule = rule))

    # Flexibility stuff

    def calc_flex_comp(self, results, dynamic, init_results):
        """
        This method calculates the maximal possible flexibility from the battery.
        This is done by comparing two values:
            1) The maximal possible CHA/DCH power
            2) The maximal allowed CHA/DCH power regarding the available power BEFORE the PLANNED CHA/DCH
        The minimal value is chosen to be the maximal possible CHA/DCH power. To claculate the available flexibility from this,
        the PLANNED CHA/DCH rates are added/substracted depending on the type of flexibility.
        E.g. positive flex: the current DCH power is substracted and the current CHA power is added
        Parameters
        ----------
        flows
        results
        T

        Returns
        -------

        """

        cap_storage = results[(self.name, 'capacity')]  # the total theoretical capacity of the comm_batt
        e_storage = results[(self.name, 'energy')]
        e_storage_shift = e_storage.shift(periods=1)
        e_storage_shift[dynamic.index_of(0)] = cap_storage[dynamic.index_of(0)] * self.init_soe

        # maximal in/out powers that the comm_batt is capable of
        max_power_dch = 0
        max_power_cha = 0

        # get the total output/input powers
        init_dch = pd.Series(data=0.0, index=dynamic.time_steps())
        init_dch += results[(self.name, 'output_1')] #  self.output_efficiency #here not necessary

        init_cha = pd.Series(data=0.0, index=dynamic.time_steps())
        init_cha += results[(self.name, 'input_1')] # self.input_efficiency

        # for now inputs and outputs are not relevant in this component because we just look at electric flows.
        # The power constraints will be added by the heatpump which has the boundaries for the electrical power.
        total_dch = pd.Series(data=0.0, index=dynamic.time_steps())
        total_cha = pd.Series(data=0.0, index=dynamic.time_steps())

        # now the energy restriction is calculated
        # ToDO @jbr: in the future here should be an extra restriction if some SOE values
        #  have to be reached in future timesteps

        # special energy restriction: the available energy for discharge in time step t depends on the available
        # energy in t+x. x is the number of time steps until an ongoing discharging process is finished.
        # That means that E(t+x) >= E(t+x-1)

        e_dch = pd.Series(data=0.0, index=dynamic.time_steps())
        for t in dynamic.time_steps():
            if t == init_results.index[0]:
                e_old = self.init_soe * cap_storage[0]
            else:
                e_old = init_results[(self.name, 'energy')].loc[t]

            if e_old <= self.min_soe * cap_storage[dynamic.index_of(0)]:
                e_dch[t] = 0
                continue

            for e_act in init_results[(self.name, 'energy')].loc[t+1:]:
                if e_act >= e_old:
                    e_dch[t] = e_old - self.min_soe * cap_storage[dynamic.index_of(0)]
                    break
                elif e_act <= self.min_soe * cap_storage[dynamic.index_of(0)]:
                    e_dch[t] = 0
                    break
                else:
                    e_old = e_act
        e_cha = (1 * cap_storage[dynamic.index_of(0)] - e_storage)  # / self.input_efficiency
        e_dch = e_dch.combine((e_storage - 0.2 * cap_storage),min)  # * self.output_efficiency

        e_cha.loc[e_cha < 0] = 0
        e_dch.loc[e_dch < 0] = 0

        for t in dynamic.time_steps():
            if e_dch[t] < -1 * e_cha[t]:
                print('Fehler zum Zeitpunkt ' + str(t))

        # pos flex:
        self.temp_flex['flex_pos_inc'] = max_power_dch - total_dch
        self.temp_flex.loc[self.temp_flex['flex_pos_inc'] < 0, 'flex_pos_inc'] = 0

        self.temp_flex['flex_pos_dec'] = total_cha
        self.temp_flex['flex_pos'] = max_power_dch - total_dch + total_cha
        self.temp_flex.loc[self.temp_flex['flex_pos'] < 0, 'flex_pos'] = 0

        self.temp_flex['e_dch'] = e_dch

        # neg flex: negative signs due to negative flexibility
        self.temp_flex['flex_neg_inc'] = max_power_cha - total_cha
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] < 0, 'flex_neg_inc'] = 0
        self.temp_flex['flex_neg_dec'] = total_dch
        self.temp_flex['flex_neg'] = max_power_cha - total_cha + total_dch
        self.temp_flex.loc[self.temp_flex['flex_neg'] < 0, 'flex_neg'] = 0

        self.temp_flex['e_cha'] = e_cha

    def calc_correction_factors(self, time_steps, c_f_df, results):

        c_f_dch = pd.Series(data=1/self.output_efficiency, index=time_steps)
        c_static_dch =pd.Series(data=0, index=time_steps)
        c_f_cha = pd.Series(data=self.input_efficiency, index=time_steps)
        c_static_cha = pd.Series(data=0, index=time_steps)

        c_f_df['c_f_dch'] = c_f_dch * c_f_df['c_f_dch']
        c_f_df['c_static_dch'] = c_static_dch + c_f_df['c_static_dch']

        c_f_df['c_f_cha'] = c_f_cha * c_f_df['c_f_cha']
        c_f_df['c_static_cha'] = c_static_cha + c_f_df['c_static_cha']

        return c_f_df
