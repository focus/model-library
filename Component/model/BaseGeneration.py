"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import AbstractComponent, ComponentKind, ComponentCommodity
from Tooling.predictor.Predictor import Predictor
from Tooling.dynamics.Dynamic import resample

import pyomo.environ as pyo

class BaseGeneration(AbstractComponent):

    def __init__(self, name, type, commodity, configuration, profiles, dynamic):
        super().__init__(name=name,
                         type=type,
                         commodity_1=None,
                         commodity_2=None,
                         commodity_3=commodity,
                         commodity_4=None,
                         min_size=None,
                         max_size=None,
                         flexible=None,
                         dynamic=dynamic)
        
        self.commodity = commodity

        if configuration['type'] != 'ElectricalGeneration':
            if isinstance(configuration['generation'], str):
                self.generation = resample(profiles[configuration['generation']][0], profiles[configuration['generation']][1], dynamic)
            elif isinstance(configuration['generation'], dict):
                self.generation = Predictor(resample(profiles[configuration['generation']['profile']][0], profiles[configuration['generation']['profile']][1], dynamic), configuration['generation']['type'], configuration['generation']['method'], dynamic)

    def match(self, kind=ComponentKind.ALL, commodity=ComponentCommodity.ALL):
        match_kind = kind == ComponentKind.ALL or kind == ComponentKind.GENERATION
        match_commodity = commodity == ComponentCommodity.ALL or \
            commodity == self.commodity or \
            (isinstance(commodity, list) and self.commodity in commodity)
        return match_kind and match_commodity

    def get_base_variable_names(self):
        return [((self.name, 'output_1'), 'T')]
    
    def _add_variables(self, model, prefix):
        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))

    def _add_constraints(self, model, prefix, configuration):
        model.add(prefix + ('generation',), pyo.Param(model.T, mutable = True))

        if isinstance(self.generation, Predictor):
            if 'predict' in configuration:
                generation = self.generation.predict(list(model.T))
            else:
                generation = resample(self.generation.profile, self.dynamic, model.dynamic)
        else:
            generation = resample(self.generation, self.dynamic, model.dynamic)

        model.set_value(prefix + ('generation',), generation)

        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] == model.component_dict[prefix + ('generation',)][t]
        model.add(prefix + ('generation_cons',), pyo.Constraint(model.T, rule = rule))
