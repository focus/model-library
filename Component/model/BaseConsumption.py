"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import AbstractComponent, ComponentKind, ComponentCommodity
from Tooling.predictor.Predictor import Predictor
from Tooling.dynamics.Dynamic import resample

import pyomo.environ as pyo

class BaseConsumption(AbstractComponent):

    def __init__(self, name, type, commodity, configuration, profiles, dynamic):
        super().__init__(name=name,
                         type=type,
                         commodity_1=commodity,
                         commodity_2=None,
                         commodity_3=None,
                         commodity_4=None,
                         min_size=None,
                         max_size=None,
                         flexible=None,
                         dynamic=dynamic)
        
        self.commodity = commodity

        if configuration['type'] != 'DrivingConsumption':
            if isinstance(configuration['consumption'], str):
                self.consumption = resample(profiles[configuration['consumption']][0], profiles[configuration['consumption']][1], dynamic)
            elif isinstance(configuration['consumption'], dict):
                self.consumption = Predictor(resample(profiles[configuration['consumption']['profile']][0], profiles[configuration['consumption']['profile']][1], dynamic), configuration['consumption']['type'], configuration['consumption']['method'], dynamic)

    def match(self, kind=ComponentKind.ALL, commodity=ComponentCommodity.ALL):
        match_kind = kind == ComponentKind.ALL or kind == ComponentKind.CONSUMPTION
        match_commodity = commodity == ComponentCommodity.ALL or \
            commodity == self.commodity or \
            (isinstance(commodity, list) and self.commodity in commodity)
        return match_kind and match_commodity

    def get_base_variable_names(self):
        return [((self.name, 'input_1'), 'T')]

    def _add_variables(self, model, prefix):
        model.add(prefix + ('input_1',), pyo.Var(model.T, bounds=(0, None)))

    def _add_constraints(self, model, prefix, configuration):
        model.add(prefix + ('consumption',), pyo.Param(model.T, mutable = True))

        if isinstance(self.consumption, Predictor):
            if 'predict' in configuration:
                consumption = self.consumption.predict(list(model.T))
            else:
                consumption = resample(self.consumption.profile, self.dynamic, model.dynamic)
        else:
            consumption = resample(self.consumption, self.dynamic, model.dynamic)

        model.set_value(prefix + ('consumption',), consumption)

        if 'consumption_slack' in configuration and self.match(commodity=configuration['consumption_slack']['commodity']):
            model.add(prefix + ('s_consumption',), pyo.Var(model.T, bounds=(0, None)))

            def rule(m, t):
                return model.component_dict[prefix + ('input_1',)][t] + model.component_dict[prefix + ('s_consumption',)][t] == model.component_dict[prefix + ('consumption',)][t]
            model.add(prefix + ('consumption_cons',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[prefix + ('s_consumption',)][t] * configuration["consumption_slack"]["strategy_factor"]
            model.add_objective_term(pyo.Expression(model.T, rule=rule))
        else:
            def rule(m, t):
                return model.component_dict[prefix + ('input_1',)][t] == model.component_dict[prefix + ('consumption',)][t]
            model.add(prefix + ('consumption_cons',), pyo.Constraint(model.T, rule = rule))