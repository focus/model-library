"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import AbstractComponent, ComponentKind, ComponentCommodity
from Tooling.predictor.Predictor import Predictor
from Tooling.dynamics.Dynamic import resample

import warnings
import math
import pyomo.environ as pyo
from Model_Library.Prosumer.scripts import calc_annuity_vdi2067
import json
import os

class BaseGrid(AbstractComponent):

    def __init__(self, name, type, commodity, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type=type,
                         commodity_1=commodity,
                         commodity_2=None,
                         commodity_3=commodity,
                         commodity_4=None,
                         min_size=configuration['min_size'],
                         max_size=configuration['max_size'],
                         flexible=False,
                         dynamic=dynamic)
        
        self.commodity = commodity
        
        with open((os.path.join(model_directory, configuration['type'], configuration['model'] + '.json'))) as f:
            model = json.load(f)
        if 'service_life' in model:
            self.life = model['service_life']
        else:
            warnings.warn(f"No column for service_life for component {self.name}!")
        if 'cost' in model:
            self.cost = model['cost']
        else:
            warnings.warn(f"No column for cost for component {self.name}!")
        if 'factor_repair_effort' in model:
            self.f_inst = model['factor_repair_effort']
        else:
            warnings.warn(f"No column for factor_repair_effort for component {self.name}!")
        if 'factor_servicing_effort' in model:
            self.f_w = model['factor_servicing_effort']
        else:
            warnings.warn(f"No column for factor_servicing_effort for component {self.name}!")
        if 'servicing_effort_hours' in model:
            self.f_op = model['servicing_effort_hours']
        else:
            warnings.warn(f"No column for servicing_effort_hours for component {self.name}!")
        
        if 'price' in configuration:
            if isinstance(configuration['price'], float) or isinstance(configuration['injection_price'], int):
                self.price = configuration['price']
            elif isinstance(configuration['price'], str):
                self.price = resample(profiles[configuration['price']][0], profiles[configuration['price']][1], dynamic)
            elif isinstance(configuration['price'], dict):
                self.price = Predictor(resample(profiles[configuration['price']['profile']][0], profiles[configuration['price']['profile']][1], dynamic), configuration['price']['type'], configuration['price']['method'], dynamic)
        else:
            self.price = 0
        if 'injection_price' in configuration:
            if isinstance(configuration['injection_price'], float) or isinstance(configuration['injection_price'], int):
                self.injection_price = configuration['injection_price']
            elif isinstance(configuration['injection_price'], str):
                self.injection_price = resample(profiles[configuration['injection_price']][0], profiles[configuration['injection_price']][1], dynamic)
            elif isinstance(configuration['injection_price'], dict):
                self.injection_price = Predictor(resample(profiles[configuration['injection_price']['profile']][0], profiles[configuration['injection_price']['profile']][1], dynamic), configuration['injection_price']['type'], configuration['injection_price']['method'], dynamic)
        else:
            self.injection_price = 0

    def match(self, kind=ComponentKind.ALL, commodity=ComponentCommodity.ALL):
        match_kind = kind == ComponentKind.ALL or kind == ComponentKind.GRID
        match_commodity = commodity == ComponentCommodity.ALL or \
            commodity == self.commodity or \
            (isinstance(commodity, list) and self.commodity in commodity)
        return match_kind and match_commodity
    
    def get_base_variable_names(self):
        return [((self.name, 'capacity'), None), ((self.name, 'input_1'), 'T'), ((self.name, 'output_1'), 'T')]
    
    def _add_variables(self, model, prefix):
        lb_capacity = self.min_size
        ub_capacity = self.max_size
        if math.isinf(self.min_size):
            lb_capacity = None
        if math.isinf(self.max_size):
            ub_capacity = None

        model.add(prefix + ('capacity',), pyo.Var(bounds=(lb_capacity, ub_capacity)))

        model.add(prefix + ('input_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))

    def _add_constraints(self, model, prefix, configuration):
        if 'fix_sizing' in configuration:
            model.add(prefix + ('fix_capacity',), pyo.Constraint(expr = model.component_dict[prefix + ('capacity',)] == configuration['fix_sizing']['values'][self.name]))
        
        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] <= model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('capacity_input_cons',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] <= model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('capacity_output_cons',), pyo.Constraint(model.T, rule = rule))

        if 'grid_slack' in configuration and self.match(commodity=configuration['grid_slack']['commodity']):
            model.add(prefix + ('s_p_export',), pyo.Var(model.T, bounds=(0, None)))
            model.add(prefix + ('s_n_export',), pyo.Var(model.T, bounds=(0, None)))
            model.add(prefix + ('s_p_import',), pyo.Var(model.T, bounds=(0, None)))
            model.add(prefix + ('s_n_import',), pyo.Var(model.T, bounds=(0, None)))

            def rule(m, t):
                return model.component_dict[prefix + ('input_1',)][t] == configuration['grid_slack']['values'][self.name][0][t] + model.component_dict[prefix + ('s_p_export',)][t] - model.component_dict[prefix + ('s_n_export',)][t]
            model.add(prefix + ('s_export',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[prefix + ('output_1',)][t] == configuration['grid_slack']['values'][self.name][1][t] + model.component_dict[prefix + ('s_p_import',)][t] - model.component_dict[prefix + ('s_n_import',)][t]
            model.add(prefix + ('s_import',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[prefix + ('s_p_export',)][t] * configuration['grid_slack']['strategy_factor']
            model.add_objective_term(pyo.Expression(model.T, rule=rule))

            def rule(m, t):
                return model.component_dict[prefix + ('s_n_export',)][t] * configuration['grid_slack']['strategy_factor']
            model.add_objective_term(pyo.Expression(model.T, rule=rule))

            def rule(m, t):
                return model.component_dict[prefix + ('s_p_import',)][t] * configuration['grid_slack']['strategy_factor']
            model.add_objective_term(pyo.Expression(model.T, rule=rule))

            def rule(m, t):
                return model.component_dict[prefix + ('s_n_import',)][t] * configuration['grid_slack']['strategy_factor']
            model.add_objective_term(pyo.Expression(model.T, rule=rule))

        if 'grid_fix' in configuration and self.match(commodity=configuration['grid_fix']['commodity']):
            def rule(m, t):
                return model.component_dict[prefix + ('input_1',)][t] == configuration['grid_fix']['values'][self.name][0][t]
            model.add(prefix + ('fix_export',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[prefix + ('output_1',)][t] == configuration['grid_fix']['values'][self.name][1][t]
            model.add(prefix + ('fix_import',), pyo.Constraint(model.T, rule = rule))
    
    def add_capital_costs(self, model, prosumer_configuration):
        prefix = (self.name,)

        model.add(prefix + ('capital_cost',), pyo.Var(bounds=(0, None)))
        
        capital_cost = calc_annuity_vdi2067.run(prosumer_configuration['planning_horizon'],
                                               self.life,
                                               self.cost,
                                               model.component_dict[prefix + ('capacity',)],
                                               self.f_inst,
                                               self.f_w,
                                               self.f_op,
                                               prosumer_configuration['yearly_interest'])
        model.add(prefix + ('capital_cost_cons',), pyo.Constraint(expr = model.component_dict[prefix + ('capital_cost',)] == capital_cost))
        return prefix + ('capital_cost',)

    def add_operating_costs(self, model, configuration):
        prefix = (self.name,)
        
        if isinstance(self.price, float) or isinstance(self.price, int):
            cost_function = lambda t: model.component_dict[prefix + ('output_1',)][t] * self.price
        else:
            model.add(prefix + ('price',), pyo.Param(model.T, mutable = True))

            if isinstance(self.price, Predictor):
                if 'predict' in configuration:
                    price = self.price.predict(list(model.T))
                else:
                    price = resample(self.price.profile, self.dynamic, model.dynamic)
            else :
                price = resample(self.price, self.dynamic, model.dynamic)
            
            model.set_value(prefix + ('price',), price)
        
            cost_function = lambda t: model.component_dict[prefix + ('output_1',)][t] * model.component_dict[prefix + ('price',)][t]
        
        if isinstance(self.injection_price, float) or isinstance(self.injection_price, int):
            revenue_function = lambda t: model.component_dict[prefix + ('input_1',)][t] * self.injection_price
        else:
            model.add(prefix + ('injection_price',), pyo.Param(model.T, mutable = True))

            if isinstance(self.injection_price, Predictor):
                if 'predict' in configuration:
                    injection_price = self.injection_price.predict(list(model.T))
                else:
                    injection_price = resample(self.injection_price.profile, self.dynamic, model.dynamic)
            else :
                injection_price = resample(self.injection_price, self.dynamic, model.dynamic)
            
            model.set_value(prefix + ('injection_price',), injection_price)
        
            revenue_function = lambda t: model.component_dict[prefix + ('input_1',)][t] * model.component_dict[prefix + ('injection_price',)][t]

        model.add(prefix + ('operating_cost',), pyo.Var(model.T))

        def rule(m, t):
            return model.component_dict[prefix + ('operating_cost',)][t] == (cost_function(t) - revenue_function(t)) * model.step_size(t)
        model.add(prefix + ('operation_cost_cons',), pyo.Constraint(model.T, rule = rule))

        return prefix + ('operating_cost',)
