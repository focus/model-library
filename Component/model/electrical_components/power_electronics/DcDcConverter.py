"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseComponent import BaseComponent

import pyomo.environ as pyo

class DcDcConverter(BaseComponent):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type='DcDcConverter',
                         commodity_1=ComponentCommodity.ELECTRICITY,
                         commodity_2=ComponentCommodity.ELECTRICITY,
                         commodity_3=ComponentCommodity.ELECTRICITY,
                         commodity_4=ComponentCommodity.ELECTRICITY,
                         configuration=configuration,
                         model_directory=model_directory,
                         dynamic=dynamic)
    
    def get_input_output_variable_names(self):
        return [((self.name, 'input_1'), 'T'), ((self.name, 'input_2'), 'T'), ((self.name, 'output_1'), 'T'), ((self.name, 'output_2'), 'T')]
    
    def add_input_output_variables(self, model, prefix):
        model.add(prefix + ('input_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('input_2',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_2',), pyo.Var(model.T, bounds=(0, None)))

    def _add_constraints(self, model, prefix, configuration):
        super()._add_constraints(model, prefix, configuration)
        self._constraint_bi_flow(model, prefix, configuration)

    def _constraint_capacity(self, model, prefix):
        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] <= model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('capacity_cons_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] <= model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('capacity_cons_2',), pyo.Constraint(model.T, rule = rule))

    def _constraint_conser(self, model, prefix, configuration):
        def rule(m, t):
            return model.component_dict[prefix + ('output_2',)][t] == model.component_dict[prefix + ('input_1',)][t] * self.efficiency
        model.add(prefix + ('conser_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] == model.component_dict[prefix + ('input_2',)][t] * self.efficiency
        model.add(prefix + ('conser_2',), pyo.Constraint(model.T, rule = rule))

    def _constraint_bi_flow(self, model, prefix, configuration):
        model.add(prefix + ('z_1',), pyo.Var(model.T, domain=pyo.Binary))
        model.add(prefix + ('z_2',), pyo.Var(model.T, domain=pyo.Binary))

        def rule(m, t):
            return model.component_dict[prefix + ('z_1',)][t] + model.component_dict[prefix + ('z_2',)][t] <= 1
        model.add(prefix + ('bi_flow_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] <= model.component_dict[prefix + ('z_1',)][t] * 100000
        model.add(prefix + ('bi_flow_2',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] <= model.component_dict[prefix + ('z_2',)][t] * 100000
        model.add(prefix + ('bi_flow_3',), pyo.Constraint(model.T, rule = rule))

    # Flexibility stuff

    def check_limits(self, input_flows, output_flows, results, time_steps):
        """
        ToDO JBR: clean
        calculate the consequences of maximal positive and negative flexibility. Positive flexibility can
        increase  (pos_flex_inc) and decrease (pos_flex_dec) the inputs of an Inverter.
        The negative flexibility increases/decreases the outputs of the Inverter.
        Therefore, they have to be transformed to input values.

        Parameters
        ----------
        input_flows: incoming power flows
        output_flows: outgoing power flows
        results: df with iniital prosumer results. Needed for obtaining the initial component size.
        time_steps: time steps of current RH interval
        """
        # flexibility that was already translated to the input side by def adjust_with_efficiency
        pos_flex_dec = self.temp_flex['flex_pos_dec'][time_steps]
        neg_flex_inc = self.temp_flex['flex_neg_inc'][time_steps]

        power_limit = 1 * results[(self.name, 'capacity')]  # excluding the excess power for safety

        # ----------------CHECK POWER--------------------

        # additional input power from positive flexibility
        # the increasing part has to be retransformed to the 'other' side of the comp becaus ethat ois  the value that is really increased
        # the decreasing part stays the same because the input flow of this flex type comes from the grid
        max_input_1 = input_flows + self.flex_pos_inc_old - pos_flex_dec
        # the decreasing part has to be retransformed to the 'other' side of the comp becaus ethat ois  the value that is really increased
        # the increasing part stays the same because the input flow of this flex type comes from the grid
        # max_input_2 = neg_flex_inc.combine(input_flows - self.flex_neg_dec_old,  max)  # additional input power from negative flexibility
        max_input_2 = input_flows + neg_flex_inc - self.flex_neg_dec_old  # additional input power from negative flexibility

        # upper limit for pos-inc df
        upper_limit = power_limit - input_flows + pos_flex_dec
        upper_limit.loc[upper_limit < 0] = 0  # account for the case input_flows > power_limit (excess)

        # max pos-inc df on input side of converter
        max_pos_inc = self.flex_pos_inc_old.combine(upper_limit, min)

        excess_input_1 = max_input_1 - power_limit
        excess_input_2 = max_input_2 - power_limit
        excess_input_1.loc[excess_input_1 < 0] = 0
        excess_input_2.loc[excess_input_2 < 0] = 0

        # adjust the flexibility values
        # the energy values are not affected because they are still availabele even if inter_comps can not handle the amount
        trans_excess_1 = excess_input_1 * self.efficiency
        trans_excess_1.loc[trans_excess_1 < 0] = 0

        self.temp_flex['flex_pos_inc'] -= trans_excess_1  # excess has to be transformed to inv output value
        self.temp_flex.loc[self.temp_flex['flex_pos_inc'] < 0, 'flex_pos_inc'] = 0

        self.temp_flex['flex_neg_inc'] -= excess_input_2
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] < 0, 'flex_neg_inc'] = 0

        self.temp_flex['flex_pos'] = self.temp_flex['flex_pos_inc'] + self.temp_flex['flex_pos_dec']
        self.temp_flex['flex_neg'] = self.temp_flex['flex_neg_inc'] + self.temp_flex['flex_neg_dec']
