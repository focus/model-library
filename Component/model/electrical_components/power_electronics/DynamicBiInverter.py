"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseComponent import BaseComponent

import warnings
from scipy.optimize import curve_fit
import numpy as np
import pyomo.environ as pyo
import copy
import json
import pandas as pd
import os

class DynamicBiInverter(BaseComponent):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type='DynamicBiInverter',
                         commodity_1=ComponentCommodity.ELECTRICITY,
                         commodity_2=ComponentCommodity.ELECTRICITY,
                         commodity_3=ComponentCommodity.ELECTRICITY,
                         commodity_4=ComponentCommodity.ELECTRICITY,
                         configuration=configuration,
                         model_directory=model_directory,
                         dynamic=dynamic)

        with open((os.path.join(model_directory, configuration['type'], configuration['model'] + '.json'))) as f:
            model = json.load(f)
        self.eff_vars = dict()
        if 'p1' in model:
            self.eff_vars['p1'] = model['p1']
        else:
            warnings.warn(f"No column for p1 for component {self.name}!")
        if 'p2' in model:
            self.eff_vars['p2'] = model['p2']
        else:
            warnings.warn(f"No column for p2 for component {self.name}!")
        if 'p3' in model:
            self.eff_vars['p3'] = model['p3']
        else:
            warnings.warn(f"No column for p3 for component {self.name}!")
        if 'eta1' in model:
            self.eff_vars['eta1'] = model['eta1']
        else:
            warnings.warn(f"No column for eta1 for component {self.name}!")
        if 'eta2' in model:
            self.eff_vars['eta2'] = model['eta2']
        else:
            warnings.warn(f"No column for eta2 for component {self.name}!")
        if 'eta3' in model:
            self.eff_vars['eta3'] = model['eta3']
        else:
            warnings.warn(f"No column for eta3 for component {self.name}!")
        if 'nominal_efficiency' in model:
            self.eta_norm = model['nominal_efficiency']
        else:
            warnings.warn(f"No column for nominal_efficiency for component {self.name}!")
        if 'curve_type' in model:
            self.curve_type = model['curve_type']
        else:
            self.curve_type = 'input_power'
            warnings.warn(f"No column for curve_type for component {self.name}!")
        # todo: replace the bigM with max capacity given in the input matrix
        self.bigM = 100

    def calculate_input_curve_parameters(self):
        """
        The PV inverter model based on D.Sauer's DA.
        The return value e, f are fitted parameters for equation:
        p_out[p.u.] = e * p_in[p.u.] - f
        """

        # based on sauer (diplomarbeit) and own calculations to aproximate a linear relationship
        p1 = self.eff_vars['p1']
        p2 = self.eff_vars['p2']
        p3 = self.eff_vars['p3']
        eta1 = self.eff_vars['eta1']
        eta2 = self.eff_vars['eta2']
        eta3 = self.eff_vars['eta3']

        def get_p_self_in():
            p_self_in = (p1 * p2 * p3 * (
                    eta1 * eta1 * p1 * (eta2 - eta3) + eta1 * (eta3 * eta3 * p3 - eta2 * eta2 * p2) + eta2 * eta3 *
                    (eta2 * p2 - eta3 * p3))) / \
                        ((eta1 * eta1 * p1 * p1 - eta1 * p1 * (eta2 * p2 + eta3 * p3) + eta2 * eta3 * p2 * p3) * (
                                eta2 * p2 - eta3 * p3))
            return p_self_in

        def get_v_loss_in():
            v_loss_in = (eta1 * eta1 * p1 * p1 * (eta2 * p2 - eta3 * p3 - p2 + p3) + eta1 * p1 * (
                    eta3 * eta3 * p3 * p3 - eta2 * eta2 * p2 * p2) +
                         eta2 * eta2 * p2 * p2 * (
                                 eta3 * p3 + p1 - p3) - eta2 * eta3 * eta3 * p2 * p3 * p3 + eta3 * eta3 * p3 * p3 *
                         (p2 - p1)) / ((eta1 * p1 - eta2 * p2) * (eta1 * p1 - eta3 * p3) * (eta3 * p3 - eta2 * p2))
            return v_loss_in

        def get_r_loss():
            r_loss_in = (eta1 * p1 * (p2 - p3) + eta2 * p2 * (p3 - p1) + eta3 * p3 * (p1 - p2)) / \
                        ((eta1 * eta1 * p1 * p1 - eta1 * p1 * (eta2 * p2 + eta3 * p3) + eta2 * eta3 * p2 * p3) * (
                                eta3 * p3 - eta2 * p2))
            return r_loss_in

        return [get_p_self_in(), get_v_loss_in(), get_r_loss()]

    def calculate_output_curve_parameters(self):
        p1 = self.eff_vars['p1']
        p2 = self.eff_vars['p2']
        p3 = self.eff_vars['p3']
        eta1 = self.eff_vars['eta1']
        eta2 = self.eff_vars['eta2']
        eta3 = self.eff_vars['eta3']

        def get_p_self_out():
            p_self_out = (p1 * p2 * p3 * (
                    eta1 * eta2 * (p1 - p2) + eta1 * eta3 * (p3 - p1) + eta2 * eta3 * (p2 - p3))) / \
                         (eta1 * eta2 * eta3 * (p1 - p2) * (p1 - p3) * (p2 - p3))
            return p_self_out

        def get_v_loss_out():
            v_loss_out = (eta1 * eta2 * (p1 - p2) * (
                    eta3 * (p2 - p3) * (p1 - p3) + p3 * (p1 + p2)) + eta1 * eta3 * p2 * (
                                  p3 * p3 - p1 * p1) + eta2 * eta3 * p1 * (p2 * p2 - p3 * p3)) / \
                         (eta1 * eta2 * eta3 * (p1 - p2) * (p1 - p3) * (p3 - p2))
            return v_loss_out

        def get_r_loss_out():
            r_loss_out = (eta1 * (eta2 * p3 * (p1 - p2) + eta3 * p2 * (p3 - p1)) + eta2 * eta3 * p1 * (p2 - p3)) / \
                         (eta1 * eta2 * eta3 * (p1 * p1 - p1 * (p2 + p3) + p2 * p3) * (p2 - p3))
            return r_loss_out

        return [get_p_self_out(), get_v_loss_out(), get_r_loss_out()]

    def transform_output_curve_parameters(self):
        vals = self.calculate_output_curve_parameters()
        p_self_in = vals[0] * self.eta_norm
        v_loss_in = vals[1]
        r_loss_in = vals[2] / self.eta_norm
        return [p_self_in, v_loss_in, r_loss_in]

    def calculate_efficiency_curve(self):

        assert self.curve_type == 0 or self.curve_type == 1, \
            'The curve type must be either input_power or output_power'
        if self.curve_type == 0:
            vals = self.calculate_input_curve_parameters()
        elif self.curve_type == 1:
            vals = self.transform_output_curve_parameters()

        a = -(1 + vals[1]) / (2 * vals[2])
        b = ((1 + vals[1]) ** 2) / (4 * vals[2] ** 2)
        c = 1 / vals[2]
        d = vals[0] / vals[2]

        # fit a linear curve to the original function
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
        # cite scipy and Levenberg-Marquardt. Uses least squares

        def ydata(x):
            return a + np.power((b - d + c * x), 0.5)

        def func(x, e, f):
            return x * e - f

        xdata = np.linspace(0, 1, 1000)
        y = ydata(xdata)
        popt, pcov = curve_fit(func, xdata, y)
        e = popt[0]
        f = popt[1]
        return e, f
    
    def get_base_variable_names(self):
        return super().get_base_variable_names() + self.get_power_flow_variable_names()
    
    def get_input_output_variable_names(self):
        return [((self.name, 'input_1'), 'T'), ((self.name, 'input_2'), 'T'), ((self.name, 'output_1'), 'T'), ((self.name, 'output_2'), 'T')]
    
    def get_power_flow_variable_names(self):
        return [((self.name, 'pin1_1'), 'T'), ((self.name, 'pin1_2'), 'T'), ((self.name, 'pin2_1'), 'T'), ((self.name, 'pin2_2'), 'T'), ((self.name, 'z_pin_1'), 'T'), ((self.name, 'z_pin_2'), 'T')]

    def _add_variables(self, model, prefix):
        super()._add_variables(model, prefix)
        self._add_power_flow_variables(model, prefix)
    
    def add_input_output_variables(self, model, prefix):
        model.add(prefix + ('input_1',), pyo.Var(model.T, bounds=(0, None)))
        
        model.add(prefix + ('input_2',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_2',), pyo.Var(model.T, bounds=(0, None)))

    def _add_power_flow_variables(self, model, prefix):
        model.add(prefix + ('pin1_1',), pyo.Var(model.T, bounds=(None, None)))

        model.add(prefix + ('pin1_2',), pyo.Var(model.T, bounds=(None, None)))

        model.add(prefix + ('pin2_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('pin2_2',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('z_pin_1',), pyo.Var(model.T, domain=pyo.Binary))

        model.add(prefix + ('z_pin_2',), pyo.Var(model.T, domain=pyo.Binary))

    def _add_constraints(self, model, prefix, configuration):
        super()._add_constraints(model, prefix, configuration)
        self._constraint_bi_flow(model, prefix, configuration)

    def _constraint_capacity(self, model, prefix):
        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] <= model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('capacity_cons_1',), pyo.Constraint(model.T, rule = rule))
        
        def rule(m, t):
            return model.component_dict[prefix + ('input_2',)][t] <= model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('capacity_cons_2',), pyo.Constraint(model.T, rule = rule))

    def _constraint_conser(self, model, prefix, configuration):
        # find out the component in flow dictionary according to name
        bigM = self.max_size  # * 10
        # Add power flow constraints
        e, f = self.calculate_efficiency_curve()
        # g = e / self.max_power  # avoid this step if we want to dimensionate the inverter, use of big m
        '''
        # non linear! because a continuous is multiplied by a binary
        model.cons.add(
            pyo.quicksum(var_dict[i][t] * e for i in input_powers) - f*self.max_power == var_dict[prefix + ('pin1',)][t])

        model.cons.add(var_dict[prefix + ('pin1',)][t] * var_dict[prefix + ('z_pin',)][t] ==
                        var_dict[prefix + ('pin2',)][t])

        model.cons.add(var_dict[prefix + ('pin1',)][t]/bigM + 1 >= var_dict[prefix + ('z_pin',)][t])

        model.cons.add(var_dict[prefix + ('z_pin',)][t] >= var_dict[prefix + ('pin1',)][t]/bigM)

        model.cons.add(var_dict[prefix + ('pin2',)][t] == pyo.quicksum(
            var_dict[i][t] for i in output_powers))
        # name=self.name + '_' + str(t)
        '''
        # True linear model

        # pin1 is continuous and unbounded (can be negative)
        # pin2 is continuous and bounded as positive (0,None)
        # z_pin is binary --> z_pin = 0 when pin1 < 0; z_pin = 1 when pin1 > 0

        # constraint 1: ∑_(𝑖∈𝐼𝑛𝑝𝑢𝑡𝑃𝑜𝑤𝑒𝑟𝑠)▒[𝑒∗𝑃_𝑖𝑛𝑝𝑢𝑡 [𝑖]] −𝑓∗𝑃_𝑖𝑛𝑣𝑒𝑟𝑡𝑒𝑟_𝑛𝑜𝑚=𝑝_𝑖𝑛′ = pin1   (1)

        # constraint 2: The following constraint ensures that 𝑝_𝑖𝑛′′ (continuous) be zero
        # if 𝑧_(𝑝_𝑖𝑛)(binary) is zero and bounds 𝑝_𝑖𝑛′′ to positive and negative big M if 𝑧_(𝑝_𝑖𝑛) is one.
        # −big𝑀∗𝑧_(𝑝_𝑖𝑛) ≤ 𝑝_𝑖𝑛′′ ≤ big𝑀∗𝑧_(𝑝_𝑖𝑛)           (2) --> p_in'' = 0 when z_pin = 0

        # constraint 3: The following constraint ensures that 𝑝_𝑖𝑛′′ (continuous) be
        # equal to 𝑝_𝑖𝑛′ (continuous) if 𝑧_(𝑝_𝑖𝑛)(binary) is one.
        # 𝑝_𝑖𝑛′−(1−𝑧_(𝑝_𝑖𝑛))∗big𝑀 ≤ 𝑝_𝑖𝑛′′ ≤ 𝑝_𝑖𝑛′−(1−𝑧_(𝑝_𝑖𝑛))∗(−big𝑀)     (3) --> p_in'' = p_in' when z_pin = 1

        # From (2) and (3) and the fact that 𝑝_𝑖𝑛^′′ is bounded as positive follows that 𝑧_(𝑝_𝑖𝑛 )
        # will be forced by (3) to be zero in the case of 𝑝_𝑖𝑛^′ being negative since otherwise 𝑝_𝑖𝑛^′′
        # would be equal to 𝑝_𝑖𝑛^′ (negative), which violates the bounds of 𝑝_𝑖𝑛^′′. With 𝑧_(𝑝_𝑖𝑛 )
        # set to 0, (2) will ensure that 𝑝_𝑖𝑛^′′ be 0.

        # The following constraint sets the final output of the inverter linearization
        # model to the sum of power outputs of the inverter (4).
        # 𝑝_𝑖𝑛′′=∑_(𝑖∈𝑂𝑢𝑡𝑝𝑢𝑡𝑃𝑜𝑤𝑒𝑟𝑠)[𝑃_𝑜𝑢𝑡𝑝𝑢𝑡 [𝑖]]

        # This constraint is the representation of a linear approximation of the original function, where e and f are
        # parameters of the linear relationship fitted to the simplification of the original non linear function
        # by means of a least squares regression implemented in self.calculate_efficiency_curve(). (1)
        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] * e - model.component_dict[prefix + ('capacity',)] * f == model.component_dict[prefix + ('pin1_1',)][t]
        model.add(prefix + ('conser_1_1',), pyo.Constraint(model.T, rule = rule))
        
        def rule(m, t):
            return model.component_dict[prefix + ('input_2',)][t] * e - model.component_dict[prefix + ('capacity',)] * f == model.component_dict[prefix + ('pin1_2',)][t]
        model.add(prefix + ('conser_1_2',), pyo.Constraint(model.T, rule = rule))

        # The following constraint (split in two due to expression syntax) ensures that pin2 (continuous) be zero if
        # z_pin (binary) is zero and bounds pin2 to positive and negative bigM if z_pin is one (2)
        def rule(m, t):
            return model.component_dict[prefix + ('z_pin_1',)][t] * -bigM <= model.component_dict[prefix + ('pin2_1',)][t]
        model.add(prefix + ('conser_2_1',), pyo.Constraint(model.T, rule = rule))
        
        def rule(m, t):
            return model.component_dict[prefix + ('z_pin_2',)][t] * -bigM <= model.component_dict[prefix + ('pin2_2',)][t]
        model.add(prefix + ('conser_2_2',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('pin2_1',)][t] <= model.component_dict[prefix + ('z_pin_1',)][t] * bigM
        model.add(prefix + ('conser_3_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('pin2_2',)][t] <= model.component_dict[prefix + ('z_pin_2',)][t] * bigM
        model.add(prefix + ('conser_3_2',), pyo.Constraint(model.T, rule = rule))

        # The following constraint (split in two due to expression syntax) ensures that pin2 (continuous) be equal
        # to pin1 (continuous) if z_pin (binary) is one. (3)
        def rule(m, t):
            return model.component_dict[prefix + ('pin1_1',)][t] - (1 - model.component_dict[prefix + ('z_pin_1',)][t]) * bigM <= model.component_dict[prefix + ('pin2_1',)][t]
        model.add(prefix + ('conser_4_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('pin1_2',)][t] - (1 - model.component_dict[prefix + ('z_pin_2',)][t]) * bigM <= model.component_dict[prefix + ('pin2_2',)][t]
        model.add(prefix + ('conser_4_2',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('pin1_1',)][t] + (1 - model.component_dict[prefix + ('z_pin_1',)][t]) * bigM >= model.component_dict[prefix + ('pin2_1',)][t]
        model.add(prefix + ('conser_5_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('pin1_2',)][t] + (1 - model.component_dict[prefix + ('z_pin_2',)][t]) * bigM >= model.component_dict[prefix + ('pin2_2',)][t]
        model.add(prefix + ('conser_5_2',), pyo.Constraint(model.T, rule = rule))

        # from (2) and (3) and the fact that pin2 is bounded as positive follows that z_pin will be forced by (3) to
        # be zero in the case of pin1 being negative since otherwise pin2 would be equal to pin1 (negative), which
        # violates the bounds of pin2. With z_pin set to 0, (2) will ensure that pin2 be 0.

        # The following constraint sets the final output of the inverter linearization model to the sum of power
        # outputs of the inverter (4)
        def rule(m, t):
            return model.component_dict[prefix + ('pin2_1',)][t] == model.component_dict[prefix + ('output_1',)][t]
        model.add(prefix + ('conser_6_1',), pyo.Constraint(model.T, rule = rule))
        
        def rule(m, t):
            return model.component_dict[prefix + ('pin2_2',)][t] == model.component_dict[prefix + ('output_2',)][t]
        model.add(prefix + ('conser_6_2',), pyo.Constraint(model.T, rule = rule))

        # The following constraints are not strictly necessary but they shrink the feasible space, allowing for
        # faster solving
        def rule(m, t):
            return model.component_dict[prefix + ('pin2_1',)][t] >= -bigM
        model.add(prefix + ('conser_7_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('pin2_2',)][t] >= -bigM
        model.add(prefix + ('conser_7_2',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('pin2_1',)][t] <= bigM
        model.add(prefix + ('conser_8_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('pin2_2',)][t] <= bigM
        model.add(prefix + ('conser_8_2',), pyo.Constraint(model.T, rule = rule))

        # model.cons.add(var_dict[prefix + ('pin2',)][t]<=var_dict[prefix + ('pin1',)][t]+(1 - var_dict[prefix + ('z_pin',)][t]) * bigM)

        # Explicitly enforces z = 0 for negative pin1 and z = 1 for positive, before implicit in variable bounds of
        # pin2
        def rule(m, t):
            return model.component_dict[prefix + ('pin1_1',)][t] / bigM + 1 >= model.component_dict[prefix + ('z_pin_1',)][t]
        model.add(prefix + ('conser_9_1',), pyo.Constraint(model.T, rule = rule))
        
        def rule(m, t):
            return model.component_dict[prefix + ('pin1_2',)][t] / bigM + 1 >= model.component_dict[prefix + ('z_pin_2',)][t]
        model.add(prefix + ('conser_9_2',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('z_pin_1',)][t] >= model.component_dict[prefix + ('pin1_1',)][t] / bigM
        model.add(prefix + ('conser_10_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('z_pin_2',)][t] >= model.component_dict[prefix + ('pin1_2',)][t] / bigM
        model.add(prefix + ('conser_10_2',), pyo.Constraint(model.T, rule = rule))

    def _constraint_bi_flow(self, model, prefix, configuration):
        model.add(prefix + ('z_1',), pyo.Var(model.T, domain=pyo.Binary))
        model.add(prefix + ('z_2',), pyo.Var(model.T, domain=pyo.Binary))

        def rule(m, t):
            return model.component_dict[prefix + ('z_1',)][t] + model.component_dict[prefix + ('z_2',)][t] <= 1
        model.add(prefix + ('bi_flow_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] <= model.component_dict[prefix + ('z_1',)][t] * 100000
        model.add(prefix + ('bi_flow_2',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('input_2',)][t] <= model.component_dict[prefix + ('z_2',)][t] * 100000
        model.add(prefix + ('bi_flow_3',), pyo.Constraint(model.T, rule = rule))

    # Flexibility stuff

    def check_limits(self, input_flows, output_flows, results, dynamic):
        """
        ToDO JBR: clean
        calculate the consequences of maximal positive and negative flexibility. Positive flexibility can
        increase  (pos_flex_inc) and decrease (pos_flex_dec) the inputs of an Inverter.
        The negative flexibility increases/decreases the outputs of the Inverter.
        Therefore, they have to be transformed to input values.

        Parameters
        ----------
        input_flows: incoming power flows
        output_flows: outgoing power flows
        results: df with iniital prosumer results. Needed for obtaining the initial component size.
        time_steps: time steps of current RH interval
        """
        pos_flex_dec = self.temp_flex['flex_pos_dec'][dynamic.time_steps()]  # curtailment of comp input

        neg_flex_inc = self.temp_flex['flex_neg_inc'][dynamic.time_steps()]  # increasing of comp input

        if not hasattr(self, 'limits'):
            self.limits = pd.DataFrame()

            self.limits['planned_input'] = input_flows
            self.limits['planned_output'] = output_flows
            self.limits['flex_input'] = input_flows
            self.limits['flex_output'] = output_flows

        power_limit = 1 * results[(self.name, 'capacity')]  # excluding the excess power for safety
        e, f = self.calculate_efficiency_curve()
        # ----------------CHECK POWER--------------------
        # Power of all input flows <= maximal power + excess power
        # ToDO. The possibility of rerouting the power flow from the PV into storage instead into the grid
        #  is not yet implemented --> missing negative flexibility

        # additional input power from positive flexibility
        # the increasing part has to be retransformed to the 'other' side of the comp becaus ethat ois  the value that is really increased
        # the decreasing part stays the same because the input flow of this flex type comes from the grid
        max_input_1 = input_flows + self.flex_pos_inc_old - pos_flex_dec
        # the decreasing part has to be retransformed to the 'other' side of the comp becaus ethat ois  the value that is really increased
        # the increasing part stays the same because the input flow of this flex type comes from the grid
        # max_input_2 = neg_flex_inc.combine(input_flows - self.flex_neg_dec_old,  max)  # additional input power from negative flexibility
        max_input_2 = input_flows + neg_flex_inc - self.flex_neg_dec_old  # additional input power from negative flexibility

        # upper limit for pos-inc df
        upper_limit_pos_inc = power_limit - input_flows
        upper_limit_pos_inc.loc[upper_limit_pos_inc < 0] = 0  # account for the case input_flows > power_limit (excess)

        # max pos-inc df on input side of converter
        max_pos_inc = self.flex_pos_inc_old.combine(upper_limit_pos_inc, min)

        # transferring the maximal allower pos-inc flex to the output side as it lays nearer to the grid
        max_pos_inc_out = max_pos_inc * e - f * results[(self.name, 'capacity')]
        max_pos_inc_out.loc[max_pos_inc_out < 0] = 0

        # upper limit for neg-inc df
        upper_limit_neg_inc = power_limit - input_flows
        upper_limit_neg_inc.loc[upper_limit_neg_inc < 0] = 0  # account for the case input_flows > power_limit (excess)

        # max neg-inc df on input side of converter
        max_neg_inc = neg_flex_inc.combine(upper_limit_neg_inc, min)

        # negative flex doesn't need conversion as it already has been
        max_neg_inc_out = max_neg_inc

        self.temp_flex['flex_pos_inc'] = max_pos_inc_out  # excess has to be transformed to inv output value
        self.temp_flex.loc[self.temp_flex['flex_pos_inc'] < 0, 'flex_pos_inc'] = 0

        self.temp_flex['flex_neg_inc'] = max_neg_inc_out
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] < 0, 'flex_neg_inc'] = 0

        self.temp_flex['flex_pos'] = self.temp_flex['flex_pos_inc'] + self.temp_flex['flex_pos_dec']
        self.temp_flex['flex_neg'] = self.temp_flex['flex_neg_inc'] + self.temp_flex['flex_neg_dec']

    def adjust_flex_with_efficiency(self, results, dynamic):
        """
        ToDO JBR: clean
        Adjust the DF values from other components.
        The theoretically available DF has to be adjusted according to the components efficiency.

        Parameters
        ----------
        results: result df from initial scheduling
        flows: all power flow variables of this prosumer
        input_profiles: not needed here
        T: time steps of current RH interval
        """
        # energy is still available after each flex activation. e.g.: If the efficiency would not affect the energy,
        # the community would always underestimate the effect of a positive flexibility activation on the energy amount
        # left

        self.flex_pos_inc_old = copy.deepcopy(self.temp_flex['flex_pos_inc'])
        self.flex_neg_dec_old = copy.deepcopy(self.temp_flex['flex_neg_dec'])

        # ------------EFFICIENCY--------------
        # the idea here is that the flexibility that comes out of the inverter is
        # related to the efficiency of the inverter.
        # So considering the power flow of the flexibility type,
        # the net flexibility gets higher or lower compared to the flexibility at the origin component

        e, f = self.calculate_efficiency_curve()
        # ------POSITIVE--------------
        # increasing positive flex means lower flex at grid
        self.temp_flex['flex_pos_inc'] = self.temp_flex['flex_pos_inc'] * e - f * results[(self.name, 'capacity')]
        self.temp_flex.loc[self.temp_flex['flex_pos_inc'] < 0, 'flex_pos_inc'] = 0

        # decreasing positive flex means higher flex at grid
        self.temp_flex.loc[self.temp_flex['flex_pos_dec'] > 0, 'flex_pos_dec'] = \
            (self.temp_flex['flex_pos_dec'] + f * results[(self.name, 'capacity')]) / e

        self.temp_flex['flex_pos'] = self.temp_flex['flex_pos_inc'] + self.temp_flex['flex_pos_dec']

        # ------NEGATIVE--------------
        # increasing negative flex means higher flex at grid
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] > 0, 'flex_neg_inc'] = \
            (self.temp_flex['flex_neg_inc'] + f * results[(self.name, 'capacity')]) /e

        # decreasing neg flex means lower flex at grid
        self.temp_flex['flex_neg_dec'] = (self.temp_flex['flex_neg_dec']) * e - f * results[(self.name, 'capacity')]
        self.temp_flex.loc[self.temp_flex['flex_neg_dec'] < 0, 'flex_neg_dec'] = 0  # fix error from efficiency error

        self.temp_flex['flex_neg'] = (self.temp_flex['flex_neg_inc'] + self.temp_flex['flex_neg_dec'])

    def calc_correction_factors(self, time_steps, c_f_df, results):
        """
        Calculates the correction factors (CF).
        For the inverter, with the dynamic behaviour, there exists a static part (only depending on comp. size) and
        a dynamic part (depending on power flow).

        Parameters
        ----------
        T: time steps of current RH interval
        c_f_df: correction factors of all components that have aleary been searched
        results: results of initial scheduling to get comp. size
        input_profiles: not necessary in this method.

        Returns
        -------
        c_f_df: updated CF dataframe
        """
        capacity = results[(self.name, 'capacity')]
        e, f = self.calculate_efficiency_curve()

        c_f_dch = pd.Series(data=1 / e, index=time_steps)
        c_static_dch = pd.Series(data=f * capacity / e, index=time_steps)
        c_f_cha = pd.Series(data=e, index=time_steps)
        c_static_cha = pd.Series(data=f*capacity, index=time_steps)

        c_f_df['c_f_dch'] = c_f_dch * c_f_df['c_f_dch']
        c_f_df['c_static_dch'] = c_static_dch + c_f_df['c_static_dch']

        c_f_df['c_f_cha'] = c_f_cha * c_f_df['c_f_cha']
        c_f_df['c_static_cha'] = c_static_cha + c_f_df['c_static_cha']

        return c_f_df