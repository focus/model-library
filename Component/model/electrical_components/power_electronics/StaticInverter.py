"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseComponent import BaseComponent

import pyomo.environ as pyo

class StaticInverter(BaseComponent):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type='StaticInverter',
                         commodity_1=ComponentCommodity.ELECTRICITY,
                         commodity_2=None,
                         commodity_3=ComponentCommodity.ELECTRICITY,
                         commodity_4=None,
                         configuration=configuration,
                         model_directory=model_directory,
                         dynamic=dynamic)

    def add_operating_costs(self, model, configuration):
        # penalize fluctuation of charging curve
        prefix = (self.name,)

        P = self.max_size # change to size of inverter

        model.add(prefix + ('z_increase',), pyo.Var(model.T, domain=pyo.Binary, initialize=0))
        model.add(prefix + ('z_decrease',), pyo.Var(model.T, domain=pyo.Binary, initialize=0))

        set_1 = set(list(model.T)[1:])
        set_2 = set(list(model.T)[1:-1])

        def rule_1(m):
            return model.component_dict[prefix + ('z_increase',)][0] == 0
        def rule_2(m):
            return model.component_dict[prefix + ('z_decrease',)][0] == 0
        def rule_3(m,t):
            return model.component_dict[prefix + ('z_increase',)][t] >= (model.component_dict[prefix + ('output_1',)][t] - model.component_dict[prefix + ('output_1',)][t-1]) / P
        def rule_4(m,t):
            return model.component_dict[prefix + ('z_decrease',)][t] >= (model.component_dict[prefix + ('output_1',)][t-1] - model.component_dict[prefix + ('output_1',)][t]) / P
        def rule_5(m, t):
            return model.component_dict[prefix + ('z_increase',)][t] + model.component_dict[prefix + ('z_decrease',)][t] <= 1
        def rule_6(m,t):
            return model.component_dict[prefix + ('power_fluctuation',)][t] == (model.component_dict[prefix + ('output_1',)][t] - model.component_dict[prefix + ('output_1',)][t-1]) * model.component_dict[prefix + ('z_increase',)][t] + \
                                                                             (model.component_dict[prefix + ('output_1',)][t-1] - model.component_dict[prefix + ('output_1',)][t]) * model.component_dict[prefix + ('z_decrease',)][t]
        # t=0
        model.add(prefix + ('penalty_1',), pyo.Constraint(rule=rule_1))
        model.add(prefix + ('penalty_2',), pyo.Constraint(rule=rule_2))
        # t=1
        model.add(prefix + ('penalty_3',), pyo.Constraint(set_1, rule=rule_3))
        model.add(prefix + ('penalty_4',), pyo.Constraint(set_1, rule=rule_4))
        model.add(prefix + ('penalty_5',), pyo.Constraint(set_1, rule=rule_5))

        model.add(prefix + ('power_fluctuation',), pyo.Var(model.T, within=pyo.NonNegativeReals, initialize=0))
        model.add(prefix + ('penalty_6',), pyo.Constraint(set_2, rule=rule_6))

        #penalty_cost = pyo.quicksum(model.component_dict[prefix + ('power_fluctuation',)][t] for t in model.T) * 0.001
        model.add(prefix + ('operating_cost',), pyo.Var(model.T))
        def rule_cost(m,t):
            return model.component_dict[prefix + ('operating_cost',)][t] == model.component_dict[prefix + ('power_fluctuation',)][t] * 0.001
        model.add(prefix + ('operating_cost_cons',), pyo.Constraint(model.T, rule=rule_cost))

        return prefix + ('operating_cost',)

    # Flexibility stuff

    def check_limits(self, input_flows, output_flows, results, dynamic):
        """
        ToDO JBR: clean
        calculate the consequences of maximal positive and negative flexibility. Positive flexibility can
        increase  (pos_flex_inc) and decrease (pos_flex_dec) the inputs of an Inverter.
        The negative flexibility increases/decreases the outputs of the Inverter.
        Therefore, they have to be transformed to input values.

        Parameters
        ----------
        input_flows: incoming power flows
        output_flows: outgoing power flows
        results: df with iniital prosumer results. Needed for obtaining the initial component size.
        time_steps: time steps of current RH interval
        """
        # flexibility that was already translated to the input side by def adjust_with_efficiency
        pos_flex_dec = self.temp_flex['flex_pos_dec'][dynamic.time_steps()]
        neg_flex_inc = self.temp_flex['flex_neg_inc'][dynamic.time_steps()]

        power_limit = 1 * results[(self.name, 'capacity')]  # excluding the excess power for safety

        # ----------------CHECK POWER--------------------

        # additional input power from positive flexibility
        # the increasing part has to be retransformed to the 'other' side of the comp becaus ethat ois  the value that is really increased
        # the decreasing part stays the same because the input flow of this flex type comes from the grid
        max_input_1 = input_flows + self.flex_pos_inc_old - pos_flex_dec
        # the decreasing part has to be retransformed to the 'other' side of the comp becaus ethat ois  the value that is really increased
        # the increasing part stays the same because the input flow of this flex type comes from the grid
        # max_input_2 = neg_flex_inc.combine(input_flows - self.flex_neg_dec_old,  max)  # additional input power from negative flexibility
        max_input_2 = input_flows + neg_flex_inc - self.flex_neg_dec_old  # additional input power from negative flexibility

        # upper limit for pos-inc df
        upper_limit = power_limit - input_flows + pos_flex_dec
        upper_limit.loc[upper_limit < 0] = 0  # account for the case input_flows > power_limit (excess)

        # max pos-inc df on input side of converter
        max_pos_inc = self.flex_pos_inc_old.combine(upper_limit, min)

        excess_input_1 = max_input_1 - power_limit
        excess_input_2 = max_input_2 - power_limit
        excess_input_1.loc[excess_input_1 < 0] = 0
        excess_input_2.loc[excess_input_2 < 0] = 0

        # adjust the flexibility values
        # the energy values are not affected because they are still availabele even if inter_comps can not handle the amount
        trans_excess_1 = excess_input_1 * self.efficiency
        trans_excess_1.loc[trans_excess_1 < 0] = 0

        self.temp_flex['flex_pos_inc'] -= trans_excess_1  # excess has to be transformed to inv output value
        self.temp_flex.loc[self.temp_flex['flex_pos_inc'] < 0, 'flex_pos_inc'] = 0

        self.temp_flex['flex_neg_inc'] -= excess_input_2
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] < 0, 'flex_neg_inc'] = 0

        self.temp_flex['flex_pos'] = self.temp_flex['flex_pos_inc'] + self.temp_flex['flex_pos_dec']
        self.temp_flex['flex_neg'] = self.temp_flex['flex_neg_inc'] + self.temp_flex['flex_neg_dec']
