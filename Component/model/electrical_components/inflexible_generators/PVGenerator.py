"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseComponent import BaseComponent
from Tooling.predictor.Predictor import Predictor
from Tooling.dynamics.Dynamic import resample

import warnings
import pyomo.environ as pyo
import json
import os

class PVGenerator(BaseComponent):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type='PVGenerator',
                         commodity_1=None,
                         commodity_2=None,
                         commodity_3=ComponentCommodity.ELECTRICITY,
                         commodity_4=None,
                         configuration=configuration,
                         model_directory=model_directory,
                         dynamic=dynamic)

        with open((os.path.join(model_directory, configuration['type'], configuration['model'] + '.json'))) as f:
            model = json.load(f)
        if 'NOCT' in model:
            self.noct = model['NOCT']
        else:
            warnings.warn(f"No column for NOCT for component {self.name}!")
        if 'temp_coefficient' in model:
            self.temp_coefficient = float(model['temp_coefficient'])
        else:
            warnings.warn(f"No column for temp_coefficient for component {self.name}!")

        if 'irradiance' in configuration:
            if isinstance(configuration['irradiance'], str):
                self.irradiance = resample(profiles[configuration['irradiance']][0], profiles[configuration['irradiance']][1], dynamic)
            elif isinstance(configuration['irradiance'], dict):
                self.irradiance = Predictor(resample(profiles[configuration['irradiance']['profile']][0], profiles[configuration['irradiance']['profile']][1], dynamic), configuration['irradiance']['type'], configuration['irradiance']['method'], dynamic)
        if 'temperature' in configuration:
            if isinstance(configuration['temperature'], str):
                self.temperature = resample(profiles[configuration['temperature']][0], profiles[configuration['temperature']][1], dynamic)
            elif isinstance(configuration['temperature'], dict):
                self.temperature = Predictor(resample(profiles[configuration['temperature']['profile']][0], profiles[configuration['temperature']['profile']][1], dynamic), configuration['temperature']['type'], configuration['temperature']['method'], dynamic)
        if 'power_factors' in configuration:
            if isinstance(configuration['power_factors'], str):
                self.power_factors = resample(profiles[configuration['power_factors']][0], profiles[configuration['power_factors']][1], dynamic)
            elif isinstance(configuration['power_factors'], dict):
                self.power_factors = Predictor(resample(profiles[configuration['power_factors']['profile']][0], profiles[configuration['power_factors']['profile']][1], dynamic), configuration['power_factors']['type'], configuration['power_factors']['method'], dynamic)

    def calculate_power_factors(self, irradiance, temperature):
        cell_temp = temperature + (irradiance / 800) * (self.noct - 20)
        return (irradiance / 1000) * (1 - self.temp_coefficient * (cell_temp - 25))
    
    def get_input_output_variable_names(self):
        return [((self.name, 'output_1'), 'T')]

    def add_input_output_variables(self, model, prefix):
        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))

    def _constraint_capacity(self, model, prefix):
        pass

    def _constraint_conser(self, model, prefix, configuration):
        model.add(prefix + ('power_factor',), pyo.Param(model.T, mutable = True))

        if hasattr(self, 'power_factors'):
            if isinstance(self.power_factors, Predictor):
                if 'predict' in configuration:
                    power_factors = self.power_factors.predict(list(model.T))
                else:
                    power_factors = resample(self.power_factors.profile, self.dynamic, model.dynamic)
            else:
                power_factors = resample(self.power_factors, self.dynamic, model.dynamic)
        else:
            if isinstance(self.irradiance, Predictor):
                if 'predict' in configuration:
                    irradiance = self.irradiance.predict(list(model.T))
                else:
                    irradiance = resample(self.irradiance.profile, self.dynamic, model.dynamic)
            else:
                irradiance = resample(self.irradiance, self.dynamic, model.dynamic)
            if isinstance(self.temperature, Predictor):
                if 'predict' in configuration:
                    temperature = self.temperature.predict(list(model.T))
                else:
                    temperature = resample(self.temperature.profile, self.dynamic, model.dynamic)
            else:
                temperature = resample(self.temperature, self.dynamic, model.dynamic)
            power_factors = self.calculate_power_factors(irradiance, temperature)
            power_factors.loc[lambda power_factor: power_factor > 1] = 1
            power_factors.loc[lambda power_factor: power_factor < 0] = 1

        model.set_value(prefix + ('power_factor',), power_factors)
            
        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] <= model.component_dict[prefix + ('capacity',)] * model.component_dict[prefix + ('power_factor',)][t]
        model.add(prefix + ('conser',), pyo.Constraint(model.T, rule = rule))
