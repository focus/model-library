"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseGrid import BaseGrid
from Tooling.predictor.Predictor import Predictor
from Tooling.dynamics.Dynamic import resample

import pyomo.environ as pyo

class ElectricalGrid(BaseGrid):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type='ElectricalGrid',
                         commodity=ComponentCommodity.ELECTRICITY,
                         configuration=configuration,
                         model_directory=model_directory,
                         profiles=profiles,
                         dynamic=dynamic)
        
        if 'emmision' in configuration:
            if isinstance(configuration['emission'], float) or isinstance(configuration['injection_price'], int):
                self.emission = configuration['emission']
            elif isinstance(configuration['emission'], str):
                self.emission = resample(profiles[configuration['emission']][0], profiles[configuration['emission']][1], dynamic)
            elif isinstance(configuration['emission'], dict):
                self.emission = Predictor(resample(profiles[configuration['emission']['profile']][0], profiles[configuration['emission']['profile']][1], dynamic), configuration['emission']['type'], configuration['emission']['method'], dynamic)
        else:
            self.emission = 0
        if 'peak_power_cost' in configuration:
            self.peak_power_cost = configuration['peak_power_cost']
        else:
            self.peak_power_cost = 0

    def add_co2_emissions(self, model, configuration):
        prefix = (self.name,)

        if isinstance(self.emission, float) or isinstance(self.emission, int):
            def rule(m, t):
                return model.component_dict[prefix + ('co2_emission',)][t] == model.component_dict[prefix + ('output_1',)][t] * self.emission * model.step_size(t)
            model.add(prefix + ('co2_emission_cons',), pyo.Constraint(model.T, rule = rule))
        else:
            model.add(prefix + ('emission',), pyo.Param(model.T, mutable = True))

            if isinstance(self.emission, Predictor):
                if 'predict' in configuration:
                    emission = self.emission.predict(list(model.T))
                else:
                    emission = resample(self.emission.profile, self.dynamic, model.dynamic)
            else:
                emission = resample(self.emission, self.dynamic, model.dynamic)

            model.set_value(prefix + ('emission',), emission)

            model.add(prefix + ('co2_emission',), pyo.Var(model.T))

            def rule(m, t):
                return model.component_dict[prefix + ('co2_emission',)][t] == model.component_dict[prefix + ('output_1',)][t] * model.component_dict[prefix + ('emission',)][t] * model.step_size(t)
            model.add(prefix + ('co2_emission_cons',), pyo.Constraint(model.T, rule = rule))

        return prefix + ('co2_emission',)

    def add_peak_power_costs(self, model):
        prefix = (self.name,)

        model.add(prefix + ('peak_import',), pyo.Var())

        model.add(prefix + ('peak_power_cost',), pyo.Var())

        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] <= model.component_dict[prefix + ('peak_import',)]
        model.add(prefix + ('peak_import_cons',), pyo.Constraint(model.T, rule = rule))

        model.add(prefix + ('peak_power_cost_cons',), pyo.Constraint(expr = model.component_dict[prefix + ('peak_import',)] * self.peak_power_cost - model.component_dict[prefix + ('peak_power_cost',)] == 0))
        return prefix + ('peak_power_cost',)
