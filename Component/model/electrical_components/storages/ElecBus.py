"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.electrical_components.storages.ElecVehicle import ElecVehicle

import pyomo.environ as pyo

class ElecBus(ElecVehicle):

    def __init__(self, name, configuration, model_directory, profiles, dynamic, type='ElecBus'):
        super().__init__(name=name,
                         configuration=configuration,
                         model_directory=model_directory,
                         profiles=profiles,
                         dynamic=dynamic,
                         type=type)

    def _add_variables(self, model, prefix):
        super()._add_variables(model, prefix)

    def _add_constraints(self, model, prefix, configuration):
        super()._add_constraints(model, prefix, configuration)
        self._constraint_final_soe(model, prefix)
        self._constraint_SOC_above_below_x(model, prefix, 0.5)
        #self._constraint_min_soe(model, prefix)

    def _constraint_conser(self, model, prefix, configuration):
        if 'storage_connect' in configuration and self.match(commodity=configuration['storage_connect']['commodity']):
            last_energy = configuration['storage_connect']['values'][self.name]
            model.add(prefix + ('fix_first_energy',), pyo.Constraint(expr = model.component_dict[prefix + ('energy',)][model.T_prime.first()] == last_energy))
        else:
            model.add(prefix + ('fix_first_energy',), pyo.Constraint(expr=model.component_dict[prefix + ('energy',)][model.T_prime.first()] <= model.component_dict[prefix + ('capacity',)]))

        def rule(m, t):
            return model.component_dict[prefix + ('energy',)][t] == model.component_dict[prefix + ('energy',)][model.T_prime[model.T_prime.ord(t) - 1]] + model.component_dict[prefix + ('input_1',)][t] * self.input_efficiency * model.step_size(t) - model.component_dict[prefix + ('output_1',)][t] / self.output_efficiency * model.step_size(t)
        model.add(prefix + ('conser',), pyo.Constraint(model.T, rule=rule))

    def _constraint_final_soe(self, model, prefix):
        model.add(prefix + ('fix_final_energy',), pyo.Constraint(expr=model.component_dict[prefix + ('energy',)][model.T.last()] >= model.component_dict[prefix + ('energy',)][model.T_prime.first()]))

    def _constraint_min_soe(self, model, prefix):
        def rule(m,t):
            return model.component_dict[prefix + ('energy',)][t] >= 0.3 * model.component_dict[prefix + ('capacity',)] * model.component_dict[prefix + ('z_input',)][t]
        model.add(prefix + ('min_SOE_while_CHA',), pyo.Constraint(model.T, rule=rule))

    def add_operating_costs(self, model, configuration):
        # opportunity costs for battery_energy_throughput and for SOC > 50%
        prefix = (self.name,)

        cost_function = lambda t: model.component_dict[prefix + ('input_1',)][t] * 0.2 + model.component_dict[prefix + ('z_SOC_above_50',)][t] * 0.0

        model.add(prefix + ('operating_cost',), pyo.Var(model.T))

        def rule(m,t ):
            return model.component_dict[prefix + ('operating_cost',)][t] == cost_function(t) * model.step_size(t)
        model.add(prefix + ('operating_cost_cons',), pyo.Constraint(model.T, rule=rule))

        return prefix + ('operating_cost',)
