"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.electrical_components.storages.LiionBattery import LiionBattery

import pyomo.environ as pyo
from pyomo.environ import value

class ElecVehicle(LiionBattery):

    def __init__(self, name, configuration, model_directory, profiles, dynamic, type='ElecVehicle'):
        super().__init__(name=name,
                         configuration=configuration,
                         model_directory=model_directory,
                         profiles=profiles,
                         dynamic=dynamic,
                         type=type)

    def _add_variables(self, model, prefix):
        super()._add_variables(model, prefix)

    def _add_constraints(self, model, prefix, configuration):
        super()._add_constraints(model, prefix, configuration)
        self._constraint_SOC_above_below_x(model, prefix, 0.3)
        self._constraint_SOC_above_below_x(model, prefix, 0.99)

    # constraint that introduces two binaries for the given SOC threshold, only 1 can be true at the same time for each respective threshold
    # binary = 1 indicates that condition is true
    # SOC should be passed as decimal number, not as percentage
    def _constraint_SOC_above_below_x(self, model, prefix, soc):
        model.add(prefix + ('z_SOC_below_' + str(int(soc*100)),), pyo.Var(model.T, within=pyo.Binary))
        model.add(prefix + ('z_SOC_above_' + str(int(soc*100)),), pyo.Var(model.T, within=pyo.Binary))

        def rule(m,t):
            return 1000 * self.max_size * model.component_dict[prefix + ('z_SOC_below_' + str(int(soc*100)),)][t] >= soc * 0.9999 * model.component_dict[prefix + ('capacity',)] - model.component_dict[prefix + ('energy',)][t]
        model.add(prefix + ('SOC_below_' + str(int(soc*100)) + '_cons',), pyo.Constraint(model.T, rule=rule))
        def rule(m,t):
            return 1000 * self.max_size * model.component_dict[prefix + ('z_SOC_above_' + str(int(soc*100)),)][t] >= model.component_dict[prefix + ('energy',)][t] - soc * model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('SOC_above_' + str(int(soc*100)) + '_cons',), pyo.Constraint(model.T, rule=rule))
        def rule(m,t):
            return model.component_dict[prefix + ('z_SOC_above_' + str(int(soc*100)),)][t] + model.component_dict[prefix + ('z_SOC_below_' + str(int(soc*100)),)][t] <= 1
        model.add(prefix + ('SOC_below_above_' + str(int(soc*100)) + '_cons',), pyo.Constraint(model.T, rule=rule))


