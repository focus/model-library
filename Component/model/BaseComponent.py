"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import AbstractComponent, ComponentKind, ComponentCommodity

import warnings
import math
import pyomo.environ as pyo
from Model_Library.Prosumer.scripts import calc_annuity_vdi2067
import pandas as pd
import json
import os

class BaseComponent(AbstractComponent):

    def __init__(self, name, type, commodity_1, commodity_2, commodity_3, commodity_4, configuration, model_directory, dynamic):
        super().__init__(name=name, 
                         type=type,
                         commodity_1=commodity_1,
                         commodity_2=commodity_2,
                         commodity_3=commodity_3,
                         commodity_4=commodity_4,
                         min_size=configuration['min_size'],
                         max_size=configuration['max_size'],
                         flexible=False,
                         dynamic=dynamic)

        with open((os.path.join(model_directory, configuration['type'], configuration['model'] + '.json'))) as f:
            model = json.load(f)
        if type not in ['BHKW', 'CHP', 'DynamicBiInverter', 'DynamicInverter', 'PEMFuelCell', 'PVGenerator', 'SolarThermalCollector']:
            if 'efficiency' in model:
                self.efficiency = model['efficiency']
            else:
                warnings.warn(f"No column for efficiency for component {self.name}!")
        if 'service_life' in model:
            self.life = model['service_life']
        else:
            warnings.warn(f"No column for service_life for component {self.name}!")
        if 'cost' in model:
            self.cost = model['cost']
        else:
            warnings.warn(f"No column for cost for component {self.name}!")
        if 'factor_repair_effort' in model:
            self.f_inst = model['factor_repair_effort']
        else:
            warnings.warn(f"No column for factor_repair_effort for component {self.name}!")
        if 'factor_servicing_effort' in model:
            self.f_w = model['factor_servicing_effort']
        else:
            warnings.warn(f"No column for factor_servicing_effort for component {self.name}!")
        if 'servicing_effort_hours' in model:
            self.f_op = model['servicing_effort_hours']
        else:
            warnings.warn(f"No column for servicing_effort_hours for component {self.name}!")

    def match(self, kind=ComponentKind.ALL, commodity=ComponentCommodity.ALL):
        match_kind = kind == ComponentKind.ALL or kind == ComponentKind.BASE
        match_commodity = commodity == ComponentCommodity.ALL or \
            commodity == self.input_commodity_1 or \
            commodity == self.input_commodity_2 or \
            commodity == self.output_commodity_1 or \
            commodity == self.output_commodity_2 or \
            (isinstance(commodity, list) and \
                (self.input_commodity_1 in commodity or \
                self.input_commodity_2 in commodity or \
                self.output_commodity_1 in commodity or \
                self.output_commodity_2 in commodity))
        return match_kind and match_commodity
            
    def get_base_variable_names(self):
        return self.get_power_variable_names() + self.get_input_output_variable_names()
    
    def get_power_variable_names(self):
        return [((self.name, 'capacity'), None)]
    
    def get_input_output_variable_names(self):
        return [((self.name, 'input_1'), 'T'), ((self.name, 'output_1'), 'T')]

    def _add_variables(self, model, prefix):
        self.add_power_variables(model, prefix)
        self.add_input_output_variables(model, prefix)

    def add_power_variables(self, model, prefix):
        lb_capacity = self.min_size
        ub_capacity = self.max_size
        if math.isinf(self.min_size):
            lb_capacity = None
        if math.isinf(self.max_size):
            ub_capacity = None

        model.add(prefix + ('capacity',), pyo.Var(bounds=(lb_capacity, ub_capacity)))
    
    def add_input_output_variables(self, model, prefix):
        model.add(prefix + ('input_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))
    
    def _add_constraints(self, model, prefix, configuration):
        self._constraint_capacity(model, prefix)
        self._constraint_conser(model, prefix, configuration)

        if 'fix_sizing' in configuration:
            model.add(prefix + ('fix_capacity',), pyo.Constraint(expr = model.component_dict[prefix + ('capacity',)] == configuration['fix_sizing']['values'][self.name]))

    def _constraint_capacity(self, model, prefix):
        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] <= model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('capacity_cons',), pyo.Constraint(model.T, rule = rule))

    def _constraint_conser(self, model, prefix, configuration):
        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] == model.component_dict[prefix + ('input_1',)][t] * self.efficiency
        model.add(prefix + ('conser',), pyo.Constraint(model.T, rule = rule))
    
    def add_capital_costs(self, model, prosumer_configuration):
        prefix = (self.name,)
        
        model.add(prefix + ('capital_cost',), pyo.Var(bounds=(0, None)))
        
        capital_cost = calc_annuity_vdi2067.run(prosumer_configuration['planning_horizon'],
                                               self.life,
                                               self.cost,
                                               model.component_dict[prefix + ('capacity',)],
                                               self.f_inst,
                                               self.f_w,
                                               self.f_op,
                                               prosumer_configuration['yearly_interest'])
        model.add(prefix + ('capital_cost_cons',), pyo.Constraint(expr = model.component_dict[prefix + ('capital_cost',)] == capital_cost))
        return prefix + ('capital_cost',)

    # Flexibility stuff

    def check_limits(self, input_flows, output_flows, results, dynamic):
        """
        ToDO JBR: clean
        calculate the consequences of maximal positive and negative flexibility. Positive flexibility can
        increase  (pos_flex_inc) and decrease (pos_flex_dec) the inputs of an Inverter.
        The negative flexibility increases/decreases the outputs of the Inverter.
        Therefore, they have to be transformed to input values.

        Parameters
        ----------
        input_flows: incoming power flows
        output_flows: outgoing power flows
        results: df with iniital prosumer results. Needed for obtaining the initial component size.
        time_steps: time steps of current RH interval
        """

        if not hasattr(self, 'efficiency'):
            return
        if (self.name, 'capacity') not in results:
            return

        # flexibility that was already translated to the input side by def adjust_with_efficiency
        pos_flex_dec = self.temp_flex['flex_pos_dec'][dynamic.time_steps()]
        neg_flex_inc = self.temp_flex['flex_neg_inc'][dynamic.time_steps()]

        power_limit = 1 * results[(self.name, 'capacity')]  # excluding the excess power for safety

        # ----------------CHECK POWER--------------------

        # upper limit for pos-inc df
        upper_limit_pos_inc = power_limit - input_flows
        upper_limit_pos_inc.loc[upper_limit_pos_inc < 0] = 0  # account for the case input_flows > power_limit (excess)

        # max pos-inc df on input side of converter
        max_pos_inc = self.flex_pos_inc_old.combine(upper_limit_pos_inc, min)

        # transferring the maximal allower pos-inc flex to the output side as it lays nearer to the grid
        max_pos_inc_out = max_pos_inc * self.efficiency
        max_pos_inc_out.loc[max_pos_inc_out < 0] = 0

        # upper limit for neg-inc df
        upper_limit_neg_inc = power_limit - input_flows
        upper_limit_neg_inc.loc[upper_limit_neg_inc < 0] = 0  # account for the case input_flows > power_limit (excess)

        # max neg-inc df on input side of converter
        max_neg_inc = neg_flex_inc.combine(upper_limit_neg_inc, min)

        # negative flex doesn't need conversion as it already has been
        max_neg_inc_out = max_neg_inc

        self.temp_flex['flex_pos_inc'] = max_pos_inc_out  # excess has to be transformed to inv output value
        # self.temp_flex['flex_pos_inc'].loc[self.temp_flex['flex_pos_inc'] < 0] = 0
        self.temp_flex.loc[self.temp_flex['flex_pos_inc'] < 0, 'flex_pos_inc'] = 0
        self.temp_flex['flex_neg_inc'] = max_neg_inc_out
        # self.temp_flex['flex_neg_inc'].loc[self.temp_flex['flex_neg_inc'] < 0] = 0
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] < 0, 'flex_neg_inc'] = 0

        self.temp_flex['flex_pos'] = self.temp_flex['flex_pos_inc'] + self.temp_flex['flex_pos_dec']
        self.temp_flex['flex_neg'] = self.temp_flex['flex_neg_inc'] + self.temp_flex['flex_neg_dec']

    def adjust_flex_with_efficiency(self, results, dynamic):
        """
        ToDO JBR: clean
        Adjust the DF values from other components.
        The theoretically available DF has to be adjusted according to the components efficiency.

        Parameters
        ----------
        results: result df from initial scheduling
        input_profiles: not needed here
        T: time steps of current RH interval
        """
        if not hasattr(self, 'efficiency'):
            self.flex_pos_inc_old = self.temp_flex['flex_pos_inc'].copy()
            self.flex_neg_dec_old = self.temp_flex['flex_neg_dec'].copy()
            return

        # ToDo: Is this correct? Efficiency effects energy as well because in the end the community has to know how much
        # energy is still available after each flex activation. e.g.: If the efficiency would not affect the energy,
        # the community would always underestimate the effect of a positive flexibility activation on the energy amount
        # left

        self.flex_pos_inc_old = self.temp_flex['flex_pos_inc'].copy()
        self.flex_neg_dec_old = self.temp_flex['flex_neg_dec'].copy()

        # ------------EFFICIENCY--------------
        # the idea here is that the flexibility that comes out of the inverter is
        # related to the efficiency of the inverter.
        # So considering the power flow of the flexibility type,
        # the net flexibility gets higher or lower compared to the flexibility at the origin component

        # ------POSITIVE--------------
        # increasing positive flex means lower flex at grid
        self.temp_flex['flex_pos_inc'] = self.temp_flex['flex_pos_inc'] * self.efficiency
        # decreasing positive flex means higher flex at grid
        self.temp_flex['flex_pos_dec'] = self.temp_flex['flex_pos_dec'] / self.efficiency

        self.temp_flex['flex_pos'] = self.temp_flex['flex_pos_inc'] + self.temp_flex['flex_pos_dec']

        # ------NEGATIVE--------------
        # increasing negative flex means higher flex at grid
        self.temp_flex['flex_neg_inc'] = self.temp_flex['flex_neg_inc'] / self.efficiency
        # decreasing neg flex means lower flex at grid
        self.temp_flex['flex_neg_dec'] = self.temp_flex['flex_neg_dec'] * self.efficiency

        self.temp_flex['flex_neg'] = (self.temp_flex['flex_neg_inc'] + self.temp_flex['flex_neg_dec'])

    def calc_correction_factors(self, time_steps, c_f_df, results):
        """
        Calculates the correction factors (CF).
        A general component will only have an input and output efficiency.
        If not, no calculation will be done and the cf df will not be changed.

        Parameters
        ----------
        T: time steps of current RH interval
        c_f_df: correction factors of all components that have aleary been searched
        results: results of initial scheduling to get comp. size
        input_profiles: not necessary in this method.

        Returns
        -------
        c_f_df: updated CF dataframe
        """
        try:
            c_f_dch = pd.Series(data=1/self.output_efficiency, index=time_steps)
            c_static_dch =pd.Series(data=0, index=time_steps)
            c_f_cha = pd.Series(data=self.input_efficiency, index=time_steps)
            c_static_cha = pd.Series(data=0, index=time_steps)

            c_f_df['c_f_dch'] = c_f_dch * c_f_df['c_f_dch'],
            c_f_df['c_static_dch'] = c_static_dch + c_f_df['c_static_dch']

            c_f_df['c_f_cha'] = c_f_cha * c_f_df['c_f_cha']
            c_f_df['c_static_cha'] = c_static_cha + c_f_df['c_static_cha']
        except:
            pass

        return c_f_df
