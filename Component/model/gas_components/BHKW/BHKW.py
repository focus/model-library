"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseComponent import BaseComponent

import warnings
import pyomo.environ as pyo
import json
import os

class BHKW(BaseComponent):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type="BHKW",
                         commodity_1=ComponentCommodity.GAS,
                         commodity_2=None,
                         commodity_3=ComponentCommodity.HEAT,
                         commodity_4=ComponentCommodity.ELECTRICITY,
                         configuration=configuration,
                         model_directory=model_directory,
                         dynamic=dynamic)

        with open((os.path.join(model_directory, configuration['type'], configuration['model'] + '.json'))) as f:
            model = json.load(f)
        if 'efficiency_elec_under50' in model:
            self.efficiency_elec_under50 = model['efficiency_elec_under50']
        else:
            warnings.warn(f"No column for efficiency_elec_under50 for component {self.name}!")
        if 'efficiency_heat_under50' in model:
            self.efficiency_heat_under50 = model['efficiency_heat_under50']
        else:
            warnings.warn(f"No column for efficiency_heat_under50 for component {self.name}!")
        if 'efficiency_elec_over50' in model:
            self.efficiency_elec_over50 = model['efficiency_elec_over50']
        else:
            warnings.warn(f"No column for efficiency_elec_over50 for component {self.name}!")
        if 'efficiency_heat_over50' in model:
            self.efficiency_heat_over50 = model['efficiency_heat_over50']
        else:
            warnings.warn(f"No column for efficiency_heat_over50 for component {self.name}!")
    
    def get_base_variable_names(self):
        return super().get_base_variable_names() + self.get_power_flow_variable_names()
    
    def get_input_output_variable_names(self):
        return [((self.name, 'input_1'), 'T'), ((self.name, 'output_1'), 'T'), ((self.name, 'output_2'), 'T')]
    
    def get_power_flow_variable_names(self):
        return [((self.name, 'p_el'), 'T'), ((self.name, 'p_th'), 'T'), ((self.name, 'p_g'), 'T'), ((self.name, 'z_50perc'), 'T')]

    def _add_variables(self, model, prefix):
        super()._add_variables(self, model, prefix)
        self._add_power_flow_variables(model, prefix)
        
    def add_input_output_variables(self, model, prefix):
        model.add(prefix + ('input_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_2',), pyo.Var(model.T, bounds=(0, None)))

    def _add_power_flow_variables(self, model, prefix):
        model.add(prefix + ('p_el',), pyo.Var(model.T))

        model.add(prefix + ('p_th',), pyo.Var(model.T))

        model.add(prefix + ('p_g',), pyo.Var(model.T))

        model.add(prefix + ('z_50perc',), pyo.Var(model.T, domain=pyo.Binary))

    def _constraint_capacity(self, model, prefix):
        def rule(m, t):
            return model.component_dict[prefix + ('output_2',)][t] <= model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('capacity_cons',), pyo.Constraint(model.T, rule = rule))

    def _constraint_conser(self, model, prefix, configuration):
        # p_g is the total gas input of this bhkw (continuous and unbounded)
        # constraint (1)
        # p_el is the total electrical output (continuous and unbounded)
        # constraint (2)
        # p_th is the total thermal output (continuous and unbounded)
        # constraint (3)
        # z_50perc is binary --> z_50perc = 0 when p_el <= 50% capacity; z_50perc = 1 when p_el >= 50% capacity
        # (p_g - 0.5*gas_power)/bigM <= z_50perc <= (p_g - 0.5*gas_power)/bigM + 1
        # constraint (4) (5)
        # eta_el_under50 * p_g - bigM * z_50perc <= p_el <= eta_el_under50 * p_g + bigM * z_50perc
        # p_el = eta_el_under50 * p_g if z_50perc = 0; p_el open, if z_50perc = 1
        # constraint (6) (7)
        # eta_heat_under50 * p_g - bigM * z_50perc <= p_th <= eta_heat_under50 * p_g + bigM * z_50perc
        # p_th = eta_heat_under50 * p_g if z_50perc = 0; p_th open, if z_50perc = 1
        # constraint (8) (9)
        # eta_el_over50 * p_g - bigM * (1-z_50perc) <= p_el <= eta_el_over50 * p_g + bigM * (1-z_50perc)
        # p_el = eta_el_over50 * p_g if z_50perc = 1; p_el open, if z_50perc = 0
        # constraint (10) (11)
        # eta_heat_over50 * p_g - bigM * (1-z_50perc) <= p_th <= eta_heat_over50 * p_g + bigM * (1-z_50perc)
        # p_th = eta_heat_over50 * p_g if z_50perc = 1; p_th open, if z_50perc = 0
        # constraint (12) (13)

        bigM = 1e10

            # constraint (1)
        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] == model.component_dict[prefix + ('p_g',)][t]
        model.add(prefix + ('conser_1',), pyo.Constraint(model.T, rule = rule))

            # constraint (2)
        def rule(m, t):
            return model.component_dict[prefix + ('output_2',)][t] == model.component_dict[prefix + ('p_el',)][t]
        model.add(prefix + ('conser_2',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (3)
        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] == model.component_dict[prefix + ('p_th',)][t]
        model.add(prefix + ('conser_3',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (4)
        def rule(m, t):
            return (model.component_dict[prefix + ('p_el',)][t] - 0.5 * model.component_dict[prefix + ('capacity',)]) / bigM + 1 >= model.component_dict[prefix + ('z_50perc',)][t]
        model.add(prefix + ('conser_4',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (5)
        def rule(m, t):
            return (model.component_dict[prefix + ('p_el',)][t] - 0.5 * model.component_dict[prefix + ('capacity',)]) / bigM <= model.component_dict[prefix + ('z_50perc',)][t]
        model.add(prefix + ('conser_5',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (6)
        def rule(m, t):
            return model.component_dict[prefix + ('p_g',)][t] * self.efficiency_elec_under50 - bigM * model.component_dict[prefix + ('z_50perc',)][t] <= model.component_dict[prefix + ('p_el',)][t]
        model.add(prefix + ('conser_6',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (7)
        def rule(m, t):
            return model.component_dict[prefix + ('p_g',)][t] * self.efficiency_elec_under50 + bigM * model.component_dict[prefix + ('z_50perc',)][t] >= model.component_dict[prefix + ('p_el',)][t]
        model.add(prefix + ('conser_7',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (8)
        def rule(m, t):
            return model.component_dict[prefix + ('p_g',)][t] * self.efficiency_heat_under50 - bigM * model.component_dict[prefix + ('z_50perc',)][t] <= model.component_dict[prefix + ('p_th',)][t]
        model.add(prefix + ('conser_8',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (9)
        def rule(m, t):
            return model.component_dict[prefix + ('p_g',)][t] * self.efficiency_heat_under50 + bigM * model.component_dict[prefix + ('z_50perc',)][t] >= model.component_dict[prefix + ('p_th',)][t]
        model.add(prefix + ('conser_9',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (10)
        def rule(m, t):
            return model.component_dict[prefix + ('p_g',)][t] * self.efficiency_elec_over50 - bigM * (1 - model.component_dict[prefix + ('z_50perc',)][t]) <= model.component_dict[prefix + ('p_el',)][t]
        model.add(prefix + ('conser_10',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (11)
        def rule(m, t):
            return model.component_dict[prefix + ('p_g',)][t] * self.efficiency_elec_over50 + bigM * (1 - model.component_dict[prefix + ('z_50perc',)][t]) >= model.component_dict[prefix + ('p_el',)][t]
        model.add(prefix + ('conser_11',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (12)
        def rule(m, t):
            return model.component_dict[prefix + ('p_g',)][t] * self.efficiency_heat_over50 - bigM * (1 - model.component_dict[prefix + ('z_50perc',)][t]) <= model.component_dict[prefix + ('p_th',)][t]
        model.add(prefix + ('conser_12',), pyo.Constraint(model.T, rule = rule))
        
            # constraint (13)
        def rule(m, t):
            return model.component_dict[prefix + ('p_g',)][t] * self.efficiency_heat_over50 + bigM * (1 - model.component_dict[prefix + ('z_50perc',)][t]) >= model.component_dict[prefix + ('p_th',)][t]
        model.add(prefix + ('conser_13',), pyo.Constraint(model.T, rule = rule))
