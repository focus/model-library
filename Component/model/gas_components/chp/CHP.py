"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseComponent import BaseComponent

import warnings
import pyomo.environ as pyo
import json
import os

class CHP(BaseComponent):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type='CHP',
                         commodity_1=ComponentCommodity.GAS,
                         commodity_2=None,
                         commodity_3=ComponentCommodity.HEAT,
                         commodity_4=ComponentCommodity.ELECTRICITY,
                         configuration=configuration,
                         model_directory=model_directory,
                         dynamic=dynamic)

        with open((os.path.join(model_directory, configuration['type'], configuration['model'] + '.json'))) as f:
            model = json.load(f)
        if 'efficiency_heat' in model:
            self.efficiency_heat = model['efficiency_heat']
        else:
            warnings.warn(f"No column for efficiency_heat for component {self.name}!")
        if 'efficiency_elec' in model:
            self.efficiency_elec = model['efficiency_elec']
        else:
            warnings.warn(f"No column for efficiency_elec for component {self.name}!")
    
    def get_input_output_variable_names(self):
        return [((self.name, 'input_1'), 'T'), ((self.name, 'output_1'), 'T'), ((self.name, 'output_2'), 'T')]

    def add_input_output_variables(self, model, prefix):
        model.add(prefix + ('input_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_2',), pyo.Var(model.T, bounds=(0, None)))

    def _constraint_capacity(self, model, prefix):
        def rule(m, t):
            return model.component_dict[prefix + ('output_2',)][t] <= model.component_dict[prefix + ('capacity',)]
        model.add(prefix + ('capacity_cons',), pyo.Constraint(model.T, rule = rule))

    def _constraint_conser(self, model, prefix, configuration):
        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] == model.component_dict[prefix + ('input_1',)][t] * self.efficiency_heat
        model.add(prefix + ('conser_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('output_2',)][t] == model.component_dict[prefix + ('input_1',)][t] * self.efficiency_elec
        model.add(prefix + ('conser_2',), pyo.Constraint(model.T, rule = rule))
