"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import pandas as pd
from enum import Enum

class ComponentKind(Enum):
    ALL = 1,
    BASE = 2,
    BUSBAR = 3,
    CONSUMPTION = 4,
    GENERATION = 5,
    GRID = 6,
    STORAGE = 7

class ComponentCommodity(Enum):
    ALL = 1,
    ELECTRICITY = 2,
    HEAT = 3,
    GAS = 4,
    COLD = 5,
    HYDROGEN = 6

class AbstractComponent:

    def __init__(self, name, type, commodity_1, commodity_2, commodity_3, commodity_4, min_size, max_size, flexible, dynamic):
        self.name = name
        self.type = type
        self.input_commodity_1 = commodity_1
        self.input_commodity_2 = commodity_2
        self.output_commodity_1 = commodity_3
        self.output_commodity_2 = commodity_4
        self.min_size = min_size
        self.max_size = max_size
        self.flexible = flexible
        self.dynamic = dynamic
        self.flex = pd.DataFrame()
        self.temp_flex = pd.DataFrame()

    def get_input_output_commodities(self):
        return (self.input_commodity_1, self.input_commodity_2, self.output_commodity_1, self.output_commodity_2)
    
    def get_base_variable_names(self):
        pass

    def build_model(self, model, configuration):
        prefix = (self.name,)
        self._add_variables(model, prefix)
        self._add_constraints(model, prefix, configuration)

    def _add_variables(self, model, prefix):
        pass

    def _add_constraints(sefl, model, prefix, configuration):
        pass
    
    def add_capital_costs(self, model, prosumer_configuration):
        return None
    
    def add_operating_costs(self, model, configuration):
        return None

    def add_co2_emissions(self, model, configuration):
        # heat https://www.uni-goettingen.de/de/document/download/e778b3727c64ed6f962e4c1cea80fa2f.pdf/CO2%20Emissionen_2016.pdf
        # https://www.umweltbundesamt.de/presse/pressemitteilungen/bilanz-2019-co2-emissionen-pro-kilowattstunde-strom
        pass

    def add_peak_power_costs(self, model):
        return None

    # Flexibility stuff

    def calc_flex_comp(self, results, dynamic, init_results):
        pass

    def check_limits(self, input_flows, output_flows, results, dynamic):
        pass

    def adjust_flex_with_efficiency(self, results, dynamic):
        """
        Function is only called for not flexible components. BaseComponent overwrites this function. BaseConsumption and BaseGrid do not have a efficiency so the just copy over the values.
        Adjust the DF values from other components.
        The theoretically available DF has to be adjusted according to the components efficiency.

        Parameters
        ----------
        results: result df from initial scheduling
        input_profiles: not needed here
        T: time steps of current RH interval
        """
        self.flex_pos_inc_old = self.temp_flex['flex_pos_inc'].copy()
        self.flex_neg_dec_old = self.temp_flex['flex_neg_dec'].copy()

    def calc_correction_factors(self, time_steps, c_f_df, results):
        return c_f_df
