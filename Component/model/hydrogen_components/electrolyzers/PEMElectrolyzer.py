"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.AbstractComponent import ComponentCommodity
from Model_Library.Component.model.BaseComponent import  BaseComponent

import pandas as pd

class PEMElectrolyzer(BaseComponent):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type='PEMElectrolyzer',
                         commodity_1=ComponentCommodity.ELECTRICITY,
                         commodity_2=None,
                         commodity_3='hydrogen',
                         commodity_4=None,
                         configuration=configuration,
                         model_directory=model_directory,
                         dynamic=dynamic)

    # Flexibility stuff

    def check_limits(self, input_flows, output_flows, results, dynamic):
        """
        calculate the consequences of maximal positive and negative flexibility. Positive flexibility can
        increase  (pos_flex_inc) and decrease (pos_flex_dec) the inputs of an Inverter.
        The negative flexibility increases/decreases the outputs of the Inverter.
        Therefore, they have to be transformed to input values.
        A
        Parameters
        ----------
        path_comps
        comp

        Returns
        -------
        diff --> the differences between maximal input flows from flexibility usage and power limit
        """

        # flexibility that was already translated to the input side by def adjust_with_efficiency
        pos_flex_dec = self.temp_flex['flex_pos_dec'][dynamic.time_steps()]
        neg_flex_inc = self.temp_flex['flex_neg_inc'][dynamic.time_steps()]

        power_limit = 1 * results[(self.name, 'capacity')]  # excluding the excess power for safety

        # ----------------CHECK POWER--------------------

        # additional input power from positive flexibility
        # the increasing part has to be retransformed to the 'other' side of the comp becaus ethat ois  the value that is really increased
        # the decreasing part stays the same because the input flow of this flex type comes from the grid
        max_input_1 = input_flows + self.flex_pos_inc_old - pos_flex_dec
        # the decreasing part has to be retransformed to the 'other' side of the comp becaus ethat ois  the value that is really increased
        # the increasing part stays the same because the input flow of this flex type comes from the grid
        # max_input_2 = neg_flex_inc.combine(input_flows - self.flex_neg_dec_old,  max)  # additional input power from negative flexibility
        max_input_2 = input_flows + neg_flex_inc - self.flex_neg_dec_old  # additional input power from negative flexibility


        excess_input_1 = max_input_1 - power_limit
        excess_input_2 = max_input_2 - power_limit
        excess_input_1.loc[excess_input_1 < 0] = 0
        excess_input_2.loc[excess_input_2 < 0] = 0

        # adjust the flexibility values
        # the energy values are not affected because they are still availabele even if inter_comps can not handle the amount
        trans_excess_1 = excess_input_1 * self.efficiency
        trans_excess_1.loc[trans_excess_1 < 0] = 0

        self.temp_flex['flex_pos_inc'] -= trans_excess_1  # excess has to be transformed to inv output value
        self.temp_flex.loc[self.temp_flex['flex_pos_inc'] < 0, 'flex_pos_inc'] = 0

        self.temp_flex['flex_neg_inc'] -= excess_input_2
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] < 0, 'flex_neg_inc'] = 0

        self.temp_flex['flex_pos'] = self.temp_flex['flex_pos_inc'] + self.temp_flex['flex_pos_dec']
        self.temp_flex['flex_neg'] = self.temp_flex['flex_neg_inc'] + self.temp_flex['flex_neg_dec']

    def adjust_flex_with_efficiency(self, results, dynamic):

        # ToDo: Is this correct? Efficiency effects energy as well because in the end the community has to know how much
        # energy is still available after each flex activation. e.g.: If the efficiency would not affect the energy,
        # the community would always underestimate the effect of a positive flexibility activation on the energy amount
        # left

        # an electroyzer does just offer negative increasing flex or positive decreasing
        self.flex_pos_inc_old = self.temp_flex['flex_pos_inc'].copy() * 0
        self.flex_neg_dec_old = self.temp_flex['flex_neg_dec'].copy() * 0

        # ------------EFFICIENCY--------------
        # the idea here is that the flexibility that comes out of the inverter is
        # related to the efficiency of the inverter.
        # So considering the power flow of the flexibility type,
        # the net flexibility gets higher or lower compared to the flexibility at the origin component

        # ------POSITIVE--------------
        # increasing positive flex means lower flex at grid
        self.temp_flex['flex_pos_inc'] = self.temp_flex['flex_pos_inc'] * 0
        # decreasing positive flex means higher flex at grid
        self.temp_flex['flex_pos_dec'] = self.temp_flex['flex_pos_dec'] / self.efficiency

        self.temp_flex['flex_pos'] = self.temp_flex['flex_pos_inc'] + self.temp_flex['flex_pos_dec']

        # ------NEGATIVE--------------
        # increasing negative flex means higher flex at grid
        self.temp_flex['flex_neg_inc'] = self.temp_flex['flex_neg_inc'] / self.efficiency
        # decreasing neg flex means lower flex at grid
        self.temp_flex['flex_neg_dec'] = self.temp_flex['flex_neg_dec'] * 0

        self.temp_flex['flex_neg'] = (self.temp_flex['flex_neg_inc'] + self.temp_flex['flex_neg_dec'])

    def calc_correction_factors(self, time_steps, c_f_df, results):

        c_f_cha = pd.Series(data=self.efficiency, index=time_steps)
        c_static_cha = pd.Series(data=0, index=time_steps)

        c_f_df['c_f_cha'] = c_f_cha * c_f_df['c_f_cha']
        c_f_df['c_static_cha'] = c_static_cha + c_f_df['c_static_cha']

        return c_f_df
