"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

from Model_Library.Component.model.BaseStorage import BaseStorage

import pandas as pd

class PressureStorage(BaseStorage):

    def __init__(self, name, configuration, model_directory, profiles, dynamic):
        super().__init__(name=name,
                         type='PressureStorage',
                         commodity='hydrogen',
                         configuration=configuration,
                         model_directory=model_directory)

    # Flexibility stuff

    def calc_flex_comp(self, results, dynamic, init_results):
        """
        #TODO in what way is this method different from calc_flex_comp in BaseStoreage
        This method calculates the maximal possible flexibility from the battery.
        This is done by comparing two values:
            1) The maximal possible CHA/DCH power
            2) The maximal allowed CHA/DCH power regarding the available power BEFORE the PLANNED CHA/DCH
        The minimal value is chosen to be the maximal possible CHA/DCH power. To claculate the available flexibility from this,
        the PLANNED CHA/DCH rates are added/substracted depending on the type of flexibility.
        E.g. positive flex: the current DCH power is substracted and the current CHA power is added
        Parameters
        ----------
        flows
        results
        T

        Returns
        -------

        """

        cap_storage = results[(self.name, 'capacity')]  # the total theoretical capacity of the comm_batt
        e_storage = results[(self.name, 'energy')]


        # maximal in/out powers that the comm_batt is capable of
        max_power_out = cap_storage * self.e2p_out
        max_power_in = cap_storage * self.e2p_in

        # get the total output/input powers
        total_dch = pd.Series(data=0.0, index=dynamic.time_steps())
        total_dch += results[(self.name, 'output_1')] #  self.output_efficiency #here not necessary

        total_cha = pd.Series(data=0.0, index=dynamic.time_steps())
        total_cha += results[(self.name, 'input_1')] # self.input_efficiency

        # these are the power values that are theoretically available
        # for flexibility through CHA and DCH (missing curtailment)
        max_power_dch = max_power_out
        max_power_cha = max_power_in

        # now the energy restriction is calculated
        # ToDO @jbr: in the future here should be an extra restriction if some SOE values
        #  have to be reached in future timesteps
        e_cha = pd.Series(index=dynamic.time_steps())
        e_dch = pd.Series(index=dynamic.time_steps())

        for t in dynamic.time_steps():
            if total_dch[t] > 0:
                e_dch[t] = (e_storage[t] - 0.2 * cap_storage[t])
            else:
                e_dch[t] = 0

            if total_cha[t] > 0:
                e_cha[t] = (0.8 * cap_storage[t] - e_storage[t] )
            else:
                e_cha[t] = 0


        for t in dynamic.time_steps():
            if e_dch[t] < -1 * e_cha[t]:
                print('Fehler zum Zeitpunkt ' + str(t))

        # problem with these ones is: a negative energy means that the energy level is in "restricted" levels.
        # This may lead to infeasailities as this constraint requires the Prosumer to provide charge/discharge
        # even if this is not possible. But this should be correct

        #e_cha.loc[e_cha < 0] = 0
        #e_dch.loc[e_dch < 0] = 0

        # pos flex:
        self.temp_flex['flex_pos_inc'] = max_power_dch - total_dch
        #self.temp_flex['flex_pos_inc'].loc[self.temp_flex['flex_pos_inc'] < 0] = 0
        self.temp_flex.loc[self.temp_flex['flex_pos_inc'] < 0, 'flex_pos_inc'] = 0
        self.temp_flex['flex_pos_dec'] = total_cha
        self.temp_flex['flex_pos'] = max_power_dch - total_dch + total_cha
        self.temp_flex.loc[self.temp_flex['flex_pos'] < 0, 'flex_pos'] = 0
        #self.temp_flex['flex_pos'].loc[self.temp_flex['flex_pos'] < 0] = 0
        self.temp_flex['e_dch'] = e_dch

        # neg flex: negative signs due to negative flexibility
        self.temp_flex['flex_neg_inc'] = max_power_cha - total_cha
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] < 0, 'flex_neg_inc'] = 0
        #self.temp_flex['flex_neg_inc'].loc[self.temp_flex['flex_neg_inc'] < 0] = 0
        self.temp_flex['flex_neg_dec'] = total_dch
        self.temp_flex['flex_neg'] = max_power_cha - total_cha + total_dch
        self.temp_flex.loc[self.temp_flex['flex_neg'] < 0, 'flex_neg'] = 0
        #self.temp_flex['flex_neg'].loc[self.temp_flex['flex_neg'] < 0] = 0
        self.temp_flex['e_cha'] = e_cha
