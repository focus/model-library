"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from Model_Library.Component.model.AbstractComponent import AbstractComponent, ComponentKind, ComponentCommodity

import warnings
import math
import pyomo.environ as pyo
from Model_Library.Prosumer.scripts import calc_annuity_vdi2067
import pandas as pd
import json
import os

class BaseStorage(AbstractComponent):

    def __init__(self, name, type, commodity, configuration, model_directory, dynamic):
        super().__init__(name=name,
                         type=type,
                         commodity_1=commodity,
                         commodity_2=None,
                         commodity_3=commodity,
                         commodity_4=None,
                         min_size=configuration['min_size'],
                         max_size=configuration['max_size'],
                         flexible=True,
                         dynamic=dynamic)
        
        self.commodity = commodity

        with open((os.path.join(model_directory, configuration['type'], configuration['model'] + '.json'))) as f:
            model = json.load(f)
        if 'input_efficiency' in model:
            self.input_efficiency = model['input_efficiency']
        else:
            warnings.warn(f"No column for input_efficiency for component {self.name}!")
        if 'e2p_in' in model:
            self.e2p_in = model['e2p_in']
        else:
            warnings.warn(f"No column for e2p_in for component {self.name}!")
        if 'output_efficiency' in model:
            self.output_efficiency = model['output_efficiency']
        else:
            warnings.warn(f"No column for output_efficiency for component {self.name}!")
        if 'e2p_out' in model:
            self.e2p_out = model['e2p_out']
        else:
            warnings.warn(f"No column for e2p_out for component {self.name}!")
        if 'min_soe' in model:
            self.min_soe = model['min_soe']
        else:
            warnings.warn(f"No column for min_soe for component {self.name}!")
        if 'max_soe' in model:
            self.max_soe = model['max_soe']
        else:
            warnings.warn(f"No column for max_soe for component {self.name}!")
        if 'init_soe' in model:
            self.init_soe = model['init_soe']
        else:
            warnings.warn(f"No column for init_soe for component {self.name}!")
        if 'service_life' in model:
            self.life = model['service_life']
        else:
            warnings.warn(f"No column for service_life for component {self.name}!")
        if 'cost' in model:
            self.cost = model['cost']
        else:
            warnings.warn(f"No column for cost for component {self.name}!")
        if 'factor_repair_effort' in model:
            self.f_inst = model['factor_repair_effort']
        else:
            warnings.warn(f"No column for factor_repair_effort for component {self.name}!")
        if 'factor_servicing_effort' in model:
            self.f_w = model['factor_servicing_effort']
        else:
            warnings.warn(f"No column for factor_servicing_effort for component {self.name}!")
        if 'servicing_effort_hours' in model:
            self.f_op = model['servicing_effort_hours']
        else:
            warnings.warn(f"No column for servicing_effort_hours for component {self.name}!")

    def match(self, kind=ComponentKind.ALL, commodity=ComponentCommodity.ALL):
        match_kind = kind == ComponentKind.ALL or kind == ComponentKind.STORAGE
        match_commodity = commodity == ComponentCommodity.ALL or \
            commodity == self.commodity or \
            (isinstance(commodity, list) and self.commodity in commodity)
        return match_kind and match_commodity
    
    def get_base_variable_names(self):
        return [((self.name, 'capacity'), None), ((self.name, 'energy'), 'T_prime'), ((self.name, 'input_1'), 'T'), ((self.name, 'output_1'), 'T')]

    def _add_variables(self, model, prefix):
        lb_cap = self.min_size
        ub_cap = self.max_size
        if math.isinf(lb_cap):
            lb_cap = None
        if math.isinf(ub_cap):
            ub_cap = None

        model.add(prefix + ('capacity',), pyo.Var(bounds=(lb_cap, ub_cap)))

        model.add(prefix + ('energy',), pyo.Var(model.T_prime, bounds=(0, None)))

        model.add(prefix + ('input_1',), pyo.Var(model.T, bounds=(0, None)))

        model.add(prefix + ('output_1',), pyo.Var(model.T, bounds=(0, None)))

    def _add_constraints(self, model, prefix, configuration):
        self._constraint_capacity(model, prefix, configuration)
        self._constraint_conser(model, prefix, configuration)
        self._constraint_bi_flow(model, prefix)

        if 'fix_sizing' in configuration:
            model.add(prefix + ('fix_capacity',), pyo.Constraint(expr = model.component_dict[prefix + ('capacity',)] == configuration['fix_sizing']['values'][self.name]))

        if 'storage_slack' in configuration and self.match(commodity=configuration['storage_slack']['commodity']):
            model.add(prefix + ('s_p_energy',), pyo.Var(model.T, bounds=(0, None)))
            model.add(prefix + ('s_n_energy',), pyo.Var(model.T, bounds=(0, None)))

            def rule(m, t):
                return model.component_dict[prefix + ('energy',)][t] == configuration['storage_slack']['values'][self.name][t] + model.component_dict[prefix + ('s_p_energy',)][t] - model.component_dict[prefix + ('s_n_energy',)][t]
            model.add(prefix + ('s_energy',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[prefix + ('s_p_energy',)][t] * configuration['storage_slack']['strategy_factor']
            model.add_objective_term(pyo.Expression(model.T, rule=rule))

            def rule(m, t):
                return model.component_dict[prefix + ('s_n_energy',)][t] * configuration['storage_slack']['strategy_factor']
            model.add_objective_term(pyo.Expression(model.T, rule=rule))

    def _constraint_capacity(self, model, prefix, configuration):
        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] * self.input_efficiency <= model.component_dict[prefix + ('capacity',)] / self.e2p_in
        model.add(prefix + ('capacity_input_cons',), pyo.Constraint(model.T, rule = rule))
        
        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] / self.output_efficiency <= model.component_dict[prefix + ('capacity',)] / self.e2p_out
        model.add(prefix + ('capacity_output_cons',), pyo.Constraint(model.T, rule = rule))

        if 'storage_boundaries' in configuration and self.match(commodity=configuration['storage_boundaries']['commodity']):
            def rule(m, t):
                return configuration['storage_boundaries']['min'] * model.component_dict[prefix + ('capacity',)] <= model.component_dict[prefix + ('energy',)][t]
            model.add(prefix + ('energy_lb',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[prefix + ('energy',)][t] <= configuration['storage_boundaries']['max'] * model.component_dict[prefix + ('capacity',)]
            model.add(prefix + ('energy_ub',), pyo.Constraint(model.T, rule = rule))
        else:
            def rule(m, t):
                return self.min_soe * model.component_dict[prefix + ('capacity',)] <= model.component_dict[prefix + ('energy',)][t]
            model.add(prefix + ('energy_lb',), pyo.Constraint(model.T, rule = rule))

            def rule(m, t):
                return model.component_dict[prefix + ('energy',)][t] <= self.max_soe * model.component_dict[prefix + ('capacity',)]
            model.add(prefix + ('energy_ub',), pyo.Constraint(model.T, rule = rule))

    def _constraint_conser(self, model, prefix, configuration):
        if 'storage_connect' in configuration and self.match(commodity=configuration['storage_connect']['commodity']):
            last_energy = configuration['storage_connect']['values'][self.name]
            model.add(prefix + ('fix_first_energy',), pyo.Constraint(expr = model.component_dict[prefix + ('energy',)][model.T_prime.first()] == last_energy))
        else:
            model.add(prefix + ('fix_first_energy',), pyo.Constraint(expr = model.component_dict[prefix + ('energy',)][model.T_prime.first()] == self.init_soe * model.component_dict[prefix + ('capacity',)]))
        
        def rule(m, t):
            return model.component_dict[prefix + ('energy',)][t] == model.component_dict[prefix + ('energy',)][model.T_prime[model.T_prime.ord(t) - 1]] + model.component_dict[prefix + ('input_1',)][t] * self.input_efficiency * model.step_size(t) - model.component_dict[prefix + ('output_1',)][t] / self.output_efficiency * model.step_size(t)
        model.add(prefix + ('conser',), pyo.Constraint(model.T, rule = rule))

    def _constraint_bi_flow(self, model, prefix):
        model.add(prefix + ('z_input',), pyo.Var(model.T, domain=pyo.Binary))
        model.add(prefix + ('z_output',), pyo.Var(model.T, domain=pyo.Binary))

        def rule(m, t):
            return model.component_dict[prefix + ('z_input',)][t] + model.component_dict[prefix + ('z_output',)][t] <= 1
        model.add(prefix + ('bi_flow_1',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('input_1',)][t] <= model.component_dict[prefix + ('z_input',)][t] * 100000
        model.add(prefix + ('bi_flow_2',), pyo.Constraint(model.T, rule = rule))

        def rule(m, t):
            return model.component_dict[prefix + ('output_1',)][t] <= model.component_dict[prefix + ('z_output',)][t] * 100000
        model.add(prefix + ('bi_flow_3',), pyo.Constraint(model.T, rule = rule))
    
    def add_capital_costs(self, model, prosumer_configuration):
        prefix = (self.name,)
        
        model.add(prefix + ('capital_cost',), pyo.Var(bounds=(0, None)))
        
        capital_cost = calc_annuity_vdi2067.run(prosumer_configuration['planning_horizon'],
                                               self.life,
                                               self.cost,
                                               model.component_dict[prefix + ('capacity',)],
                                               self.f_inst,
                                               self.f_w,
                                               self.f_op,
                                               prosumer_configuration['yearly_interest'])
        model.add(prefix + ('capital_cost_cons',), pyo.Constraint(expr = model.component_dict[prefix + ('capital_cost',)] == capital_cost))
        return prefix + ('capital_cost',)

    # Flexibility stuff

    def calc_flex_comp(self, results, dynamic, init_results):
        """
        This method calculates the maximal possible flexibility from the battery.
        This is done by comparing two values:
            1) The maximal possible CHA/DCH power
            2) The maximal allowed CHA/DCH power regarding the available power BEFORE the PLANNED CHA/DCH
        The minimal value is chosen to be the maximal possible CHA/DCH power. To calaculate the available flexibility from this,
        the PLANNED CHA/DCH rates are added/substracted depending on the type of flexibility.
        E.g. positive flex: the current DCH power is substracted and the current CHA power is added
        Parameters
        ----------
        flows: flow variables in the prosumer
        results: result df to get capacity and energy values
        T: time steps of current RH step
        """

        cap_storage = results[(self.name, 'capacity')]  # the total theoretical capacity of the comm_batt
        e_storage = results[(self.name, 'energy')]

        # maximal in/out powers that the comm_batt is capable of
        max_power_out = cap_storage / self.e2p_out * self.output_efficiency
        max_power_in = cap_storage / self.e2p_in / self.input_efficiency

        # get the total output/input powers
        total_dch = pd.Series(data=0.0, index=dynamic.time_steps())
        total_dch += results[(self.name, 'output_1')] #  self.output_efficiency #here not necessary

        total_cha = pd.Series(data=0.0, index=dynamic.time_steps())
        total_cha += results[(self.name, 'input_1')] # self.input_efficiency

        # these are the power values that are theoretically available
        # for flexibility through CHA and DCH (missing curtailment)
        max_power_dch = max_power_out
        max_power_cha = max_power_in

        # now the energy restriction is calculated
        # ToDO @jbr: in the future here should be an extra restriction if some SOE values
        #  have to be reached in future timesteps

        #e_cha = (self.max_soe * cap_storage - e_storage_shift) / self.input_efficiency
        #e_dch = (e_storage_shift - self.min_soe * cap_storage) * self.output_efficiency
        buffer_cha = 0.0
        buffer_dch = 0.0
        e_cha = (0.8 * cap_storage - e_storage)  # / self.input_efficiency
        e_dch = (e_storage - 0.2 * cap_storage)  # * self.output_efficiency

        for t in dynamic.time_steps():
            if e_dch[t] < -1 * e_cha[t]:
                print('Fehler zum Zeitpunkt ' + str(t))

        # problem with these ones is: a negative energy means that the energy level is in "restricted" levels.
        # This may lead to infeasailities as this constraint requires the Prosumer to provide charge/discharge
        # even if this is not possible. But this should be correct

        e_cha.loc[e_cha < 0] = 0
        e_dch.loc[e_dch < 0] = 0

        # pos flex:
        self.temp_flex['flex_pos_inc'] = max_power_dch - total_dch
        self.temp_flex.loc[self.temp_flex['flex_pos_inc'] < 0, 'flex_pos_inc'] = 0
        self.temp_flex['flex_pos_dec'] = total_cha
        self.temp_flex['flex_pos'] = max_power_dch - total_dch + total_cha
        self.temp_flex.loc[self.temp_flex['flex_pos'] < 0, 'flex_pos'] = 0

        self.temp_flex['e_dch'] = e_dch

        # neg flex: negative signs due to negative flexibility
        self.temp_flex['flex_neg_inc'] = max_power_cha - total_cha
        self.temp_flex.loc[self.temp_flex['flex_neg_inc'] < 0, 'flex_neg_inc'] = 0
        self.temp_flex['flex_neg_dec'] = total_dch
        self.temp_flex['flex_neg'] = max_power_cha - total_cha + total_dch
        self.temp_flex.loc[self.temp_flex['flex_neg'] < 0, 'flex_neg'] = 0

        self.temp_flex['e_cha'] = e_cha

    def check_limits(self, input_flows, output_flows, results, dynamic):
        """
        calculate the consequences of maximal positive and negative flexibility. Positive flexibility can
        increase  (pos_flex_inc) and decrease (pos_flex_dec) the outputs.
        The negative flexibility increases/outputs the inputs of the Storage.
        Therefore, they have to be transformed to input values.
        Parameters
        ----------
        path_comps
        comp

        Returns
        -------

        """
        i_start = len(self.temp_flex['flex_pos']) - len(dynamic.time_steps())

        pos_flex = self.temp_flex['flex_pos'][dynamic.time_steps()]
        pos_flex_inc = self.temp_flex['flex_pos_inc'][dynamic.time_steps()]  # increasing of comp output
        pos_flex_dec = self.temp_flex['flex_pos_dec'] [dynamic.time_steps()] # curtailment of comp input

        neg_flex = self.temp_flex['flex_neg'][dynamic.time_steps()]
        neg_flex_inc = self.temp_flex['flex_neg_inc'][dynamic.time_steps()]  # increasing of comp input
        neg_flex_dec = self.temp_flex['flex_neg_dec'][dynamic.time_steps()]  # curtailment of comp output

        if not hasattr(self, 'limits'):
            self.limits = pd.DataFrame()

            self.limits['planned_input'] = input_flows
            self.limits['planned_output'] = output_flows
            self.limits['flex_input'] = input_flows
            self.limits['flex_output'] = output_flows


        # maximal in/out powers that the comm_batt is capable of
        cap_storage = results[(self.name, 'capacity')]
        power_limit_out = cap_storage / self.e2p_out
        power_limit_in = cap_storage / self.e2p_in

        # ----------------MAX FLEX RESULTS--------------------

        extra_output = (pos_flex_inc - pos_flex_dec) / self.output_efficiency
        extra_input = (neg_flex_inc - neg_flex_dec) * self.input_efficiency

        # get the maximum value of inputs and outputs. This is enough information because
        # the grid is just able to deliver increasing flexibility of only one type in each timestep
        max_output = output_flows + extra_output
        max_input = input_flows + extra_input
        self.limits['max_usage'] = max_input

        diff = max_input - power_limit_in
        diff[diff < 0] = 0

        return diff

    def calc_correction_factors(self, time_steps, c_f_df, results):
        """
        Calculates the correction factors (CF).
        A general component will only have an input and output efficiency.
        If not, no calculation will be done and the cf df will not be changed.

        Parameters
        ----------
        T: time steps of current RH interval
        c_f_df: correction factors of all components that have aleary been searched
        results: results of initial scheduling to get comp. size
        input_profiles: not necessary in this method.

        Returns
        -------
        c_f_df: updated CF dataframe
        """
        c_f_dch = pd.Series(data=1/self.output_efficiency, index=time_steps)
        c_static_dch = pd.Series(data=0, index=time_steps)
        c_f_cha = pd.Series(data=self.input_efficiency, index=time_steps)
        c_static_cha = pd.Series(data=0, index=time_steps)

        c_f_df['c_f_dch'] = c_f_dch * c_f_df['c_f_dch']
        c_f_df['c_static_dch'] = c_static_dch + c_f_df['c_static_dch']

        c_f_df['c_f_cha'] = c_f_cha * c_f_df['c_f_cha']
        c_f_df['c_static_cha'] = c_static_cha + c_f_df['c_static_cha']

        return c_f_df
