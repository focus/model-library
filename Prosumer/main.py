"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

# main.py is the central script to execute the flexible modelling of prosumers within this simulation tool.
# For further Information please have a closer look at the documentation in "README.txt"
# This work is published under the public license: XXX

import Model_Library.Prosumer.model.Prosumer as Prosumer
import Model_Library.Prosumer.model.DistrictAsset as DistrictAsset
import Model_Library.Prosumer.model.BusChargingStation as BusChargingStation
import Model_Library.Prosumer.model.BusChargingStationWithHPC as BusChargingStationWithHPC
import Model_Library.Prosumer.model.OverheadLineForIMC as OverheadLineForIMC


class ProsumerMain:
    def __init__(self, configurations, input_profiles, dynamic):
        self.prosumers = {}

        for name, configuration in configurations.items():
            # Create prosumer object
            if configuration['type'].lower() == 'buschargingstation':
                self.prosumers[name] = BusChargingStation(name, configuration, input_profiles, dynamic)
            elif configuration['type'].lower() == 'buschargingstationwithhpc':
                self.prosumers[name] = BusChargingStationWithHPC(name, configuration, input_profiles, dynamic)
            elif configuration['type'].lower() == 'overheadlineforimc':
                self.prosumers[name] = OverheadLineForIMC(name, configuration, input_profiles, dynamic)
            else:
                self.prosumers[name] = Prosumer(name, configuration, input_profiles, dynamic)

    def optimize_sizing(self, key, prosumer_sizing_strategy):
        # ToDo: callback, abbruchbedingung, maybe check the feasebility of the model before building,
        #  häufige fehler bei der eingabe prüfen usw
        for ps in self.prosumers.values():
            ps.optimize_sizing(key, prosumer_sizing_strategy)

    def pareto_analysis(self, prosumer_sizing_strategy_1, prosumer_sizing_strategy_2):
        for ps in self.prosumers.values():
            ps.pareto_analysis(prosumer_sizing_strategy_1, prosumer_sizing_strategy_2)

    def save_results(self):
        for ps in self.prosumers.values():
            ps.save_results()


class DistrictAssetMain:
    def __init__(self, configurations, input_profiles, dynamic):
        self.district_assets = {}

        for name, configuration in configurations.items():
            # Create district_asset object
            self.district_assets[name] = DistrictAsset(name, configuration, input_profiles, dynamic)


# class BusChargingStationMain:
#     def __init__(self, configurations, input_profiles, t_horizon, t_step):
#         self.bus_charging_stations = {}
#
#         for name, configuration in configurations.items():
#             # Create prosumer object
#             self.bus_charging_stations[name] = BusChargingStation(name, configuration, input_profiles, t_horizon, t_step)
#
#     def optimize_sizing(self, bus_charging_station_sizing_strategy):
#         for bcs in self.bus_charging_stations.values():
#             bcs.optimize_sizing(bus_charging_station_sizing_strategy)
#
#     def pareto_analysis(self, bus_charging_station_sizing_strategy_1, bus_charging_station_sizing_strategy_2):
#         for bcs in self.bus_charging_stations.values():
#             bcs.pareto_analysis(bus_charging_station_sizing_strategy_1, bus_charging_station_sizing_strategy_2)
#
#     def save_results(self):
#         for bcs in self.bus_charging_stations.values():
#             bcs.save_results()

