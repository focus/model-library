"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from multiprocessing import Pool
import os
import matplotlib.style as style


def Plot_savings(reference_scenario, topology):#='C:/Data/git/intraday/output_files/results_SCN0_CAT1'):

        #reference_scenario: path to the reference scenario
        #topology: name of the considered topology

        # calculate savings and exract demand
        all_folders = os.listdir('output_files')
        scenario_savings_percent = []
        scenario_savings_euro = []
        scenario_elec_demand = []
        scenario_therm_demand = []

        try:
            try:
                for folder_name in all_folders:
                    all_files = os.listdir('output_files/'+folder_name+'/')
                    for file_name in all_files:
                        if topology in file_name and file_name.endswith('.xlsx'):
                            scenario_results = pd.read_excel('output_files/'+folder_name+'/'+file_name)
                            scenario_costs = scenario_results['total_annual_costs'][0]
                            scenario_elec_demand.append(scenario_results['elec_demand_yearly'][0])
                            scenario_therm_demand.append(scenario_results['therm_demand_yearly'][0])
                            reference_results = pd.read_excel('output_files/'+ reference_scenario + '_' +
                                                          str(scenario_results['elec_demand_yearly'][0]) + '_' +
                                                          str(scenario_results['therm_demand_yearly'][0]) + '/'
                                                          + 'results_' + reference_scenario + '_' +
                                                          str(scenario_results['elec_demand_yearly'][0]) + '_' +
                                                          str(scenario_results['therm_demand_yearly'][0]) + '.xlsx')
                            reference_costs = reference_results['total_annual_costs'][0]
                            scenario_savings_percent.append((reference_costs - scenario_costs) / reference_costs * 100)
                            scenario_savings_euro.append(reference_costs - scenario_costs)

                # heatmap plot
                if scenario_savings_percent:
                    file_dir = os.path.dirname(os.path.abspath(__file__))
                    for j in range(2):
                        file_dir = os.path.dirname(file_dir)

                    # Create result path
                    if not os.path.exists('output_files/plots/savings_percent'):
                        os.makedirs('output_files/plots/savings_percent')
                    if not os.path.exists('output_files/plots/savings_euros'):
                        os.makedirs('output_files/plots/savings_euros')

                    # Collect data and plot heat maps
                    # Savings in percent
                    data_percent = pd.DataFrame(
                        data={'Electrical demand': scenario_elec_demand, 'Thermal demand': scenario_therm_demand,
                              'Savings percent': scenario_savings_percent})
                    data_percent = data_percent.pivot(index='Electrical demand', columns='Thermal demand', values='Savings percent')
                    sns_plt_percent = sns.heatmap(data_percent)
                    fig_percent = sns_plt_percent.get_figure()
                    plt.legend(title='Cost savings in % of ' + topology)  # , labels=['test1', 'test2'])
                    # Save plot
                    fig_percent.savefig(os.path.join(file_dir, 'output_files/plots/savings_percent/', 'savings_heatmap_' + topology + '.jpeg'), dpi=500)
                    plt.close()

                    # Collect data and plot heat maps
                    # Savings in euro
                    data_euro = pd.DataFrame(
                        data={'Electrical demand': scenario_elec_demand, 'Thermal demand': scenario_therm_demand,
                              'Savings euro': scenario_savings_euro})
                    data_euro = data_euro.pivot(index='Electrical demand', columns='Thermal demand', values='Savings euro')
                    sns_plt_euro = sns.heatmap(data_euro)
                    fig_euro = sns_plt_euro.get_figure()
                    plt.legend(title='Cost savings in Euro of ' + topology)  # , labels=['test1', 'test2'])
                    # Save plot
                    fig_euro.savefig(os.path.join(file_dir, 'output_files/plots/savings_euros/', 'savings_heatmap_' + topology + '.jpeg'), dpi=500)
                    plt.close()
            except ValueError:
                print('Numerical error! Please check results.')
        except OSError as e:
            print('No reference results found: Please check spelling!')




'''prosumer_name = ['SCN0_CAT1',
                 'SCN1_CAT1',
                 'SCN2_CAT1_PV1',
                 'SCN2_CAT1_PV2_BA',
                 'SCN2_CAT1_PV3_BA_HP']

for i in prosumer_name:
    Plot_savings(i)'''