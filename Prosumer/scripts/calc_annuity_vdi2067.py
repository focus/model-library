"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import numpy as np
import pandas as pd


def annuity_factor(t, q=1.007):
    """
    The method calculates the annuity factor, which shows repeated payments of
    equal amount; usually the annual instalments required to pay off the
    principal and interest on a debt.
    :param t: number of years of the observation period
    :param q: interest factor (taken from Bundesministerium der Finanzen)
    :return: annuity factor
    """
    # q:
    if q == 1.0:
        a = 1 / t
    else:
        try:
            a = (q - 1) / (1 - pow(q, -t))
        except ZeroDivisionError:
            raise ValueError('Cannot calculate annuity')
    return a


def dynamic_cash_value(t, q=1.007, r=1.03):
    """
    The method calculates the price-dynamic cash value factor, which considers
    the price changes for the ongoing costs during the observation period.
    :param t: number of years of the observation period
    :param q: interest factor (taken from Bundesministerium der Finanzen)
    :param r: price change factor
    :return: cash value factor
    """
    if r == q:
        b = t / q
    else:
        b = (1 - pow(r / q, t)) / (q - r)
    return b


def calc_capital_cost(t, t_n, q, ann, a_0):
    """
    The method calculates the annuity of capital-related costs.
    :param t: observation period, in year
    :param t_n: service life of the equipment, in year
    :param q: interest factor
    :param ann: annuity factor
    :param a_0: investment amount in first year
    :return: annuity of capital-related costs
    """

    # r: price change factor, taken from Statistisches Bundesamt: median of
    # yearly price index change for central heating devices (GP 25 21)
    r = 1.02
    # n: number of replacements, np.ceil gives a float64 -> do int(n) for range
    n = np.ceil(t / t_n) - 1
    # r_w: residual value
    r_w = a_0 * pow(r, n * t_n) * ((n + 1) * t_n - t) / t_n * 1 / pow(q, t)

    a_inv = []
    for i in range(int(n) + 1):
        a_i = a_0 * ((pow(r, i * t_n)) / (pow(q, i * t_n)))
        a_inv.append(a_i)

    a_n_k = (sum(a_inv) - r_w) * ann
    return a_n_k


def calc_operation_cost(t, q, ann, a_0, f_inst, f_w, f_op):
    """
    The method calculates the annuity of operation-related costs.
    :param t: observation period, in year
    :param q: interest factor
    :param ann: annuity factor
    :param a_0: investment amount in first year
    :param f_inst: float, factor for repair effort (from VDI2067)
    :param f_w: float, factor for servicing and inspection effort (from VDI2067)
    :param f_op: int, effort for operation in h/a (from VDI2067)
    :return: annuity of operation-related costs

    Other used parameters in method, the values for r and price_op are taken
    from Statistisches Bundesamt.
    r_b: price change factor for actual operation, for labour cost (vdi 2067)
    r_in: price change factor for maintenance (vdi 2067)
    b_b: cash value factor for actual operation
    b_in: cash value factor for maintenance
    price_op: labour costs per hour worked (2019)
    """
    r_b = 1.02
    r_in = 1.03
    b_b = dynamic_cash_value(t, q, r_b)
    b_in = dynamic_cash_value(t, q, r_in)
    price_op = 55.6

    # disable a_b1 first for this cost doesn't exist when cap is 0!
    # The value of this is little for large system and too much for small
    # system!
    # a_b1 = f_op * price_op
    a_b1 = 0
    a_in = a_0 * (f_inst + f_w)

    a_n_b = a_b1 * ann * b_b + a_in * ann * b_in
    return a_n_b


def run(t, t_n, invest, cap, f_inst, f_w, f_op, i):
    """
    The method calculates the annuity of technical building installations
    according to VDI 2067.
    :param t: the observation period
    :param t_n: service life of the equipment, in year
    :param invest: float or pyomo variable, investment cost in [EUR/kW] or
                   [EUR/kWh], fixed value for each component or calculated
                   variable
    :param cap: power or capacity of the component in [kW] or [kWh]
    :param f_inst: float, factor for repair effort (from VDI2067)
    :param f_w: float, factor for servicing and inspection effort (from VDI2067)
    :param f_op: int, effort for operation in h/a (from VDI2067)
    :param i: float, interest rate. The value of i should be less than 1 and
              the value of q should be greater than 1
    :return: annuity (one year) for the technical building installation
    """
    q = i+1

    ann = annuity_factor(t, q)
    a_0 = cap * invest

    # The revenue and demand related cost are set in the energy management
    # system class. Because the cost are related to the model variables.
    # WARN: in the energy management system class the cash value factor is
    # not considered, because the observation is only 1 year. If the
    # observation period longer than 1 year, should use the cash value factor
    # for demand related cost and revenue as well
    # a_n_e: annuity of revenue, like feed in electricity
    # a_n_v: annuity of demand related cost, like purchased gas or electricity
    # a_n_s: annuity of other cost
    a_n_e = 0
    a_n_v = 0
    a_n_s = 0

    a_n = (calc_capital_cost(t, t_n, q, ann, a_0) + a_n_v
           + calc_operation_cost(t, q, ann, a_0, f_inst, f_w, f_op) +
           a_n_s) - a_n_e

    return a_n


if __name__ == "__main__":
    # validation for optimisation results
    t = 1
    t_n = 20
    invest = 200
    cap = 1.17
    f_inst = 0.01
    f_w = 0.015
    f_op = 20
    i = 0.02
    a_n = run(t, t_n, invest, cap, f_inst, f_w, f_op, i)
    print(a_n)
