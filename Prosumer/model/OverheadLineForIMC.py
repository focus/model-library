"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import Model_Library.Prosumer.model.Prosumer as Prosumer
import pyomo.environ as pyo

class OverheadLineForIMC(Prosumer):
    def __init__(self, name, configuration, profiles, dynamic):
        super().__init__(name=name,
                         configuration=configuration,
                         profiles=profiles,
                         dynamic=dynamic)

        self.buses = {component_name: component for component_name, component in configuration['components'].items() if component['type'] == 'ElecBus'}
        self.driving_consumptions = {component_name: component for component_name, component in configuration['components'].items() if component['type'] == 'DrivingConsumption'}

        for bus, driving_cons in zip(self.buses.keys(), self.driving_consumptions.keys()):
            profile_dict = configuration['components'][driving_cons]
            self.buses[bus]['z_connected_to_OHL'] = profiles[profile_dict['consumption']][0].loc[:, 'Connected to OHL']


    def add_prosumer_variables(self, model, prefix = ()):
        super().add_prosumer_variables(model, prefix)
        for bus in self.get_buses():
            # switched from 'within binary' to bounds due to interpolation of input profiles
            model.add((bus.name, 'z_connected_to_OHL_check'), pyo.Var(model.T, bounds=(0, 1)))

            model.add((bus.name, 'z_connected_to_OHL'), pyo.Param(model.T, mutable=True))
            model.set_value((bus.name, 'z_connected_to_OHL'), self.buses[bus.name]['z_connected_to_OHL'])


    def add_prosumer_constraints(self, model, prefix = ()):
        super().add_prosumer_constraints(model, prefix)
        self.add_inverter_adjusted_to_peak_power_demand_constraint(model, prefix)
        self.add_equal_battery_sizing_constraints(model, prefix)
        self.add_equal_converter_sizing_constraints(model, prefix)
        #self.add_equal_inverter_sizing_constraints(model, prefix) # not required if all inverters have same efficiency, if not disable peak power demand constraint and enable this one
        self.add_OHL_connection_constraints(model, prefix)


    # size of DC-AC Inverter should be fitted to peak power demand of mobility consumption
    def add_inverter_adjusted_to_peak_power_demand_constraint(self, model, prefix):
        for inverter, driving_consumption in zip(self.get_inverters(), self.get_driving_consumptions()):
            capacity = self.get_max_consumption()/inverter.efficiency
            model.add((inverter.name, 'size_cons'), pyo.Constraint(expr=model.component_dict[(inverter.name, 'capacity')] == capacity))


    # all bus batteries should have the same capacity
    def add_equal_battery_sizing_constraints(self, model, prefix):
        buses = self.get_buses()
        for i in range(len(buses)-1):
            def rule(m,t):
                return model.component_dict[(buses[i].name,'capacity')] == model.component_dict[(buses[i+1].name,'capacity')]
            model.add(('equal_bat_capacities_constraint_' + str(i+1),),pyo.Constraint(model.T, rule=rule))


    # all DC-DC converters in buses (not wayside) should have the same capacity
    def add_equal_converter_sizing_constraints(self, model, prefix):
        converters = self.get_converters_of_buses()
        for i in range(len(converters)-1):
            def rule(m,t):
                return model.component_dict[(converters[i].name, 'capacity')] == model.component_dict[(converters[i+1].name,'capacity')]
            model.add(('equal_converter_capacities_constraint_' + str(i+1),),pyo.Constraint(model.T, rule=rule))


    # all DC-AC inverters should have the same capacity
    def add_equal_inverter_sizing_constraints(self, model, prefix):
        inverters = self.get_inverters()
        for i in range(len(inverters)-1):
            def rule(m,t):
                return model.component_dict[(inverters[i].name,'capacity')] == model.component_dict[(inverters[i+1].name,'capacity')]
            model.add(('equal_inverter_capacities_constraint_' + str(i+1),),pyo.Constraint(model.T, rule=rule))

    # set power flow from OHL to bus to zero when bus disconnected
    def add_OHL_connection_constraints(self,model,prefix):
        for bus, OHL_to_bus, bus_to_OHL in zip(self.get_buses(), self.get_OHL_to_bus_flows(), self.get_bus_to_OHL_flows()):
            t_not_connected = set()
            for t in model.T:
                if model.component_dict[(bus.name, 'z_connected_to_OHL')][t] == 0:
                    t_not_connected.add(t)

            def rule(m,t):
                return model.component_dict[(bus.name, 'z_connected_to_OHL_check')][t] == model.component_dict[bus.name, 'z_connected_to_OHL'][t]
            model.add((bus.name + 'z_connected_to_OHL_check_constraint'), pyo.Constraint(model.T, rule=rule))

            def rule(m, t):
                return model.component_dict[prefix + (OHL_to_bus,)][t] == 0
            model.add((OHL_to_bus + 'z_connected_constraint',), pyo.Constraint(t_not_connected, rule=rule))

            def rule(m, t):
                return model.component_dict[prefix + (bus_to_OHL,)][t] == 0
            model.add((bus_to_OHL + 'z_connected_constraint',), pyo.Constraint(t_not_connected, rule=rule))


    def get_grid(self):
        for component in self._components.values():
            if component.type == 'ElectricalGrid':
                return component

    def get_buses(self):
        buses = []
        for component in self._components.values():
            if component.type == 'ElecBus':
                buses.append(component)
        return buses

    def get_converters(self):
        converter = []
        for component in self._components.values():
            if component.type == 'DcDcConverter':
                converter.append(component)
        return converter

    def get_converters_of_buses(self):
        converter_names = []
        buses = self.get_buses()
        for connector in self._connectors:
            for bus in buses:
                if connector.type.value == 'output_2' and bus.name in connector.other_sides:
                    converter_names.append(connector.name)
        converters = [converter for converter in self.get_converters() if converter.name in converter_names]
        return converters


    def get_inverters(self):
        inverter = []
        for component in self._components.values():
            if component.type == 'StaticInverter':
                inverter.append(component)
        return inverter

    def get_driving_consumptions(self):
        consumptions = []
        for component in self._components.values():
            if component.type == 'DrivingConsumption':
                consumptions.append(component)
        return consumptions

    def get_bus_bars(self):
        bus_bars = []
        for component in self._components.values():
            if component.type == 'ElectricalBusBar':
                bus_bars.append(component)
        return bus_bars

    def get_OHL(self):
        bus_bar_names = [component.name for component in self.get_bus_bars()]
        for connector in self._connectors:
            if connector.name in bus_bar_names and self.get_grid().name in connector.other_sides:
                for component in self._components.values():
                    if connector.name == component.name:
                        return component

    def get_bus_bars_of_buses(self):
        bus_bars_of_buses = [bus_bar for bus_bar in self.get_bus_bars() if bus_bar.name != self.get_OHL().name]
        return bus_bars_of_buses

    def get_OHL_to_bus_flows(self):
        flows = []
        for flow in self._flows:
            for bus_bar in self.get_bus_bars_of_buses():
                if flow.startswith(self.get_OHL().name) and bus_bar.name in flow:
                    flows.append(flow)
        return flows

    def get_bus_to_OHL_flows(self):
        flows = []
        for flow in self._flows:
            for bus_bar in self.get_bus_bars_of_buses():
                if flow.startswith(bus_bar.name) and self.get_OHL().name in flow:
                    flows.append(flow)
        return flows

    def get_max_consumption(self):
        max_cons = []
        for driving_consumption in self.get_driving_consumptions():
            max_cons.append(max(driving_consumption.consumption))
        return max(max_cons)
