"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import Model_Library.Prosumer.model.Prosumer as Prosumer
from Model_Library.OptimizationModel import OptimizationBlock
import pyomo.environ as pyo

class DistrictAsset(Prosumer):
    def __init__(self, name, configuration, profiles, dynamic):
        super().__init__(name, configuration, profiles, dynamic)

    def build_sizing_model(self, model):
        block = OptimizationBlock(self._name, self._dynamic)
        model.add((self._name,), block)

        for component in self._components:
            self._components[component].build_model(block, {})

        self.add_flow_variables(block)

        self.add_connection_constraints(block)

    def get_export_import_expressions(self, block):
        export_vars = []
        import_vars = []
        for component in self._components:
            if self._components[component].type == 'ElectricalGrid':
                export_vars.append((component, 'input_1'))
                import_vars.append((component, 'output_1'))
        def export_rule(m, t):
            return pyo.quicksum(block.component_dict[export_var][t] for export_var in export_vars)
        block.add(('export',), pyo.Expression(block.T, rule=export_rule))
        def import_rule(m, t):
            return pyo.quicksum(block.component_dict[import_var][t] for import_var in import_vars)
        block.add(('import',), pyo.Expression(block.T, rule=import_rule))
        return block.component_dict[('export',)], block.component_dict[('import',)]
