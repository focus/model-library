"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import Model_Library.Prosumer.model.Prosumer as Prosumer
import pyomo.environ as pyo


class BusChargingStation(Prosumer):
    def __init__(self, name, configuration, profiles, dynamic):
        super().__init__(name=name,
                         configuration=configuration,
                         profiles=profiles,
                         dynamic=dynamic)

        self.buses = {component_name: component for component_name, component in configuration['components'].items() if component['type'] == 'ElecBus'}
        self.driving_consumptions = {component_name: component for component_name, component in configuration['components'].items() if component['type'] == 'DrivingConsumption'}

        for bus, driving_cons in zip(self.buses.keys(), self.driving_consumptions.keys()):
            profile_dict = configuration['components'][driving_cons]
            self.buses[bus]['z_driving'] = profiles[profile_dict['consumption']][0].loc[:, 'Driving']
            self.buses[bus]['z_connected_to_depot'] = profiles[profile_dict['consumption']][0].loc[:, 'Connected to depot']


    def add_prosumer_variables(self, model, prefix = ()):
        super().add_prosumer_variables(model, prefix)
        #self.add_binary_flow_variables(model, prefix) # activate if m x n architecture (each bus can connect to every charging point)

        for bus in self.get_buses():
            model.add((bus.name, 'current_charge_power'), pyo.Var(model.T, bounds=(0,None)))

            #model.add((bus.name, 'z_driving'), pyo.Var(model.T, within=pyo.Binary))
            model.add((bus.name, 'z_driving'), pyo.Param(model.T, mutable=True))
            model.set_value((bus.name, 'z_driving'), self.buses[bus.name]['z_driving'])

            model.add((bus.name, 'z_connected_to_depot_check'), pyo.Var(model.T, within=pyo.Binary))
            model.add((bus.name, 'z_connected_to_depot'), pyo.Param(model.T, mutable=True))
            model.set_value((bus.name, 'z_connected_to_depot'), self.buses[bus.name]['z_connected_to_depot'])

    # activate if m x n architecture (each bus can connect to every charging point)
    def add_binary_flow_variables(self, model, prefix = ()):
        for flow in self.get_depot_to_bus_flows():
            model.add(prefix + ('z_' + flow,), pyo.Var(model.T, within=pyo.Binary))

        for flow in self.get_bus_to_depot_flows():
            model.add(prefix + ('z_' + flow,), pyo.Var(model.T, within=pyo.Binary))

    def add_prosumer_constraints(self, model, prefix = ()):
        super().add_prosumer_constraints(model, prefix)
        self.add_equal_battery_sizing_constraints(model, prefix)
        self.add_equal_depot_inverter_sizing_constraints(model, prefix)
        self.add_depot_connection_constraints(model, prefix)
        #self.add_charge_power_constraints(model, prefix) # activate if buses can switch charging points
        #self.add_simultaneous_connect_constraints(model, prefix) # activate if buses can switch charging points

    # all bus batteries should have the same capacity
    def add_equal_battery_sizing_constraints(self, model, prefix):
        buses = self.get_buses()
        for i in range(len(buses) - 1):
            def rule(m, t):
                return model.component_dict[(buses[i].name, 'capacity')] == model.component_dict[(buses[i + 1].name, 'capacity')]
            model.add(('equal_bat_capacities_constraint_' + str(i + 1),), pyo.Constraint(model.T, rule=rule))

    # all depot chargers (AC-DC inverters) should have the same capacity
    def add_equal_depot_inverter_sizing_constraints(self, model, prefix):
        inverters = self.get_depot_chargers()
        for i in range(len(inverters) - 1):
            def rule(m, t):
                return model.component_dict[(inverters[i].name, 'capacity')] == model.component_dict[(inverters[i + 1].name, 'capacity')]
            model.add(('equal_inverter_capacities_constraint_' + str(i + 1),), pyo.Constraint(model.T, rule=rule))

    # switch off power flow to bus if not connected to depot charger
    def add_depot_connection_constraints(self, model, prefix):
        for bus, depot_charger in zip(self.get_buses(), self.get_depot_chargers()):

            # just to make status visible in results file to verify results
            def rule(m,t):
                return model.component_dict[(bus.name, 'z_connected_to_depot_check')][t] == model.component_dict[(bus.name, 'z_connected_to_depot')][t]
            model.add((bus.name + '_z_connected_to_depot_constraint',), pyo.Constraint(model.T, rule=rule))

            t_not_connected = set()
            for t in model.T:
                if model.component_dict[(bus.name, 'z_connected_to_depot')][t] == 0:
                    t_not_connected.add(t)

            # no flows for 1 to 1 architecture, must work with component outputs
            def rule(m, t):
                return model.component_dict[(depot_charger.name, 'output_1')][t] == 0
            model.add((depot_charger.name + 'disable_output_to_' + bus.name + '_constraint',), pyo.Constraint(t_not_connected, rule=rule))

            # for m x n architecture
            # for depot_to_bus in self.get_depot_to_bus_flows():
            #     if bus.name in depot_to_bus:
            #         def rule(m,t):
            #             return model.component_dict[prefix + (depot_to_bus,)][t] == 0
            #         model.add((depot_to_bus + 'disable_power_flow_to_' + bus.name + '_constraint',), pyo.Constraint(t_not_connected, rule=rule))
            # for bus_to_depot in self.get_bus_to_depot_flows():
            #     if bus.name in bus_to_depot:
            #         def rule(m,t):
            #             return model.component_dict[prefix + (bus_to_depot,)][t] == 0
            #         model.add((bus_to_depot + 'disable_power_flow_from_' + bus.name + '_constraint',), pyo.Constraint(t_not_connected, rule=rule))

    # in case of no fixed connection between bus and charging station, bus can switch charging station
    # if bus is not driving and SOC is below 30%, it should be charged with max power of the respective inverter it is connected to
    def add_charge_power_constraints(self, model, prefix):
        for bus, driving_consumption in zip(self.get_buses(),self.get_driving_consumptions()):
            if hasattr(driving_consumption, 'z_driving'):
                def rule(m,t):
                    return model.component_dict[(bus.name,'z_driving')][t] == model.component_dict[(driving_consumption.name, 'z_driving')][t]
                model.add((bus.name + 'z_driving_constraint',), pyo.Constraint(model.T, rule=rule))

                t_not_driving = set()
                for t in model.T:
                    if driving_consumption.z_driving[t] == 0:
                        t_not_driving.add(t)

                # auxiliary variable and constraint
                # sets current charge power for bus to max power of the inverter it is connected to at that particular moment
                inv_to_bus_flows = []
                for inv_to_bus_flow in self.get_depot_to_bus_flows():
                    if bus.name in inv_to_bus_flow:
                        for inverter in self.get_inverters():
                            if inverter.name in inv_to_bus_flow:
                                inv_to_bus_flows.append(('z_' + inv_to_bus_flow, inverter))

                def rule(m,t):
                    return model.component_dict[(bus.name,'current_charge_power')][t] == pyo.quicksum(model.component_dict[(flow,)][t] * model.component_dict[(inverter.name,'capacity')] for (flow,inverter) in inv_to_bus_flows)
                model.add((bus.name + '_current_charge_power_constraint',), pyo.Constraint(t_not_driving, rule=rule))

                # if bus is not driving and SOC is below 30%, it should be charged with max power of the respective inverter it is connected to
                def rule(m, t):
                    return model.component_dict[(bus.name, 'input_1')][t] * bus.input_efficiency >= 0.99 * model.component_dict[(bus.name,'current_charge_power',)][t] * (model.component_dict[(bus.name, 'z_SOC_below_30')][t]) / bus.e2p_in
                model.add(prefix + (bus.name + '_not_driving_input_cons',), pyo.Constraint(t_not_driving, rule=rule))

                # in case of multiple inverters with different capacities
                # bus should at least be charged with the max power of the inverter with the lowest capacity
                # does not work if inverter size is to be optimized
                # def rule(m,t):
                #     return model.component_dict[(bus.name, 'input_1')][t] * bus.input_efficiency >= 0.99 * min_charging_power * (model.component_dict[(bus.name, 'z_SOC_below_30')][t]) / bus.e2p_in
                # model.add(prefix + (bus.name + 'not_driving_min_input_cons',), pyo.Constraint(t_not_driving, rule=rule))

    # constraints for multiple buses and multiple charging stations
    # no fixed connection between bus and charging station, bus can switch charging station
    # only allows power flows between one bus and one inverter
    def add_simultaneous_connect_constraints(self, model, prefix):
        inverters = self.get_inverter_names()
        buses = self.get_bus_names()

        for connector in self._connectors:

            # an inverter can only charge one bus at a time
            if connector.name in inverters and connector.type.value == 'output_1':
                def rule(m, t):
                    return pyo.quicksum(model.component_dict[prefix + ('z_' + flow[0],)][t] for [flow, other_side] in zip(connector.flows, connector.other_sides) if other_side in buses) <= 1
                model.add(prefix + (connector.name + '_to_buses_simul_flow_constraint',), pyo.Constraint(model.T, rule=rule))

                flows = [flow for [flow, other_side] in zip(connector.flows, connector.other_sides) if other_side in buses]
                for flow in flows:
                    def rule(m, t):
                        return model.component_dict[prefix + (flow[0],)][t] <= model.component_dict[prefix + ('z_' + flow[0],)][t] * 100000
                    model.add(prefix + (connector.name + '_output_1: ' + flow[0] + '_flow_enable_constraint',), pyo.Constraint(model.T, rule=rule))

            # an inverter can only receive power from one bus at a time
            elif connector.name in inverters and connector.type.value == 'input_1':
                def rule(m, t):
                    return pyo.quicksum(model.component_dict[prefix + ('z_' + flow[0],)][t] for [flow, other_side] in zip(connector.flows, connector.other_sides) if other_side in buses) <= 1
                model.add(prefix + ('from_buses_to_' + connector.name + '_simul_flow_constraint',),pyo.Constraint(model.T, rule=rule))

                flows = [flow for [flow, other_side] in zip(connector.flows, connector.other_sides) if other_side in buses]
                for flow in flows:
                    def rule(m, t):
                        return model.component_dict[prefix + (flow[0],)][t] <= model.component_dict[prefix + ('z_' + flow[0],)][t] * 100000
                    model.add(prefix + (connector.name + '_input_1: ' + flow[0] + '_flow_enable_constraint',), pyo.Constraint(model.T, rule=rule))

            # a bus can only be charged by one inverter at a time
            elif connector.name in buses and connector.type.value == 'input_1':
                def rule(m, t):
                    return pyo.quicksum(model.component_dict[prefix + ('z_' + flow[0],)][t] for [flow, other_side] in zip(connector.flows, connector.other_sides) if other_side in inverters) <= 1
                model.add(prefix + ('from_inverters_to_' + connector.name + '_simul_flow_constraint',),pyo.Constraint(model.T, rule=rule))

                flows = [flow for [flow, other_side] in zip(connector.flows, connector.other_sides) if other_side in inverters]
                for flow in flows:
                    def rule(m, t):
                        return model.component_dict[prefix + (flow[0],)][t] <= model.component_dict[prefix + ('z_' + flow[0],)][t] * 100000
                    model.add(prefix + (connector.name + '_input_1: ' + flow[0] + '_flow_enable_constraint',), pyo.Constraint(model.T, rule=rule))

            # a bus can only give back power to one inverter at a time
            elif connector.name in buses and connector.type.value == 'output_1':
                def rule(m, t):
                    return pyo.quicksum(model.component_dict[prefix + ('z_' + flow[0],)][t] for [flow, other_side] in zip(connector.flows, connector.other_sides) if other_side in inverters) <= 1
                model.add(prefix + (connector.name + '_to_inverters_simul_flow_constraint',),pyo.Constraint(model.T, rule=rule))

                flows = [flow for [flow, other_side] in zip(connector.flows, connector.other_sides) if other_side in inverters]
                for flow in flows:
                    def rule(m, t):
                        return model.component_dict[prefix + (flow[0],)][t] <= model.component_dict[prefix + ('z_' + flow[0],)][t] * 100000
                    model.add(prefix + (connector.name + '_output_1: ' + flow[0] + '_flow_enable_constraint',), pyo.Constraint(model.T, rule=rule))


    def get_inverter_names(self):
        inverters = []
        for name, component in self._components.items():
            if component.type == 'StaticBiInverter':
                inverters.append(name)
        return inverters

    def get_bus_names(self):
        buses = []
        for name, component in self._components.items():
            if component.type == 'ElecBus':
                buses.append(name)
        return buses

    def get_grid(self):
        for component in self._components.values():
            if component.type == 'ElectricalGrid':
                return component

    def get_buses(self):
        buses = []
        for component in self._components.values():
            if component.type == 'ElecBus':
                buses.append(component)
        return buses

    def get_inverters(self):
        inverter = []
        for component in self._components.values():
            if component.type == 'StaticBiInverter':
                inverter.append(component)
        return inverter

    def get_depot_chargers(self):
        depot_chargers = []
        for inverter in self.get_inverters():
            for connector in self._connectors:
                if connector.name == inverter.name and connector.type.value == 'output_2':
                    if self.get_grid().name not in connector.other_sides:
                        depot_chargers.append(inverter)
                        break
        return depot_chargers

    def get_driving_consumptions(self):
        consumptions = []
        for component in self._components.values():
            if component.type == 'DrivingConsumption':
                consumptions.append(component)
        return consumptions

    def get_depot_to_bus_flows(self):
        flows = []
        for flow in self._flows:
            flow_found = False
            for inverter in self.get_depot_chargers():
                if flow_found:
                    break
                for bus in self.get_buses():
                    if flow.startswith(inverter.name) and bus.name in flow:
                        flows.append(flow)
                        flow_found = True
                        break
        return flows

    def get_bus_to_depot_flows(self):
        flows = []
        for flow in self._flows:
            flow_found = False
            for bus in self.get_buses():
                if flow_found:
                    break
                for inverter in self.get_depot_chargers():
                    if flow.startswith(bus.name) and inverter.name in flow:
                        flows.append(flow)
                        flow_found = True
                        break
        return flows
