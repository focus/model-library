"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import Model_Library.Prosumer.model.BusChargingStation as BusChargingStation
import pyomo.environ as pyo


class BusChargingStationWithHPC(BusChargingStation):
    def __init__(self, name, configuration, profiles, dynamic):
        super().__init__(name=name,
                         configuration=configuration,
                         profiles=profiles,
                         dynamic=dynamic)

        for bus, driving_cons in zip(self.buses.keys(), self.driving_consumptions.keys()):
            profile_dict = configuration['components'][driving_cons]
            self.buses[bus]['z_connected_to_HPC_1'] = profiles[profile_dict['consumption']][0].loc[:, 'Connected to HPC 1']
            self.buses[bus]['z_connected_to_HPC_2'] = profiles[profile_dict['consumption']][0].loc[:, 'Connected to HPC 2']
            self.buses[bus]['z_connected_to_HPC_3'] = profiles[profile_dict['consumption']][0].loc[:, 'Connected to HPC 3']
        print('test')


    def add_prosumer_variables(self, model, prefix = ()):
        super().add_prosumer_variables(model, prefix)
        for bus in self.get_buses():
            model.add((bus.name, 'z_connected_to_HPC_1_check'), pyo.Var(model.T, bounds=(0,1)))
            model.add((bus.name, 'z_connected_to_HPC_2_check'), pyo.Var(model.T, bounds=(0,1)))
            model.add((bus.name, 'z_connected_to_HPC_3_check'), pyo.Var(model.T, bounds=(0,1)))

            model.add((bus.name, 'z_connected_to_HPC_1'), pyo.Param(model.T, mutable=True))
            model.set_value((bus.name, 'z_connected_to_HPC_1'), self.buses[bus.name]['z_connected_to_HPC_1'])

            model.add((bus.name, 'z_connected_to_HPC_2'), pyo.Param(model.T, mutable=True))
            model.set_value((bus.name, 'z_connected_to_HPC_2'), self.buses[bus.name]['z_connected_to_HPC_2'])

            model.add((bus.name, 'z_connected_to_HPC_3'), pyo.Param(model.T, mutable=True))
            model.set_value((bus.name, 'z_connected_to_HPC_3'), self.buses[bus.name]['z_connected_to_HPC_3'])

    def add_prosumer_constraints(self, model, prefix = ()):
        super().add_prosumer_constraints(model, prefix)
        self.add_HPC_connection_constraints(model, prefix)

    # switch off power flow to bus if not connected to high power charger
    def add_HPC_connection_constraints(self, model, prefix):
        for bus in self.get_buses():
            connected_to_HPC_1 = self.buses[bus.name]['z_connected_to_HPC_1']
            connected_to_HPC_2 = self.buses[bus.name]['z_connected_to_HPC_2']
            connected_to_HPC_3 = self.buses[bus.name]['z_connected_to_HPC_3']
            connected_profiles = [connected_to_HPC_1, connected_to_HPC_2, connected_to_HPC_3]

            # following three constraints just to make connection status visible
            def rule(m, t):
                return model.component_dict[(bus.name, 'z_connected_to_HPC_1_check')][t] == model.component_dict[(bus.name, 'z_connected_to_HPC_1')][t]
            model.add((bus.name + '_z_connected_to_HPC_1_constraint',), pyo.Constraint(model.T, rule=rule))

            def rule(m, t):
                return model.component_dict[(bus.name, 'z_connected_to_HPC_2_check')][t] == model.component_dict[(bus.name, 'z_connected_to_HPC_2')][t]
            model.add((bus.name + '_z_connected_to_HPC_2_constraint',), pyo.Constraint(model.T, rule=rule))


            def rule(m, t):
                return model.component_dict[(bus.name, 'z_connected_to_HPC_3_check')][t] == model.component_dict[(bus.name, 'z_connected_to_HPC_3')][t]
            model.add((bus.name + '_z_connected_to_HPC_3_constraint',), pyo.Constraint(model.T, rule=rule))


            for hpc, connected_profile in zip(self.get_HPC_chargers(), connected_profiles):
                t_not_connected = set()
                for t in model.T:
                    if connected_profile[t] == 0:
                        t_not_connected.add(t)

                for hpc_to_bus in self.get_HPC_to_bus_flows():
                    if bus.name in hpc_to_bus and hpc.name in hpc_to_bus:
                        def rule(m, t):
                            return model.component_dict[prefix + (hpc_to_bus,)][t] == 0
                        model.add((hpc_to_bus + '_disable_constraint',), pyo.Constraint(t_not_connected, rule=rule))
                        break

                for bus_to_hpc in self.get_bus_to_HPC_flows():
                    if bus.name in bus_to_hpc and hpc.name in bus_to_hpc:
                        def rule(m, t):
                            return model.component_dict[prefix + (bus_to_hpc,)][t] == 0
                        model.add((bus_to_hpc + 'disable_constraint',), pyo.Constraint(t_not_connected, rule=rule))
                        break

    # differenciation is done via connectors' other sides.
    # HPC chargers should be directly connected to the grid in the JSON file
    # depot chargers are connected to a bus bar which is then connected to the grid
    def get_HPC_chargers(self):
        HPC_chargers = []
        for inverter in self.get_inverters():
            for connector in self._connectors:
                if connector.name == inverter.name and connector.type.value == 'output_2':
                    if self.get_grid().name in connector.other_sides:
                        HPC_chargers.append(inverter)
                        break
        return HPC_chargers

    def get_HPC_to_bus_flows(self):
        flows = []
        for flow in self._flows:
            flow_found = False
            for hpc in self.get_HPC_chargers():
                if flow_found:
                    break
                for bus in self.get_buses():
                    if flow.startswith(hpc.name) and bus.name in flow:
                        flows.append(flow)
                        flow_found = True
                        break
        return flows

    def get_bus_to_HPC_flows(self):
        flows = []
        for flow in self._flows:
            flow_found = False
            for bus in self.get_buses():
                if flow_found:
                    break
                for hpc in self.get_HPC_chargers():
                    if flow.startswith(bus.name) and hpc.name in flow:
                        flows.append(flow)
                        flow_found = True
                        break
        return flows
