"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import pyomo.environ as pyo
import pandas as pd
import os
from datetime import timedelta
from Model_Library.Component.load_component_library import load_component_library
from Model_Library.Component.model.AbstractComponent import ComponentKind, ComponentCommodity
from Model_Library.Component.model.EMS_components.EnergyManagementSystem import implement_strategy
from Tooling.pareto_analysis.pareto_analysis import pareto_analysis
from Model_Library.OptimizationModel import OptimizationModel, EntityResult
from Tooling.dynamics.Dynamic import resample

from enum import Enum

component_directory = __file__
for _ in range(3):
    component_directory = os.path.dirname(component_directory)
component_directory = os.path.join(component_directory, 'Component', 'model')
component_library = load_component_library(component_directory)

model_directory = __file__
for _ in range(3):
    model_directory = os.path.dirname(model_directory)
model_directory = os.path.join(model_directory, 'Component', 'data')

class ConnectorType(Enum):
    INPUT_1 = 'input_1'
    INPUT_2 = 'input_2'
    OUTPUT_1 = 'output_1'
    OUTPUT_2 = 'output_2'

class ConnectorMode(Enum):
    EMPTY = 1
    SINGLE_CONTRACTED = 2
    SINGLE = 3
    MULTIPLE = 4

class Connector:
    def __init__(self, name, type, commodity):
        self.name = name
        self.type = type
        self.commodity = commodity
        self.flows = []
        self.other_sides = []

class Prosumer:
    def __init__(self, name, configuration, profiles, dynamic):
        self._name = name
        self._configuration = configuration
        self._components = dict()
        self._connectors = []

        for name, component_configuration in configuration['components'].items():
            component_type = component_configuration['type']
            component = component_library[component_type](name, component_configuration, model_directory, profiles, dynamic)
            self._components[name] = component

            input_commodity_1, input_commodity_2, output_commodity_1, output_commodity_2 = component.get_input_output_commodities()
            if input_commodity_1 != None:
                self._connectors.append(Connector(name, ConnectorType.INPUT_1, input_commodity_1))
            if input_commodity_2 != None:
                self._connectors.append(Connector(name, ConnectorType.INPUT_2, input_commodity_2))
            if output_commodity_1 != None:
                self._connectors.append(Connector(name, ConnectorType.OUTPUT_1, output_commodity_1))
            if output_commodity_2 != None:
                self._connectors.append(Connector(name, ConnectorType.OUTPUT_2, output_commodity_2))
        
        self._flows = []
        
        for connection in configuration['connections']:
            flow_from = connection['from']
            flow_output = str(connection['output'])
            flow_to = connection['to']
            flow_input = str(connection['input'])
            flow = flow_from + '_' + flow_output + '_' + flow_to + '_' + flow_input
            self._flows.append(flow)
            for connector in self._connectors:
                if connector.name == flow_from and connector.type.value == 'output_' + flow_output:
                    connector.flows.append((flow,))
                    connector.other_sides.append(flow_to)
                elif connector.name == flow_to and connector.type.value == 'input_' + flow_input:
                    connector.flows.append((flow,))
                    connector.other_sides.append(flow_from)

        for connector in self._connectors:
            if len(connector.flows) == 0:
                connector.mode = ConnectorMode.EMPTY
            elif len(connector.flows) > 1:
                connector.mode = ConnectorMode.MULTIPLE
            else:
                # the connector is single, but if we can contract depends on the connector on the other side
                # find connector on the other side
                for other_side_connector_option in self._connectors:
                    if connector == other_side_connector_option:
                        continue
                    if connector.flows[0] in other_side_connector_option.flows:
                        other_side_connector = other_side_connector_option
                        break
                # test if connector on the other side has been assigned a mode
                if hasattr(other_side_connector, 'mode'):
                    # the other side has been assigend a mode, so it could be contracted
                    if other_side_connector.mode != ConnectorMode.SINGLE_CONTRACTED:
                        # it has not, we can contract
                        connector.mode = ConnectorMode.SINGLE_CONTRACTED
                    else:
                        # it has, we cannot contract
                        connector.mode = ConnectorMode.SINGLE
                else:
                    # the other side has not been assigend a mode, so it is not contracted, so we can contract
                    connector.mode = ConnectorMode.SINGLE_CONTRACTED
                # contract the connector
                if connector.mode == ConnectorMode.SINGLE_CONTRACTED:
                    # remove flow
                    flow_to_remove = connector.flows[0]
                    self._flows.remove(flow_to_remove[0]) # connector.flows stores the flow as a singleton tuple of the flow name, but self._flow stores just the flow name, so extract the flow name from the singleton tuple
                    # replace flow on both sides with the input/output
                    connector.flows[0] = (connector.name, connector.type.value)
                    index = [i for i in range(len(other_side_connector.flows)) if other_side_connector.flows[i] == flow_to_remove][0]
                    other_side_connector.flows[index] = (connector.name, connector.type.value)

        self._dynamic = dynamic

        self._result = {}
        self._last_result_key = None

    def get_components(self, kind=ComponentKind.ALL, commodity=ComponentCommodity.ALL):
        return (component for component in self._components.values() if component.match(kind=kind, commodity=commodity))

    def build_model(self, configuration):
        model = OptimizationModel(self._name, configuration['dynamic'])

        for component in self._components:
            self._components[component].build_model(model, configuration)

        self.add_flow_variables(model)

        self.add_connection_constraints(model)

        implement_strategy(self, model, configuration)

        self.add_prosumer_variables(model)

        self.add_prosumer_constraints(model)

        return model

    def add_prosumer_constraints(self, model, prefix = ()):
        """
        Add specific constraints of a prosumer
        """
        pass

    def add_prosumer_variables(self, model, prefix = ()):
        """
        Add variables for specific constraints of a prosumer
        """
        pass

    def optimize_sizing(self, key, strategy):
        model = self.build_model({'dynamic': self._dynamic, 'strategy': strategy})

        # todo: necessary solver options (MIPgap, ...) should be available from runme.py
        options = dict()
        options['MIPGap'] = 0.01
        options['Presolve'] = 2
        options['TimeLimit'] = 200

        model.solve(options, False)

        if model.is_ok():
            # no resampling necessary, because the model has the same dynamic as the prosumer
            self._result[key] = self.get_empty_entity_result()
            self._result[key].extract_results_from_model(model)
            self._last_result_key = key
        else:
            print('ERROR: The model is infeasible or unbounded: no optimal solution found')

    def pareto_analysis(self, strategy):
        if len(strategy) != 2:
            raise ValueError('Pareto analysis can only be done with two strategies!')
        model = self.build_model({'dynamic': self._dynamic, 'strategy': strategy})

        pareto_analysis(self, model, list(strategy.keys()))

    def get_empty_entity_result(self):
        base_variables = []
        for component in self._components.values():
            base_variables.extend(component.get_base_variable_names())
        for flow in self._flows:
            base_variables.append(((flow,), 'T'))
        return EntityResult(self._dynamic, base_variables)

    def build_graph(self):
        graph = dict.fromkeys(self._components.keys())
        for key in graph:
            graph[key] = {'neigh_all': [], 'neigh_in': [], 'neigh_out': []}
        for connector in self._connectors:
            if connector.type == ConnectorType.INPUT_1 or connector.type == ConnectorType.INPUT_2:
                for other_side in connector.other_sides:
                    graph[connector.name]['neigh_in'].append(other_side)
                    if other_side not in graph[connector.name]['neigh_all']:
                        graph[connector.name]['neigh_all'].append(other_side)
            if connector.type == ConnectorType.OUTPUT_1 or connector.type == ConnectorType.OUTPUT_2:
                for other_side in connector.other_sides:
                    graph[connector.name]['neigh_out'].append(other_side)
                    if other_side not in graph[connector.name]['neigh_all']:
                        graph[connector.name]['neigh_all'].append(other_side)
        return graph

    def optimize(self, configuration, i_start, i_end):
        dynamic = self._dynamic.partial_dynamic(i_start, i_end)

        configuration['dynamic'] = dynamic

        model = self.build_model(configuration)

        options = dict()
        options['MIPGap'] = 0.01
        options['Presolve'] = 2
        options['TimeLimit'] = 200
        
        model.solve(options, False)

        if model.is_ok():
            return model
        else:
            return None

    def get_export_import(self, result, target_dynamic):
        data = dict()
        for component in self.get_components(kind=ComponentKind.GRID, commodity=ComponentCommodity.ELECTRICITY):
            export_data = resample(result[(component.name, 'input_1')], result.dynamic, target_dynamic)
            import_data = resample(result[(component.name, 'output_1')], result.dynamic, target_dynamic)
            data[component.name] = (export_data, import_data)
        return data

    def calc_annuity(self, price_ext, price_injection, df=False):
        """
        Calculates the annuity this prosumer achieves.

        Parameters
        ----------
        price_ext: Price for buying electricity from the main grid
        price_injection: Remuneration for feeding electricity into the public grid
        df: boolean variable. True --> consider DF acitvation; False--> before DF activation

        Returns
        ----------
        annuity_init: annuity of prosumer based on the initial schedule with it's individual constract
        annuity_community: prosumer's annuity with community conditions either with or without DF activations
        """
        # The factor that converts the simulation to ONE year
        annual_factor = timedelta(days=365) / timedelta(hours=sum(self._dynamic.step_size_p(position) for position in range(self._dynamic.number_of_steps())))

        self_data = self.get_export_import(self._result['sized'], self._dynamic)
        grid_export = sum(self_export for self_export, _ in self_data.values())
        grid_import = sum(self_import for _, self_import in self_data.values())

        grid = [self._components[comp] for comp in self._components if self._components[comp].type == 'ElectricalGrid'][0]

        annuity_init = (+ grid_export * self._dynamic.step_sizes() * grid.injection_price
                        - grid_import * self._dynamic.step_sizes() * grid.price)

        annuity_init = annuity_init.sum() * annual_factor

        # for INITIAL SCHEDULE
        if not df:

            annuity_community = (+ grid_export * self._dynamic.step_sizes() * price_injection[self._dynamic.time_steps()]
                                 - grid_import * self._dynamic.step_sizes() * price_ext)

            annuity_community = annuity_community.sum() * annual_factor
            print('Annuity init ' + self._name + ': ' + str(annuity_init))
            print('Annuity agg ' + self._name + ': ' + str(annuity_community))
        else:
            # for FIXED SCHEDULE
            self_data = self.get_export_import(self._result[self._last_result_key], self._dynamic)
            grid_export = sum(self_export for self_export, _ in self_data.values())
            grid_import = sum(self_import for _, self_import in self_data.values())

            annuity_community = (+ grid_export * self._dynamic.step_sizes() * price_injection[self._dynamic.time_steps()]
                                 - grid_import * self._dynamic.step_sizes() * price_ext)

            annuity_community = annuity_community.sum() * annual_factor
            print('Annuity with df ' + self._name + ': ' + str(annuity_community))

        return annuity_init, annuity_community

    def add_flow_variables(self, model):
        for flow in self._flows:
            model.add((flow,), pyo.Var(model.T, bounds=(0, None)))

    def add_connection_constraints(self, model):
        for connector in self._connectors:
            def rule(m, t):
                return model.component_dict[(connector.name, connector.type.value)][t] == pyo.quicksum(model.component_dict[flow_variable][t] for flow_variable in connector.flows)
            model.add((connector.name + '_' + connector.type.value + '_sum',), pyo.Constraint(model.T, rule = rule))
    
    def save_results(self):
        if not os.path.exists('output_files/'):
            os.makedirs('output_files/')
        if not os.path.exists('output_files/' + self._name + '/'):
            os.makedirs('output_files/' + self._name + '/')

        # ToDo: wrap errors, exceptions in all functions
        # Results df
        with pd.ExcelWriter('output_files/' + self._name + '/results_' + self._name + '.xlsx') as writer:
            self._result[self._last_result_key].to_excel(writer)
